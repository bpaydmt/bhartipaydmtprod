<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


 <script type="text/javascript">
			function getDistributerByAggId(aggId)
			{  
				
				$.ajax({
			        url:"GetActiveDistributerByAggId",
			        cache:0,
			        data:"reportBean.aggId="+aggId,
			        success:function(result){
			        
			               document.getElementById('distributor').innerHTML=result;
			               
			         }
			  });  
				return false;
			}

			function getAgentByDistId(distId)
			{  
				
				$.ajax({
			        url:"GetActiveAgentByDistId",
			        cache:0,
			        data:"reportBean.distId="+distId,
			        success:function(result){
			        	
			               document.getElementById('agent').innerHTML=result;
			               
			         }
			  });  
				return false;
			}
			
			function getSubagentByAgentId(agentId)
			{  
				
				$.ajax({
			        url:"GetSubagentByAgentId",
			        cache:0,
			        data:"reportBean.agentId="+agentId,
			        success:function(result){
			              document.getElementById('subagent').innerHTML=result;
			         }
			  });  
				return false;
			}
			
			function showDiv(usertype){  
				if(usertype==6){
					document.getElementById("subone").style.display ="none";
					document.getElementById("ageone").style.display ="none";
					document.getElementById("disone").style.display ="none";
					document.getElementById("aggoneone").style.display ="none";
						
					}
			else if(usertype==-1){
						document.getElementById("subone").style.display ="none";
						document.getElementById("ageone").style.display ="none";
						document.getElementById("disone").style.display ="none";
						document.getElementById("aggoneone").style.display ="none";
							
						}
					else if(usertype==1){
						document.getElementById("subone").style.display ="block";
						document.getElementById("ageone").style.display ="block";
						document.getElementById("disone").style.display ="block";
						document.getElementById("aggoneone").style.display ="none";				
						
						
					} else if (usertype == 2) {
						document.getElementById("subone").style.display ="none";
						document.getElementById("ageone").style.display ="none";
						document.getElementById("disone").style.display ="block";
						document.getElementById("aggoneone").style.display ="none";
						
					} else if (usertype == 3) {
						document.getElementById("subone").style.display ="none";
						document.getElementById("ageone").style.display ="none";
						document.getElementById("disone").style.display ="none";
						document.getElementById("aggoneone").style.display ="none";
						
					} else if (usertype == 4) {
						document.getElementById("subone").style.display ="none";
						document.getElementById("ageone").style.display ="block";
						document.getElementById("disone").style.display ="block";
						document.getElementById("aggoneone").style.display ="none";
						
					} else if (usertype == 5) {
						document.getElementById("subone").style.display ="none";
						document.getElementById("ageone").style.display ="block";
						document.getElementById("disone").style.display ="block";
						document.getElementById("aggoneone").style.display ="none";			
						
					}

				}
		function checkSelectVal(){
			 
			 var elm = $("#usertype").val();
			 var des = $("#distri").val();
			 var agent = $("#agent1").val();
			 $("#suc").html("");
			 
			 if(elm == "-1"){
				 $("#err").text("Please select user Types");
				 event.preventDefault();
			     return false;       
			   }else if(elm == "2"){
				   
				   if(des == "-1"){
						 $("#err").text("Please select user distributer");
						 event.preventDefault();
						 
					     return false;       
					   }
			   }
				else if(elm == "1" || elm == "5"){
				   
				   if(des == "-1" || agent == "-1"){
					   if(des == "-1"){
						   $("#err").text("Please select user distributer");
						   
					   }else {
						   $("#err").text("Please select user Agent");
					   }
						
						 event.preventDefault();
					     return false;       
					  }
			   }
			     
			}   
</script>
</head>
<%
String userType=(String)request.getAttribute("userType");
if(userType!=null&&!userType.equalsIgnoreCase("-1")&&!userType.isEmpty()){
%>
<body onload="showDiv(<%=userType%>)">
<%	
}else{
	%>
	<body>
	<% 
}
%>


    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
 
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
        
 
            
<%
User user=(User)session.getAttribute("User");
%>
    
 
    <div class="box ">
        <div class="box-inner">
            <div class="box-header  ">
                <h2><i class="glyphicon "></i><%if(user.getUsertype()==99){out.print("Create Aggregator");}else{out.print("Create User");}%></h2>
            
            </div>
            <div class="box-content  ">
                  
		<!-- <form action="SaveSignUpUserByAgent.action" id="form_name" method="post" name="PG" onsubmit="checkSelectVal();submitForm(this)"> -->
		<form action="SaveSignUpUserByAgent" id="form_name" method="post" name="PG" >
	 
		 <div class="row">  
		 	
									<div class="row">
									<font id="err" color="red"><s:actionerror/> </font>
									<font id="suc" color="blue"><s:actionmessage/> </font>
									<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
									
<%if(!(user.getUsertype()==99)){ %>
														<%-- 	<input	class="form-control" name="walletBean.aggreatorid" id="wwdw" type="hidden" value="<%=user.getId() %>" />
															<input type="hidden" value="<%=user.getUsertype()%>" id="usertypeid">
															 --%>
<!-- ************************************************************************************************************************************-->
							<div class="form-group  col-md-12  txtnew  col-xs-6">
							<div class="col-sm-3">
									<label for="email">Select User Type <font color='red'> *</font></label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">
										<%if(user.getUsertype()==4){ %>
										    <select name="walletBean.usertype" id="usertype" cssClass="">
										    <option selected="selected" value='6'>Sub-Aggregator</option>
										    </select>
										    
											

                                           <%}else{ %>
                                            <s:select list="%{usertypeList}" headerKey="-1"
												headerValue="Select User Type" id="usertype"
												name="walletBean.usertype" cssClass=""
												requiredLabel="true"
												onchange="showDiv(document.getElementById('usertype').value);" />
                                         
                                          <%} %>
										</div>
									</div>
								</div>
							<%if(user.getUsertype()==99){ %>
							
							
							<div id="aggoneone" style="display: none">
							
								<div class="col-sm-3">
									<label for="email">Select Aggregator<font color='red'> *</font></label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="wwctrl_paymentMethods">

											<s:select list="%{aggrigatorList}" headerKey="-1"
												headerValue="Select Aggregator" id="aggregator"
												name="walletBean.aggreatorid" cssClass=""
												Label="true"
												onchange="getDistributerByAggId(document.getElementById('aggregator').value);" />

										</div>
									</div>
								</div>
</div>
<%}if(user.getUsertype()==99||user.getUsertype()==4||user.getUsertype()==6){ %>
	<div id="disone" style="display: none">
								<div class="col-sm-3">
									<label for="email">Select Distributor <font color='red'> *</font></label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="distributor">

											<s:select list="%{distributorList}" headerKey="-1"
												headerValue="Select Distributor" id="distri"
												name="walletBean.distributerid" cssClass=""
												Label="true"
												onchange="getAgentByDistId(document.getElementById('distri').value);" />

										</div>
									</div>

								</div>
								</div>
<%}if(user.getUsertype()==99||user.getUsertype()==4||user.getUsertype()==3||user.getUsertype()==6){ %>
	<div id="ageone" style="display: none">
								<div class="col-sm-3">
									<label for="email">Select Agent <font color='red'> *</font></label> <br>
									<div class="wwgrp" id="wwgrp_paymentMethods">
										<div class="wwctrl" id="agent">

											<s:select list="%{agentList}" headerKey="-1"
												headerValue="Select Agent" id="agent1"
												name="walletBean.agentid" cssClass=""
												Label="true"
												onchange="getSubagentByAgentId(document.getElementById('agent1').value);" 
												/>

										</div>
									</div>

								</div>
								</div>
								<%} 
if(user.getUsertype()==99||user.getUsertype()==4||user.getUsertype()==3||user.getUsertype()==2||user.getUsertype()==6){ 
%>
	<div id="subone" style="display: none">
<div class="col-sm-3">
	<label for="email">Select Sub-agent</label> <br>
	<div class="wwgrp" id="wwgrp_paymentMethods">
		<div class="wwctrl" id="agent">

			<s:select list="%{subagentList}" headerKey="-1"
				headerValue="Select Sub-agent" id="subagent"
				name="walletBean.subAgentId" cssClass=""
					 Label="true"
				/>

		</div>
	</div>

</div>
</div>
<%} 
}%>
				
							</div>
							
							
<!-- ****************************************************************************************************************************-->
															<%if(user.getUsertype()==99){ %>
															<input	class="form-control" name="walletBean.aggreatorid" id="wwdw" type="hidden" value="-1" />															
															<input	class="form-control" name="walletBean.distributerid" type="hidden" value="-1" />
															<input	class="form-control" name="walletBean.agentid" type="hidden" value="-1" />
															<input	class="form-control" name="walletBean.subAgentId" type="hidden" value="-1" />
															<input	class="form-control" name="walletBean.usertype" type="hidden" value="4" />															
															<%}if(user.getUsertype()==4||user.getUsertype()==6){ %>
															<input	class="form-control" name="walletBean.aggreatorid" id="wwdw" type="hidden" value="<%=user.getId()%>" />															
															<%}if(user.getUsertype()==3){ %>
															<input	class="form-control" name="walletBean.aggreatorid" id="wwdw" type="hidden" value="<%=user.getAggreatorid() %>" />															
															<input	class="form-control" name="walletBean.distributerid" type="hidden" value="<%=user.getId() %>" />
															<%}if(user.getUsertype()==2){%>
															<input	class="form-control" name="walletBean.aggreatorid" id="wwdw" type="hidden" value="<%=user.getAggreatorid() %>" />															
															<input	class="form-control" name="walletBean.distributerid" type="hidden" value="<%=user.getDistributerid() %>" />
															<input	class="form-control" name="walletBean.agentid" type="hidden" value="<%=user.getId() %>" />
															<%}if(user.getUsertype()==5){ %>
															<input	class="form-control" name="walletBean.aggreatorid" id="wwdw" type="hidden" value="<%=user.getAggreatorid() %>" />															
															<input	class="form-control" name="walletBean.distributerid" type="hidden" value="<%=user.getDistributerid() %>" />
															<input	class="form-control" name="walletBean.agentid" type="hidden" value="<%=user.getAgentid()%>" />
															<input	class="form-control" name="walletBean.subAgentId" type="hidden" value="<%=user.getId() %>" />
															<%}%>															
															<input	class="form-control" name="walletBean.createdby" id="createdby" type="hidden" value="<%=user.getId() %>" />
									
									
             					 <div class="col-md-12">
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Name <font color='red'> *</font></label>
															
															<input type="text" name="walletBean.name" placeholder="Name" class="form-control userName mandatory" id="name"  value="<s:property value='%{walletBean.name}'/>"  >

														</div>
													</div>
														<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Email <font color='red'> *</font></label>
			                       <input type="email" name="walletBean.emailid" placeholder="Email" class=" form-control emailid mandatory" id="email"  value="<s:property value='%{walletBean.emailid}'/>" >

														</div>
													</div>
														<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Mobile Number <font color='red'> *</font></label>
			                       <input type="text" name="walletBean.mobileno" placeholder="Mobile Number" class=" form-control mobile mandatory" id="mobile"  value="<s:property value='%{walletBean.mobileno}'/>" title="Mobile number must have 10 digits and should start with 7,8 or 9."  maxlength="10"  pattern="[6789][0-9]{9}" requiredmessage="invalid mobile number"   >

														</div>
													</div>
									</div>
									
									 <div class="col-md-12">
									
									 <% 
									/*  Map<String,String> mapResult=(Map<String,String>)request.getSession().getAttribute("mapResult");
			                        if(mapResult!=null&&mapResult.get("emailvalid")!=null&&Double.parseDouble(String.valueOf(mapResult.get("emailvalid")))==0){ */
			                        	  if(user.getEmailvalid()==0){
			                        %>
									
									
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Password <font color="red"> *</font></label>
			                       <input type="password"   name="walletBean.password" placeholder="Password " class=" form-control mandatory password-vl" id="password"   required >

														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Confirm Password <font color="red"> *</font></label>
			                       		<input type="password"   name="walletBean.confirmPassword" placeholder="Confirm Password " class=" form-control confirm-vl mandatory" id="password" required>

														</div>
													</div>
													<!-- 	<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">White Label </label>
			                       		<input type="radio"   name="walletBean.whiteLabel"  value="1" class=" form-control confirm-vl mandatory" id="whitelabel" >

														</div>
													</div> -->
													
													<%}if(user.getUsertype()==99){ %>
             
              <div class="col-md-4">
              <div class="form-group">
               <label for="apptxt">White Label</label>
                           <!--  <input type="radio"   name="walletBean.whiteLabel"  value="1" class=" form-control confirm-vl mandatory" id="whitelabel" > -->
                                                       <select name="walletBean.whiteLabel"  id="slectboxid">
                                                        <option value="0">Normal</option>
                                                        <option value="1">White Label</option>
                                                         </select> 
              </div>
             </div> 
             
             <%} else{%>
             
             <input type="hidden" name="walletBean.whiteLabel" value="<%=user.getWhiteLabel()%>">
             <%}
									
									%>
										</div>






												
												<div class="col-md-12">

													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Address 1</label> <input
																type="text" name="walletBean.address1"
																placeholder="Address 1" value="<s:property value='%{walletBean.address1}'/>"
																class=" form-control address-vl" maxlength="100" id="address1"
																/>

														</div>
													</div>



													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Address 2</label> <input
																type="text" name="walletBean.address2"
																placeholder="Address 2" maxlength="100"
																class=" form-control address-vl" id="address2"
																value="<s:property value='%{walletBean.address2}'/>"/>

														</div>
													</div>

													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">City</label> <input
																type="text" name="walletBean.city"
																placeholder="City"
																class=" form-control userName" id="city"
																value="<s:property value='%{walletBean.city}'/>"
																/>

														</div>
													</div>


												</div>


												<div class="col-md-12">

													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">State</label> <input
																type="text" name="walletBean.state"
																placeholder="State"
																class=" form-control userName" id="state"
																value="<s:property value='%{walletBean.state}'/>"
																/>

														</div>
													</div>



													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">PIN Code</label> 
															<input
																type="text" name="walletBean.pin"
																placeholder="PIN Code" maxlength='6' min='100000'
																class=" form-control pincode" id="pin"
																value="<s:property value='%{walletBean.pin}'/>"
																/>

														</div>
													</div>

													 <div class="col-md-4">
														<div class="form-group">

															<input type="button" class="btn btn-success submit-form"
																style="margin-top: 20px;"  value="Submit" onclick="submitVaForm('#form_name')" />
																


															<button type="reset" onclick="resetForm('#form_name')"  class="btn reset-form btn-info btn-fill"
																style="margin-top: 20px;">Reset</button>


														</div>
													</div> 

												</div>



												<div class="col-md-12">


									</div>
									
									</div>
											</div>
									</form>
								
									

        
                

    
            </div>
        </div>
  
</div>
 
        
        
 

       
  		    </div>
        </div>
	</div> 
</div><!--/.fluid-container-->


<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


