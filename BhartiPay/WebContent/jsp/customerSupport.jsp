<%@page import="java.util.List"%>
<%@page import="com.bhartipay.wallet.report.bean.SupportTicketBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />


<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script> 

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<style>
	textarea.form-control {
    min-height: 30px;
    line-height: 9px;
}





.custom-file-input {
  color: transparent;
}
.custom-file-input::-webkit-file-upload-button {
  visibility: hidden;
}
.custom-file-input::before {
  content: 'Upload Error Page';
  color: black;
  display: inline-block;
  background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
  border: 1px solid #999;
  border-radius: 3px;
  padding: 6px 12px;
  outline: none;
  white-space: nowrap;
  -webkit-user-select: none;
  cursor: pointer;
  text-shadow: 1px 1px #fff;
  /* font-weight: 700;
  font-size: 10pt; */
  border-radius: 15px;
  position: absolute;
  top: 22px;
}
.custom-file-input:hover::before {
  border-color: black;
}
.custom-file-input:active {
  outline: 0;
}
.custom-file-input:active::before {
  background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9); 
}


input[type=file] {
    display: inline-block !important;
}

</style>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->

        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
<div id="main" role="main"> 
  <div id="content">         
        

            

<div class=" row"> 
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header">
                <h2><i class="glyphicon "></i>Customer Support</h2>

                
            </div>
            <div class="box-content">                  
		<form action="SaveSupportDetails" id="form_name" method="post" name="PG">
			
		
<%
User user=(User)session.getAttribute("User");
List<SupportTicketBean>list=(List<SupportTicketBean>)request.getAttribute("supportList");
%>          
		
									<div class="row">
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>
             					
													<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
													<%-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
														 
															<label for="apptxt">Agent Id</label>
															<input	class="form-control" style="margin-left:0px;" name="supBean.userId" type="text" value="<%=user.getId() %>" readonly/>
															 <input	class="form-control" name="supBean.mobileNo" type="text" required="required"
																id="mobile" maxlength="20" placeholder="Mobile number" value="<%=user.getMobileno() %>"   readonly/>

														 
													</div> --%>
													
													
													
													<%-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
														 
															<label for="apptxt">Mobile Number</label>
															<input	class="form-control" name="supBean.userId" type="hidden" value="<%=user.getId() %>" />
															 <input	class="form-control" style="margin-left:0px;" name="supBean.mobileNo" type="text" required="required"
																id="mobile" maxlength="20" placeholder="Mobile number" value="<%=user.getMobileno() %>"   readonly/>

														 
													</div> --%>
													
													
													<%-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 form-group">
														 
															<label for="apptxt">Email </label>
															 <input	class="form-control" style="margin-left:0px;" name="supBean.emailId" type="text" required="required"
																id="email" maxlength="20"  placeholder="Email" value="<%=user.getEmailid() %>" readonly/>

														 
													</div> --%>
													
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 " style="padding-bottom: 2px;">
													 
															<label for="apptxt">Ticket Type</label></br>
															 <select name="supBean.ticketType" style="border: 1px solid #ccc !important;height: 30px;margin-top: 10px;">
															 <option value="General problem">General problem</option>
															 <option value="Recharge problem">Recharge problem</option>
															 <option value="Refund problem">Refund problem</option>
															 <option value="Money transfer problem">Money transfer problem</option>
															 <option value="Refund problem">AEPS problem</option>
															 <option value="Money transfer problem">m-ATM problem</option>
															 </select>

														 
													</div>


									
									
									
									
									
									
									


													<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
														 
															<label for="apptxt">Description</label>
															 <textarea	class="form-control mandatory disc-vl" style="margin-left:0px;margin-left:0px;border: 1px solid #ccc !important;line-height: 19px; margin-top:10px;" name="supBean.description" type="text"
																id="description"  placeholder="Descriptoin"  ></textarea>

														 
													</div>
													
											
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									
									    <input type="file" class="custom-file-input">
										<button type="button" class="btn btn-success btn-fill" style="margin-top: 22px;" onclick="submitVaForm('#form_name',this)">Submit
										</button>
										<button type="button" class="btn btn-info btn-fill" style="margin-top: 22px;" onclick="resetForm('#form_name')">Reset
										</button>  
										
									</div>
									
									
					
									
									
									
								
									</div>
									
									
									</form>
	 
									 
										
										

									
									 
                

    
          
        </div>
  
</div>

<div id="pic">

<div id="xyz" class="box">
			<table id="example" class="scrollD cell-border dataTable no-footer">
				<thead>
				
				
					<tr>
						<th><u>Id</u></th>
						<th><u>Customer Id</u></th>
						<!-- <th><u>Mobile</u></th>
						<th><u>Email</u></th> -->
						<th><u>Ticket Type</u></th>
						<th><u>Ticket Description</u></th>
						<th><u>Remark</u></th>
						<th><u>Date</u></th>
						<th><u>Status</u></th>
					</tr>
				</thead>
				<tbody>
				<%
				for(SupportTicketBean wtb:list){
				%>
				  	<tr>
		             <td><%=wtb.getId()%></td>
		             <td><%=wtb.getUserId()%></td>
		             <%-- <td><%=wtb.getMobileNo()%></td>
		             <td><%=wtb.getEmailId()%></td> --%>
		             <td><%=wtb.getTicketType()%></td>
		             <td><%=wtb.getDescription()%></td>
		             <td><%=wtb.getRemarks()!=null?wtb.getRemarks():"-"%></td>
		          <td><%=wtb.getEntryon() %></td>
		               <td><%=wtb.getStatus()%></td>
                  		  </tr>
                  <%
                  }
				%>
			        </tbody>		</table>
		</div>
</div>

</div> 

</div>
        
        

        <!-- contents ends -->

       

</div>
</div> 

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


