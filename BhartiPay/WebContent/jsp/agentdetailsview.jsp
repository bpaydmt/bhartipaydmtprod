<%@page import="com.bhartipay.wallet.user.persistence.vo.AgentDetailsView"%>
<%  
AgentDetailsView adv=(AgentDetailsView)request.getAttribute("agentDetails");

%>

<div class="table-responsive">
<table class="table table-bordered table-striped">
	<tr>
	<td style="width:25%"><strong>Agent Id</strong></td>
	<td style="width:25%"><%=adv.getId() %></td>
	<td style="width:25%"><strong>Agent Type</strong></td>
	<td style="width:25%"><%=adv.getAgenttype()%></td>
	</tr>
	<tr>
		<td><strong>Agent Name</strong></td>
		<td><%=adv.getName() %></td>
		<td><strong>Agent DOB</strong></td>
		<td><%=adv.getDob()%></td>
	</tr>
	<tr>
		<td><strong>PAN</strong></td>
		<td><%=adv.getPan() %></td>
		<td><strong>Agent Mobile</strong></td>
		<td><%=adv.getMobileno()%></td>
	</tr>
	
		<tr>
		<td><strong>Address Proof Type</strong></td>
		<td><%=adv.getAddressprooftype() %></td>
		<td><strong>Address Proof Number</strong></td>
		<td><%=adv.getAdhar()%></td>
	</tr>
	<tr>
		<td><strong>Agent Email</strong></td>
		<td><%=adv.getEmailid() %></td>
		<td><strong></strong></td>
		<td></td>
	</tr>
	<tr class="details-heading">
		<td colspan="4">Address</td>
		
	</tr>
	
	<tr>
		<td><strong>Agent Address</strong></td>
		<td><%=adv.getAddress1()%></td>
		<td><strong>Agent City</strong></td>
		<td><%=adv.getCity()%></td>
	</tr>
	<tr>
		<td><strong>Agent State</strong></td>
		<td><%=adv.getState()%></td>
		<td><strong>PIN code</strong></td>
		<td><%=adv.getPin()%></td>
	</tr>
	<tr class="details-heading">
		<td colspan="4" >Shop Address</td>
		
	</tr>
	<tr>
		<td><strong>Shop Name</strong></td>
		<td><%=adv.getShopname()%> </td>
		<td><strong>Shop Address</strong> </td>
		<td><%=adv.getShopaddress1() %> <%=adv.getShopaddress2() %></td>
		
	</tr>
	<tr>
		<td><strong>Shop City</strong></td>
		<td><%=adv.getShopcity()%></td>
		<td><strong>Shop State</strong></td>
		<td><%=adv.getShopstate()%></td>
		
	</tr>
		<tr>
		<td ><strong>PIN code</strong></td>
		<td><%=adv.getShoppin()%></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		
	</tr>
	
	<tr class="details-heading">
		<td colspan="4 " >Payment Details</td>
		
	</tr>
	<tr>
		<td ><strong>Payment Amount</strong></td>
		<td><%=adv.getPaymentamount()%></td>
		<td ><strong>Payment Date</strong></td>
		<td><%=adv.getPaymentdate()%></td>
	</tr>
		<tr>
		<td ><strong>Payment Mode</strong></td>
		<td><%=adv.getPaymentmode()%></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
		<tr class="details-heading">
		<td colspan="4">Distributor Details</td>
		
	</tr>
		<tr>
		<td ><strong>Distributor Name</strong></td>
		<td><%=adv.getDistributerName()%></td>
		<td ><strong>Distributor Id</strong></td>
		<td><%=adv.getDistributerid()%></td>
	</tr>
	
		</tr>
		<tr class="details-heading">
		<td colspan="4">Account Details</td>
		
	</tr>
		<tr>
		<td ><strong>Account Number</strong></td>
		<td><%=adv.getAccountnumber()%></td>
		<td ><strong>Bank Name</strong></td>
		<td><%=adv.getBankname()%></td>
	</tr>
		</tr>
		<tr>
		<td ><strong>IFSC Code</strong></td>
		<td><%=adv.getIfsccode()%></td>
		<td ><strong></strong></td>
		<td></td>
	</tr>
	
	</tr>
		<tr class="details-heading">
		<td colspan="4">Other Details</td>
		
	</tr>
		<tr>
		<td ><strong>Territory</strong></td>
		<td><%=adv.getTerritory()%></td>
		<td ><strong>Manager Name</strong></td>
		<td><%=adv.getManagername()%></td>
	</tr>
		</tr>
		<tr>
		<td ><strong>SO Name</strong></td>
		<td><%=adv.getSoname()%></td>
		<td ><strong></strong></td>
		<td></td>
	</tr>
	
		<tr class="paymentref">
		<td ><strong>Payment Ref. No.</strong></td>
		<td><input type="text" id="utr" required="required"/></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	
	<tr>
		<td ><strong>Agent Form</strong></td>
		<td colspan="3"><a href="<%=adv.getFormlocation()%>" download>Agent Form</a></td>
		
	</tr>
	<tr>
		<td ><strong>Address Proof</strong></td>
		<td colspan="3"><a href="<%=adv.getAddresslocation()%>" download>Address Proof</a></td>
	</tr>
	<tr>
		<td ><strong>ID proof </strong></td>
		<td colspan="3"><a href="<%=adv.getIdlocation()%>" download>ID Proof</a></td>
	</tr>
	<tr id="decline-row" class="collapse" >
		<td ><strong>Decline comment </strong></td>
		<td colspan="3"> <form action="RejectAgentByAgg" method="post" >
			<div class="form-group"><input type="text" name="walletBean.declinedComment" class="form-control" id="decline-comment" /></div>
										
													
														<input type="hidden" name="user.userId"
															value="<%=adv.getId()%>"> 
															<input type="submit"
															class="btn btn-default pull-right" value="Decline" style="padding: 5px 13px;" onclick="return rejectAgent();">
													</form>
													
													</td>
	</tr>

</table>

</div>
<div class="modal-footer" id="agentFooter">

													<form method="post" action="AcceptAgentByAgg" id="acceptByAgent">
													<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
													<input type="hidden" id="utr-hidden" name="walletBean.utrNo" value="">
														<input type="hidden" name="user.userId"
															value="<%=adv.getId()%>">  <input
															type="submit" class="btn btn-default pull-right"
															value="Approve" onclick="return acceptAgent();" style="margin: 0 -21px 0 10px;"/>
													</form>
													
													<form action="AgentDetailsViewForUpdate" method="post">
													<input type="hidden" name="walletBean.aggreatorid" value="<%=adv.getAggreatorid()%>">
													<input type="hidden" name="walletBean.id" value="<%=adv.getId()%>">
												
		  <input
															type="submit" class="btn btn-default pull-right"
															value="Update"  style="margin: 0 -0px 0 10px;"/>
													</form>
												<a href="#decline-row" data-toggle="collapse" style="margin: 9px 9px;float: right;" onclick="document.getElementById('agentFooter').style.display = 'none'">Decline </a>
													
											
											

      </div>