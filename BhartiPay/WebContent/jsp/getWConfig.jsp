<%@taglib prefix="s" uri="/struts-tags" %>
<script src="js/validateWallet.js"></script>
           
 <div class="content-block">       
                <h3 class="title">General Configuration</h3>
												<div class="col-md-12">
												<div class="col-md-3">
														<div class="form-group">
															<label for="apptxt">Application Name<font color="red"> *</font></label>
															<input	class="form-control mandatory alphaNum-vl" name="config.appname" type="text" id="appname" placeholder="Application Name" value="<s:property value='%{config.appname}'/>" />
															

														</div>
													</div>
												
												<div class="col-md-3">
														<div class="form-group">
															<label for="apptxt">Select Country<font color="red"> *</font></label>
<input type="hidden" name="config.bannerpic" value="<s:property value='%{config.bannerpic}'/>">
<input type="hidden" name="config.logopic" value="<s:property value='%{config.logopic}'/>">															
<s:select list="%{countryCode}" name="config.countryid" cssClass="form-username mandatory" requiredLabel="true" id="ccode" 
onchange="getcurrencyByCountryId(document.getElementById('ccode').value);" headerKey="-1" headerValue="Select Country"/>
														</div>
													</div>
													
												
													<div class="col-md-3">
														<div class="form-group">
															<label for="apptxt">Currency<font color="red"> *</font></label>
															<%-- <input	class="form-control" name="config.countryid" type="hidden" value="<s:property value='%{config.countryid}'/>" /> --%>
<%-- 															<input	class="form-control" name="config.appname" type="hidden" value="<s:property value='%{config.appname}'/>" />
 --%>															<%-- <input	class="form-control" name="config.country" type="hidden" value="<s:property value='%{config.country}'/>" /> --%>
 															<input	class="form-control" name="config.status" type="hidden" value="<s:property value='%{config.status}'/>" />
														
															 <input	class="form-control" name="config.countrycurrency" type="text"
																id="ccurrency" readonly="readonly" maxlength="20"  placeholder="Currency" value="<s:property value='%{config.countrycurrency}'/>" />

														</div>
													</div>
													
													
													<div class="col-md-3">
														<div class="form-group">
															<label for="apptxt">SMS Required</label>
<s:select list="%{smssendList}" name="config.smssend" cssClass="form-username" requiredLabel="true" />
														</div>
													</div>
													
													</div>
									
									
									
									
									 <div class="col-md-12">
												<div class="col-md-3">
														<div class="form-group">
															<label for="apptxt">KYC Validation Required</label>
<s:select list="%{kycvalidList}" name="config.kycvalid" cssClass="form-username" requiredLabel="true" />
														</div>
													</div>


													<div class="col-md-3">
														<div class="form-group">
															<label for="apptxt">Email Validation</label>
<s:select list="%{emailvalidList}" name="config.emailvalid" cssClass="form-username" requiredLabel="true" />
														</div>
													</div>
													
													<div class="col-md-3">
														<div class="form-group">
															<label for="apptxt">OTP Send on Email</label>
<s:select list="%{otpsendtomailList}" name="config.otpsendtomail" cssClass="form-username" requiredLabel="true" />
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group">
															<label for="apptxt">Maker/Checker</label>
<s:select list="%{reqAgentApprovalList}" name="config.reqAgentApproval" cssClass="form-username" requiredLabel="true" />
														</div>
														</div>	
													</div>

												<div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Protocol</label>
<s:select list="%{protocolList}" name="config.protocol" cssClass="form-username" requiredLabel="true" />
														</div>
															<div class="form-group">
																<label for="apptxt">GCM App Key<font color="red"> *</font></label>
															
 <input	class="form-control mandatory customer-id" name="config.gcmAppKey" type="text"
																id="ccurrency"  maxlength="100"  placeholder="GCM App Key" value="<s:property value='%{config.gcmAppKey}'/>" />
															</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Domain <font color="red"> *</font></label>
 <input	class="form-control mandatory no-space" name="config.domainName" type="text"
																id="domain"  maxlength="100"  placeholder="Domain Name" value="<s:property value='%{config.domainName}'/>" />

														</div>
														<div class="form-group">
																<label for="apptxt">Caption(Title)<font color="red"> *</font></label>
															
 <input	class="form-control mandatory" name="config.caption" type="text"
																id="ccurrency"  maxlength="100"  placeholder="Caption" value="<s:property value='%{config.caption}'/>" />
															</div>
													</div>
													
													
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Transaction Type<font color="red"> *</font></label>
													<s:select list="%{txnTypeList}" size="6"
																id="appIdsSelect" cssClass="" name="config.txnType"
																
																multiple="true" placeholder="Allowed Transaction Type"/>
													
													</div>
													</div>
												
													
									</div>
													
													

												</div>
												
											<div class="content-block">       
                <h3 class="title">Payment Gateway Configuration </h3>
                 			 <div class="col-md-12">
												<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Payment Gateway Agent Id <font color="red"> *</font></label>
	  <input	class="form-control mandatory alphaNum-vl" name="config.pgAgId" type="text" id="pgAgId"  maxlength="100" 
	  placeholder="Payment Gateway Agent Id" value="<s:property value='%{config.pgAgId}'/>" />
	  
	
														</div>
													</div>

													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Payment Gateway MID<font color="red"> *</font></label>
  <input	class="form-control mandatory alphaNum-vl" name="config.pgMid" type="text" id="pgMid"  maxlength="100" 
	  placeholder="Payment Gateway MID" value="<s:property value='%{config.pgMid}'/>" />
	  
	 
														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Payment Gateway URL <font color="red"> *</font></label>
 <input	class="form-control mandatory url-vl" name="config.pgUrl" type="text" id="pgUrl"  maxlength="100" 
	  placeholder="Payment Gateway URL" value="<s:property value='%{config.pgUrl}'/>" />
	  
	
														</div>
													</div>
													
													</div>
		<div class="col-md-12">
												<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Payment Gateway Encryption Key <font color="red"> *</font></label>
  <input	class="form-control mandatory customer-id" name="config.pgEncKey" type="text" id="pgEncKey"  maxlength="100" 
	  placeholder="Payment Gateway Encryption Key" value="<s:property value='%{config.pgEncKey}'/>" />
	  
	

														</div>
													</div>


								
									


													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Transaction Type <font color="red"> *</font></label>
  <%-- <input	class="form-control	mandatory singleName" name="config.pgTxnType" type="text" id="pgTxnType"  maxlength="100" 
	  placeholder="Transaction type" value="<s:property value='%{config.pgTxnType}'/>" /> --%>
	 
	  <s:select list="%{ptxnList}" name="config.pgTxnType" cssClass="form-username" requiredLabel="true" />

														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Select Country <font color="red"> *</font></label>
	<%--   <input	class="form-control mandatory singleName" name="config.pgCountry" type="text" id="pgCountry"  maxlength="100" 
	  placeholder="Country" value="<s:property value='%{config.pgCountry}'/>"/> --%>
<s:select list="%{pcountryCode}" name="config.pgCountry" cssClass="form-username mandatory" requiredLabel="true" id="pgcode" 
onchange="getcurrencyByCountryCode(document.getElementById('pgcode').value);" headerKey="-1" headerValue="Select Country"/>
														</div>
													</div>
													
													</div>
													<div class="col-md-12">
															<div class="col-md-4">
																<div class="form-group">
																	<label for="apptxt">Currency  <font color="red"> *</font></label> 
																	<input
																		class="form-control mandatory singleName" name="config.pgCurrency"
																		type="text" id="pgCurrency" maxlength="100"
																		placeholder="Currency"
																		value="<s:property value='%{config.pgCurrency}'/>" readonly="readonly"
																		/>
																</div>
															</div>

															
															<div class="col-md-4">
																<div class="form-group">
																	<label for="apptxt">Call Back URL <font color="red"> *</font></label> <input
																		class="form-control mandatory url-vl" name="config.pgCallBackUrl"
																		type="text" id="pgCallBackUrl" maxlength="100"
																		placeholder="Call Back URL"
																		value="<s:property value='%{config.pgCallBackUrl}'/>"
																	 />
																</div>
															</div>
														</div>
                
                </div>
		
			<div class="content-block">       
              <h3 class="title">Choose your theme</h3>
              <ul class="Choose-theme">
             
      <%
      String themes=(String)request.getAttribute("themes");
   if(themes==null||themes.isEmpty()||themes.equalsIgnoreCase("default")){
	  %>
	    <li class="li-default" onclick="changeTheme(this)">   <input type="radio" name="config.themes" value="default"  checked /></li>
			 			  <li class="li-purple" onclick="changeTheme(this)">    <input type="radio" name="config.themes"  value="purple"/></li>  
			               <li class="li-dark-blue" onclick="changeTheme(this)"> <input type="radio"  name="config.themes" value="dark-blue"></li>
			               <li class="li-orange" onclick="changeTheme(this)">    <input type="radio" name="config.themes" value="orange" /></li>
	  
	  <%
	 
   }
   else if(themes.equalsIgnoreCase("purple")){
	   %>
	    <li class="li-default" onclick="changeTheme(this)">   <input type="radio" name="config.themes" value="default" /></li>
			 			  <li class="li-purple" onclick="changeTheme(this)">    <input type="radio" name="config.themes"  value="purple" checked/></li>  
			               <li class="li-dark-blue" onclick="changeTheme(this)"> <input type="radio"  name="config.themes" value="dark-blue"></li>
			               <li class="li-orange" onclick="changeTheme(this)">    <input type="radio" name="config.themes" value="orange" /></li>
	  
	  <%
   } else if(themes.equalsIgnoreCase("dark-blue")){
	   %>
	    <li class="li-default" onclick="changeTheme(this)">   <input type="radio" name="config.themes" value="default" /></li>
			 			  <li class="li-purple" onclick="changeTheme(this)">    <input type="radio" name="config.themes"  value="purple"/></li>  
			               <li class="li-dark-blue" onclick="changeTheme(this)"> <input type="radio"  name="config.themes" value="dark-blue" checked/></li>
			               <li class="li-orange" onclick="changeTheme(this)">    <input type="radio" name="config.themes" value="orange" /></li>
	  
	  <%   
   }
 else if(themes.equalsIgnoreCase("orange")){
	 %>
	    <li class="li-default" onclick="changeTheme(this)">   <input type="radio" name="config.themes" value="default" /></li>
			 			  <li class="li-purple" onclick="changeTheme(this)">    <input type="radio" name="config.themes"  value="purple"/></li>  
			               <li class="li-dark-blue" onclick="changeTheme(this)"> <input type="radio"  name="config.themes" value="dark-blue"></li>
			               <li class="li-orange" onclick="changeTheme(this)">    <input type="radio" name="config.themes" value="orange"  checked/></li>
	  
	  <%
   }
      %>

			

             </ul>

                </div>
                								<div class="content-block"> 
                								      
                <h3 class="title">Upload files</h3>
                  
												<div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Select Logo</label>
															<input type="file" name="config.logo">
															
														</div>
													</div>
													<img src='<s:property value='%{config.logopic}'/>'  height='50px' width='50px'>
												</div>
												<div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Select Banner</label>
															<input type="file" name="config.banner">
														</div>
													</div>
													
													<img src='<s:property value='%{config.bannerpic}'/>'  height='50px' width='200px'>	
												</div>
	
	<div class="col-md-12">	<div class="alert alert-warning">
   
    <strong>Note:</strong>  Only png, jpeg and gif file format are allowed and file size must not exceed 1 MB.
</div></div>

                </div>
                
                	