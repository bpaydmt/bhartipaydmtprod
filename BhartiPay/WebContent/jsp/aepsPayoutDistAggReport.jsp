<%@page import="com.bhartipay.wallet.aeps.AepsSettlementBean"%>
<%@page import="com.bhartipay.wallet.aeps.AEPSLedger"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="java.text.DecimalFormat"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>



<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />

<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>

<%-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> > --%>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" / >  
 
  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
 
   $(document).ready(function() {
    
        
    $('#reportrange').daterangepicker({
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2024',
        dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD-MMM-YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom Range',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
     },
     function(start, end) {
         console.log("Callback has been called!");
         $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
         $('#from').val(start.format('DD-MMM-YYYY'));
         $('#to').val(end.format('DD-MMM-YYYY'));

        }
     );
  //Set the initial state of the picker label
	if("<s:property value='%{inputBean.stDate}'/>"==""){
	    $('#reportrange span').html(moment().subtract('days', 29).format('DD-MMM-YYYY') + ' - ' + moment().format('DD-MMM-YYYY'));
	    $("#from").val(moment().subtract('days', 29).format('DD-MMM-YYYY'));
	    $("#to").val(moment().format('DD-MMM-YYYY'));
	  	}else{
	  	$('#reportrange span').html('<s:property value="%{inputBean.stDate}"/>' + ' - ' + '<s:property value="%{inputBean.endDate}"/>');	
	  } 
  
 });


</script> 
 
<script type="text/javascript">
    $(document).ready(function() { 
    	
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 1, "asc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Bank Settlement - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Bank Settlement - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Bank Settlement - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Bank Settlement - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Bank Settlement - ' + '<%= currentDate %>',
              
            },{
                extend: 'colvis',
                columnText: function ( dt, idx, title ) 
                {
                    return (idx+1)+': '+title;
                }
            }
        ]
    } );
  

   /*  $('#dpStart').datepicker({
     language: 'en',
     autoClose:true,
     maxDate: new Date(),


    });
    if($('#dpStart').val().length != 0){
    	var selectedDate = new Date(converDateToJsFormat($('#dpStart').val()));
        var setDate = selectedDate.setDate(selectedDate.getDate() + 30);
    	$('#dpEnd').datepicker({
           language: 'en',
           autoClose:true,
           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
           maxDate: new Date(),
           
          }); 
    	
    }
    $("#dpStart").blur(function(){
   
    $('#dpEnd').val("")
     $('#dpEnd').datepicker({
          language: 'en',
         autoClose:true,
         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
         maxDate: new Date(),
         
        }); 
    }) */
   
    
    } ); 
	 /* function converDateToJsFormat(date) {
	   
		var sDay = date.slice(0,2);
		var sMonth = date.slice(3,6);
		var yYear = date.slice(7,date.length)
	
		return sDay + " " +sMonth+ " " + yYear;
	} */

</script>

<style>
    #agentFooter, .paymentref{display:none}
</style>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in")); 
DecimalFormat d=new DecimalFormat("0.00");

%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
<div id="main" role="main"> 
  <div id="content">       
      <div class="row"> 
 
		<div class="box2 col-md-12">
			<div class="box-inner"> 
				<div class="box-header">
					<h2>Bank Settlement</h2>
				</div>
				
				<div class="box-content">
				
					<form action="GetAepsPayoutReportByDistAgg" method="post" autocomplete="off">
                        <div class="row">  		
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 form-group">  
							    <input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">  
							    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
								    <i class="fa fa-calendar"></i>&nbsp;
								    <span></span> <i class="fa fa-caret-down"></i>
								</div>	
							    
								<input type="hidden" value="<s:property value='%{inputBean.stDate}'/>" name="inputBean.stDate" id="from" class="form-control datepicker-here1" placeholder="Start Date" data-language="en" required/>

								<input type="hidden" value="<s:property value='%{inputBean.endDate}'/>" name="inputBean.endDate" id="to" class="form-control datepicker-here2" placeholder="End Date" data-language="en"  required>
							</div>						
							<%

							if(user.getWhiteLabel()==0 && (user.getUsertype()==4 || user.getUsertype()==6)){

							%>
							<div class="col-md-4 col-sm-6 col-xs-12 form-group"> 
 				                <s:select list="%{aggrigatorList}" headerKey="All" headerValue="All" id="aggregatorid" name="walletBean.aggreatorid" cssClass="form-username" requiredLabel="true"/>
						    </div>
							<%} else if( user.getUsertype() ==3){%>	
							<div class="col-md-4 col-sm-6 col-xs-12 form-group"> 
								<s:select list="%{agentList}" headerKey="-1" headerValue="Select Agent" id="agentsub" name="inputBean.userId" cssClass="form-username" requiredLabel="true" />				
						    </div>
							<%} %>	
							<div class="col-md-2 col-sm-4 col-xs-12"> 
								<div  id="wwctrl_submit">
									<input class="btn btn-success" id="submit"
										type="submit" value="Submit">
											<input class="btn btn-primary btn-fill" id="reset"
										type="reset" value="Reset">
								</div>
							</div>
                           
                           <%
		List<AepsSettlementBean>list=(List<AepsSettlementBean>)request.getAttribute("agentList");
		int txnCount= 0;
		double txnAmount=0;
		
		
		
		%>
		
		                   <!-- <div class="col-md-2 col-sm-4 col-xs-12 form-group"></div> --> 
                           
								</form>
				
				</div>
			</div>
		</div>	
	
		
<%-- 	    <div class="col-md-12">	
			<div id="xyz" class="box2" style="margin-top: 10px;
			">
					<table id="examplea" class="table table-bordered table-striped">
					<thead>
					
					
						<tr>
							<th style="text-align: center;color:#111;">Total Successful Txn Count</th> 
							<th style="text-align: center;color:#111;"><u>Total Successful Txn Amount</th> 
						</tr>
					</thead>
					<tbody>
			
			<tr>
						<td style="text-align: center;"><%=d.format(txnCount)%></td>
			          	     <td style="text-align: center;"><%=d.format(txnAmount)%></td>
			             
			            
	                  	</tr>
				     
				        </tbody>		</table>
			</div>
        </div> --%>
        
        
		<div class="col-md-12">					
			<div id="xyz">
				<table id="example" class="scrollD cell-border dataTable">
					<thead>
<!-- 				Sr.No	Agent ID	Agent Name	Date & Time	Type	Txn ID	Mobile Numer	Bank Name	Aadhar No	Payment Amount	Commission	Transaction Status	RRN number	
 -->					
						
						<tr>
							<th width="1%" style="word-wrap: break-word;"><u>Sr.No</u></th>
							<th width="9%" style="word-wrap: break-word;"><u>Txn Date</u></th>
							<%if(user.getUsertype() != 2){ %>
							<th width="5%" style="word-wrap: break-word;"><u>Agent Id</u></th>
							<%} %>
							<th width="5%" style="word-wrap: break-word;"><u>Txn Id</u></th>
							<th width="5%" style="word-wrap: break-word;"><u>Txn Type</u></th>
							<th width="5%" style="word-wrap: break-word;"><u>Account Number</u></th>
							<th width="5%"><u>Txn Ref No</u></th>
							<th width="5%" style="word-wrap: break-word;"><u>Amount</u></th>
							<%if(user.getUsertype() != 2){ %>
							<th width="5%" style="word-wrap: break-word;"><u>Charges</u></th>
							<%} %>
							<th width="5%" style="word-wrap: break-word;"><u>Status</u></th>
							<th width="5%" style="word-wrap: break-word;"><u>Bank Txn Id</u></th>
							<% if(user.getWhiteLabel()==0 && (user.getUsertype()==4 || user.getUsertype()==6)){	%>
							<th width="5%" style="word-wrap: break-word;display:none;"><u>Aggregator ID</u></th>
							<%}%>
						</tr>
					
					</thead>
					<tbody>
					<%
					/* List<AEPSLedger> list=(List<AEPSLedger>)request.getAttribute("agentList"); */

					if(list!=null){
					
						int j=1;
					for(int i=0; i<list.size(); i++) {
						AepsSettlementBean tdo=list.get(i);
						
						%>

			          	 <tr>
			          	 <td><%=j++ %></td>
			          	 <td ><%=tdo.getSettlementDate()%></td>
			          	 <%if(user.getUsertype() != 2){ %>
			             <td><%=tdo.getAgentId()%></td>
			             <%} %>
			             <td><%=tdo.getRespTxnId()!=null?tdo.getRespTxnId():"-"%></td>
			             <td><%=tdo.getPaymentMode()!=null?tdo.getPaymentMode():"-"%></td>
			             <td><%=tdo.getAccountNumber()!=null?tdo.getAccountNumber():"-"%></td>
			             <td><%=tdo.getRrn()!=null?tdo.getRrn():"-"%></td>
			             <td><%=tdo.getAmount()%></td>
			              <%if(user.getUsertype() != 2){ %>
			             <td><%=tdo.getCharges()%></td>
			             <%} %>
			             <td><%=tdo.getPaymentStatus()!=null?tdo.getPaymentStatus():"-"%></td>
			          	 <td><%=tdo.getBankTransactionId()!=null?tdo.getBankTransactionId():"-"%></td>
			          	  <%if(user.getWhiteLabel()==0 && (user.getUsertype()==4 || user.getUsertype()==6)){	%>
			             <td style="word-wrap: break-word;display:none;"><%=tdo.getAggregatorId()!=null?tdo.getAggregatorId():"-" %></td>
            			 <%}%>
			            
	                  	</tr>
							  
				      <%} }%>	
				        </tbody>		</table>
			</div>
        </div>

 
 

</div>
        
        

        <!-- contents ends -->

       
        </div>
    </div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


<div id="newAgentBox" class="modal fade" role="dialog">
  <div class="modal-dialog model-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agent Details</h4>
      </div>
      <div class="modal-body">
        <p>Loading....</p>
      </div>
    
    </div>

  </div>
</div>
<script>
function getAgentDetails(agentId,aggrId){

	$.ajax({
		  type: "POST",
		  url: "AgentDetailsView",
		  data: "walletBean.aggreatorid="+aggrId+"&walletBean.id="+agentId,
		  success: function(result){
			  
			  $("#newAgentBox .modal-body").html(result)
		  },
		  
		});
	
}

</script>


</body>
</html>



