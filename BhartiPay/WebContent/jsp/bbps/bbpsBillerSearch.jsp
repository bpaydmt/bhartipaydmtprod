<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../theams.jsp"></jsp:include>
<jsp:include page="../reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
<link rel="stylesheet" type="text/css" href="./css/bbps/style.css" />
<!-- <link rel="stylesheet" type="text/css" href="./css/bbps/bootstrap-dialog.min.css" /> -->
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
<script type="text/javascript" src="./js/bbps/jquery-3.3.1.min.js"></script>

<style>

  .col-7-width a { 
    border: none;
    color: #5d5757; 
    text-align: center;
    text-decoration: none;
    display: inline-block;  
    cursor: pointer;
  }



  .borderfortabbutton{
    border: 2px solid #ccc;
        padding: 10px 10px 25px 10px;
  }
  .errorMsg {
    right: 12%;
    top: 57px;
  }

  .chosen-container {
    width: 90% !important;
  }

  .chosen-container-single .chosen-single {
    padding: 9px 0 8px 8px !important;
  }

  .chosen-container-active.chosen-with-drop .chosen-single div b {
    background-position: -18px 4px;
  }

  .col-7-width {
    border-top: none !important;
    border-left: none !important;
    border-right: none !important;
    border-bottom: 2px solid #ccc;
    background-color: transparent !important;
    color: #666 !important;
    border-radius: 25px !important;
    display: block !important;
      float: left !important;
      width: 100% !important;
      /*padding-top: 10px !important;*/
       /* padding-bottom: 10px !important;*/
       padding: 7px;
       text-align: center;
  }

  .tab {
    display: block;
  }

  .tab:hover {
    color: #666 !important;
  }
  .dd .ddcommon .borderRadius {
    /*z-index: 1000 !important;*/
  }
  .bank-txt{
    margin-bottom: 10px; 
    margin-top: 10px;
  }
  .dd { 
        line-height: 10px;
        width: 100%!important;
  }
  .newRechargeUi .payplutus_tab_active a:hover, 
  .newRechargeUi .payplutus_tab_active a, 
  .newRechargeUi .payplutus_tab_active a:focus{
        position: relative;
        top: 11px;
  }
  .newRechargeUi .payplutus_tab a{
        position: relative;
        top: 11px;    
  }



  #OnlyForTAbAnchor .nav>li>a:focus, .smart-style-3 .nav>li>a{
  color: #111 !important;
  background-color: transparent !important;
  }

  #OnlyForTAbAnchor .nav-tabs {
      border-bottom: 0px solid #ddd !important;
  }

  #OnlyForTAbAnchor .nav-tabs>li>a:hover {
      border-color: transparent !important;
      border-top: 1px solid transparent !important;
  }

  #OnlyForTAbAnchor .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
  color: #111 !important;
  font-weight:bold;
   box-shadow: 0 0px 0 #eb843b !important;
   background-color: transparent !important;
   border: 0px solid #ddd !important;
   padding-top: 0 !important;
  }
  
  
  .popup-overlay-other1{ 
    visibility:hidden;
    position:fixed; 
    width:50%;
    height:50%;
    left:25%;
    top: 38%; 

  }
  .overlay-other1 {
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left: 0;
      background: rgba(0,0,0,.6);
      /* z-index: 100000; */
     }

  .popup-overlay-other1.active{ 
    visibility:visible; 
    z-index: 999; 
  }

  .popup-content-other1 { 
   	visibility:hidden;
  }

  .popup-content-other1.active { 
    visibility:visible;
  }

  .box-login-title-other1{
      top: 30%;
      color: #111;
      position: absolute;
	  width: 50%;
	  height: auto;
	  left: 25%;  
      background-color:rgba(255, 255, 255, 0.8);
      padding-top: 10px;
      padding-bottom: 10px; 
  }
  .MessageBoxMiddle-other1 {
      position: relative;
      left: 20%;
      width: 60%;
  }
  .close-other1 {
       float: none; 
       font-size: 16px; 
       font-weight: bold; 
      line-height: 1;
      color: #fff;
      text-shadow:none; 
      opacity:1; 
      background: #a57225!important;
      padding: 10px; 
  }
  .popup-btn-other1{
      font-size: 16px;
      font-weight: bold;
      color: #fff;
      background: #a57225!important;
      padding: 7px;
      /* cursor: pointer; */
  }
  .MessageBoxMiddle-other1 .MsgTitle-other1 {
      letter-spacing: -1px;
      font-size: 24px;
      font-weight: 300;
  }
  .txt-color-orangeDark-other1 {
      color: #a57225!important;
  }
  .MessageBoxMiddle-other1 .pText-other1 {
    font-size: 16px;
    text-align: center;
    margin-top: 14px;
  }

  .MessageBoxButtonSection-other1 span {
      float: right;
      margin-right: 7px;
      padding-left: 15px;
      padding-right: 15px;
      font-size: 14px;
      font-weight: 700;
  } 
  
  
.popup-overlay-other1 input[type="text"]{
    border-bottom-color: #111 !important; 
        margin-bottom: 15px;
        color: #111 !important;
}
</style>

</head>

<body>
    <!-- topbar starts -->
<jsp:include page="../header.jsp"></jsp:include>
    <!-- topbar ends -->
        
        <!-- left menu starts -->
<jsp:include page="../mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div class="bbpsnotifies" id="bbpsnoti"></div>
<div id="main" role="main"> 
	<input id="totalBalance" type="hidden"/>
	
	<div id="content">       
	    <div class="row"> 
            <div class="col-md-12">
            	<div class="newRechargeUi no-chosen-drop" style="margin-top:20px">
	            	<div class="bbpsloader" id="loaderDiv"></div>
		            	<!--Creates the popup body-->
					      <div class="popup-overlay-other1 overlay-other1" style="display: block;">  
					        <div class="popup-content-other1"> 
					         <div class='box-login-title-other1'> 
					           <div class="MessageBoxMiddle-other1">  
					            	<span class="MsgTitle-other1" style="color:orange;font-size:25px;font-weight:bold;">Do you want to pay?</span>
						            <div style="text-align:center;margin-bottom: 10px;">
						            	<div id="popupInfoDetails"> </div>
							           <button type="button" class="btn btn-success" id="payPopUpYes">Yes</button> 
							           <button type="button" class="close-other1 btn btn-primary" onclick="payPopUpNo();" style="padding: 6px 12px;">No</button>
						            </div> 
					          </div>
					         </div>   
					        </div>
					      </div> 
					   <!-- Popup End -->
	            	
	            	<!--Creates the popup body-->
				<%-- 	      <div class="popup-overlay-other1 overlay-other1" style="display: block;">  
					        <div class="popup-content-other1"> 
					         <div class='box-login-title-other1'> 
					           <div class="MessageBoxMiddle-other1">  
					            	<span class="MsgTitle-other1" style="color:orange;font-size:25px;font-weight:bold;">Do you want to pay?</span>
						            <div style="text-align:center;margin-bottom: 10px;">
						            	<div id="popupConfirmation"> </div>
							           <!-- <button type="button" class="btn btn-success" id="payPopUpYes">Yes</button>  -->
							           <button type="button" class="close-other1 btn btn-primary" onclick="popupOk();" style="padding: 6px 12px;">OK</button>
						            </div> 
					          </div>
					         </div>   
					        </div>
					      </div>  --%>
					   <!-- Popup End -->
	            	
	            	<div  id="OnlyForTAbAnchor"  class="box-content col-lg-12 col-md-12 col-sm-12 col-xs-12 borderfortabbutton">

						<div class="content-header">
							<ol class="breadcrumb">
								<i><img src="./image/BBPSLOGO.png" style="width:39%; float: right;"></i>
							</ol>
						</div>
		                <div id="divbillerCat" ></div>

					    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 tab-content" style="padding-left: 40px;padding-top: 21px;"> 
					    
					    <div id="billerParamPane">
					    <!-- <div class="loaderContainer" id="loaderDiv" style="display: none">
					    	<img alt="loading" src="./images/bbps/ring_loader.gif" align="center">
					    </div> -->
					    
					    <div class="row">
					    <!--BILLER START-->
						<div class="col-sm-4 " id="biller" style="display: none">
							<div class="form-group">
								<label>Biller</label> <select class="form-control" id="BillerLists" onchange="showCustomerInfoArea(this);"></select>
							</div>
						</div>
						<!--BILLER END-->
						<!--SUB BILLER START-->
						<div class="col-sm-4 " id="subBiller" style="display: none">
							<div class="form-group">
								<label>Sub Biller</label> <select class="form-control" id="SubBillerLists" onchange="subBillerSelection(this);">
								</select>
							</div>
						</div>
						<!--SUB BILLER END -->
						</div>

				    	 
						<!-- BOX START -->
							<div class="box box-primary" id="customerInfoArea" style="display: none">
								<!-- FORM START -->
								<form action="#" id="quickpayform" >
									<!-- BOX TITLE or SECTION TITLE START -->
									<div class="box-header with-border">
										<h3 class="box-title no-margin">My Detail</h3>
									</div>
									<!-- BOX TITLE or SECTION TITLE END -->
									<!-- BOX BODY START -->
									<div class="box-body">
										<!-- ROW START -->
										<div class="row">
											<!-- MOBILE FIELD START -->
											<div class="col-sm-3">
												<div class="form-group">
													<label>Mobile No.</label><span class="mandatory-font">*</span>
													<input id="inputMobile" onblur="validateMobileNumber();"
														type="text" />
												</div>
											</div>
											<!-- MOBILE FIELD END -->
											<!--EMAIL FIELD START-->
											<div class="col-sm-3">
												<div class="form-group">
													<label>Email</label> <input id="inputEmail" onblur="validateEmail();" placeholder="Enter Email" type="text" />
												</div>
											</div>
											<!--EMAIL FIELD END-->
										</div>
										<!-- ROW END -->
										<!-- SECTION FOR CUSTOMER INFORMATION - BOX - START -->
										<!-- SUB SECTION HEAD START -->
										<div class="box-header-inside with-border">
											<h3 class="box-title-inside no-margin no-padding">Consumer Detail</h3>
										</div>
										<!-- SUB SECTION HEAD END -->
										<!-- ROW START -->
										<div class="row" id="customerinfobox">
											<!-- PAYMENT CHANNEL START -->
				
											<div class="col-sm-3 " id="quickPayChannel" style="display:none">
												<div class="form-group">
													<label>Payment Channel</label> <input id="quickPaymentChannel" name="quickPaymentChannel" readonly="readonly" type="hidden" value="Internet" /> <input id="quickPaymentChannelAlias" name="quickPaymentChannelAlias" readonly="readonly" type="text" value="Internet" />
												</div>
											</div>
											<!-- PAYMENT CHANNEL END -->
											<!-- PAYMENT METHOD START -->
											<div class="col-sm-3 " style="display:none">
												<div class="form-group">
													<label>Payment Modes</label> '
													<select class="form-control" id="quickPaymentmethod" name="quickPaymentmethod">
														<option value="">-- Select Payment Mode --</option>
														<option selected value="Cash">Cash</option>
													</select>
												</div>
											</div>
											<!-- PAYMENT METHOD END -->
											<!-- Quick Pay Amount START -->
											<div class="col-sm-3 quickpayboxes" style="display:none">
												<div class="form-group">
													<label>Quick Pay Amount</label> <!-- <input id="quickPayAmount"
														max="9999999.99" maxlength="10" min="0" name="quickPayAmount"
														onblur="validateAmount(this); calculateCCFQuickPay();"
														oninput="convertToDecimalAndUpdateMinMaxVal(this)"
														placeholder="Enter Amount" type="text" /> -->
														<input id="quickPayAmount" max="9999999.99" maxlength="10" min="0" name="quickPayAmount" placeholder="Enter Amount" type="text" />
				
												</div>
											</div>
											<!-- Quick Pay Amount END -->
											<!-- CCF + Tax(es) START -->
											<div class="col-sm-3 " style="display:none">
												<div class="form-group">
													<label>CCF + Tax(es)</label> <span
														class="fa fa-info-circle tool-tip" data-placement="bottom"
														data-toggle="tooltip" id="taxBreakupQP" style="display: none;"></span>
													<input id="quickPayCCF" name="quickPayCCF"
														placeholder="CCF + Tax(es)" type="text" value="10"/>
													<input id="quickPayCcf" name="quickPayCcf" type="hidden" /> <input
														id="quickPayCouccf" name="quickPayCouccf" type="hidden" />
				
												</div>
											</div>
											<!-- CCF + Tax(es) END -->
											<!-- TOTAL AMOUNT to be pain - START -->
											<div class="col-sm-3 " style="display:none">
												<div class="form-group">
													<label>Net Payable Amount</label> <input id="quickPayTotal"
														min="1.00" name="quickPayTotal" placeholder="Amount"
														type="text" />
												</div>
											</div>
											<!-- TOTAL AMOUNT to be pain - END -->
											<!-- FORM END -->
										</div>
										<!-- ROW END -->
										<!-- SECTION FOR CUSTOMER INFORMATION - BOX - END -->
										<!-- ALERT MESSAGE BOX START -->
										<!-- ERROR MSG START -->
										<div class="errorbox alert alert-danger" style="display: none">
											<span class="erormsg" id="billFetchErrorDecs"> </span>
										</div>
										<!-- ERROR MSG END -->
										<!-- ALERT MESSAGE BOX END -->
									</div>
									<!-- BOX BODY END -->
									<!--BOX FOOTER START-->
									<div class="alert alert-warning" id="billerDescInCustInfoSection"
										style="margin: 0 15px; font-size: .7em; font-weight: bold;">
										<i aria-hidden="true" class="fa fa-info-circle"
											style="font-size: 1.5em;"></i> <span id="billerDescInCustInfo"></span>
									</div>
									<div class="box-footer row no-margin">
										<button class="btn btn-primary waves-effect waves-light pull-right"
											id="btnFetchBill" onclick="confirmFetchBill(this)" type="button">Fetch Bill</button>
										<button class="btn btn-primary waves-effect waves-light pull-right"
											id="btn_quick_pay" onclick="qPayPopUp(this, true);" type="button">Quick Pay</button>
										<button class="btn btn-primary waves-effect waves-light pull-right"
										id="btn_quick_pay_cancel" onclick="cancelNow();" type="button">Cancel</button>
									</div>
									<!--BOX FOOTER END-->
								</form>
								<!-- FORM END -->
							</div>
							<!-- BOX END --> 
							
							<!-- SECTION FOR BILL INFOEMATION - START -->
				             <!-- BOX START -->
			             	 <div class="box box-primary" id="billInfoArea" style="width: 150%;margin-top: 100px;display:none">
				             <!-- FORM (Bill Information) START -->
						     <!-- <form action="#" id="paybillform">	 -->	<!-- onsubmit="return appendPayForm(); -->
						      <input id="refid" name="refid" type="hidden" />
						       <input id="billerResponseTags" name="billerResponseTags" type="hidden" />
						        <!-- BOX TITLE or SECTION TITLE START -->
						        <div class="box-header with-border">
						           <h3 class="box-title no-margin">Bill information</h3>
						        </div>
						        <!-- BOX TITLE or SECTION TITLE END -->
						        <!-- BOX BODY START -->
						        <div class="box-body">
						           <div class="row">
						              <!-- CUSTOMER NAME STARt -->
						              <div class="col-sm-3">
						                 <div class="form-group">
						                    <label>Customer Name</label>
						                    <input class="form-control" id="inputCustomerName" name="inputCustomerName" readonly="readonly" type="text" />
						                 </div>
						              </div>
						              <!-- CUSTOMER NAME END -->
						              <!-- CUSTOMER MOBILE STARt -->
						              <div class="col-sm-3">
						                 <div class="form-group">
						                    <label>Customer Mobile</label>
						                    <input class="form-control" id="inputCustomerMobile" name="inputCustomerMobile" readonly="readonly" type="text" />
						                 </div>
						              </div>
						              <!-- CUSTOMER MOBILE END -->
						              <!-- BILLER NAME START -->
						              <div class="col-sm-3">
						                 <div class="form-group">
						                    <label>Biller Name</label>
						                    <input class="form-control" id="inputBillerName" name="inputBillerName" readonly="readonly" type="text" />
						                    <input id="inputBillerNameID" type="hidden" />
						                 </div>
						              </div>
						              <!-- BILLER NAME END -->
						              <!-- BILL DATE START -->
						              <div class="col-sm-3">
						                 <div class="form-group">
						                    <label>Bill Date</label>
						                    <input class="form-control" id="inputBillDate" name="inputBillDate" readonly="readonly" type="text" />
						                 </div>
						              </div>
						              <!-- BILL DATE END -->
						              <!-- BILL NUMBER START -->
						              <div class="col-sm-3">
						                 <div class="form-group">
						                    <label>Bill Number</label>
						                    <input class="form-control" id="inputBillNumber" name="inputBillNumber" readonly="readonly" type="text" />
						                 </div>
						              </div>
						              <!-- BILL NUMBER END -->
						              <!-- BILL PERIOD START-->
						              <div class="col-sm-3">
						                 <div class="form-group">
						                    <label>Bill Period</label>
						                    <input class="form-control" id="inputBillPeriod" name="inputBillPeriod" readonly="readonly" type="text" />
						                 </div>
						              </div>
						              <!-- BILL PERIOD END -->
						              <!-- DUE DATE START -->
						              <div class="col-sm-3">
						                 <div class="form-group">
						                   <label>Due Date</label>
						                    <input class="form-control" id="inputDueDate" name="inputDueDate" readonly="readonly" type="text" />
						                 </div>
						              </div>
						              <!-- DUE DATE END -->
						              <!-- BILL AMOUNT START -->
						              <div class="col-sm-3" id="insertionPoint">
						                 <div class="form-group">
						                    <label>Bill Amount</label>
						                    <input class="form-control" id="inputBillAmount" name="inputBillAmount" readonly="readonly" type="text" />
						                 </div>
						              </div>
						              <!-- BILL AMOUNT END -->
						              <!-- PAYMENT CHANNEL START -->
						              <div class="col-sm-3">
						                 <div class="form-group">
						                    <label>Payment Channel</label>
						                    <input class="form-control" id="inputPaymentChannel" name="inputPaymentChannel" readonly="readonly" type="hidden" value="Internet" />
						                   <input class="form-control" id="inputPaymentChannelAlias" name="inputPaymentChannelAlias" readonly="readonly" type="text" value="Internet" />
						                  
						                 </div>
						              </div>
						              <!-- PAYMENT CHANNEL END -->
						              <!-- PAYMENT METHOD START -->
						              <div class="col-sm-3 ">
						                 <div class="form-group">
						                    <label>Payment Modes</label><span class="mandatory-font">*</span>
						                    <select class="form-control" id="paymentmethod" name="paymentmethod">
						                       <option value="">-- Select Payment Mode--</option>
						                       <!-- <option value="Cash">Cash</option> -->
						                       <option selected value="Cash">Cash</option>
						                    </select>
						                 </div>
						              </div>
						              <!-- PAYMENT METHOD END -->
						              <!-- PAYMENT EXACTNESS START -->
						              <div class="col-sm-3" id="payment_exactness">
						                 <div class="form-group">
						                    <label>Payment Exactness</label>
						                    <input class="form-control" id="billerexactness" name="billerexactness" readonly="readonly" type="text" />
						                 </div>
						              </div>
						              <!-- PAYMENT EXACTNESS END -->
						              <!-- AMOUNT OPTION START -->
						              <div class="col-sm-3 ">
						                 <div class="form-group">
						                    <label>Amount Option</label><span class="mandatory-font">*</span>
						                    <select class="form-control" id="billerAmountOptions" name="billerAmountOptions" onChange="otherPayment(checkVal());">
						                    
						                    </select>
						                 </div>
						              </div>
						              <!-- AMOUNT OPTION END -->
						              <!-- OTHER PAY AMOUNT START -->
						              <div class="col-sm-3 " id="exact_amount">
						                 <div class="form-group">
						                 <label>Payment Amount</label><span class="mandatory-font">*</span>
						                    <input class="form-control" id="billerexactness_amnt" max="9999999.99" maxlength="10" min="0" name="billerexactness_amnt" readonly onblur="validateAmount(this,false);" oninput="convertToDecimalAndUpdateMinMaxVal(this)" placeholder="Enter Amount" type="text" />
						                 </div>
						              </div>
						              <!-- OTHER PAY AMOUNT END -->
						              <!-- CCF & TAX(es) START -->
						              <div class="col-sm-3">
						                 <div class="form-group">
						                    <label>CCF + Tax(es)</label>
						                    <span class="fa fa-info-circle tool-tip" data-placement="bottom" data-toggle="tooltip" id="taxBreakup" style="display:none;"></span>
						                    <input class="form-control" id="inputCCFTax" name="inputCCFTax" placeholder="CCF + Tax(es)" readonly="readonly" type="text" />
						                     <input id="inputccf" name="inputccf" type="hidden" />
						                      <input id="inputcouccf" name="inputcouccf" type="hidden" />
						                 </div>
						              </div>
						              <!-- CCF & TAX(es) END -->
						              <!-- TOTAL AMOUNT to be PAID START -->
						              <div class="col-sm-3">
						                 <div class="form-group">
						                    <label>Net Payable Amount</label>
						                    <input class="form-control" id="inputTotalAmtToPaid" min="1.00" name="inputTotalAmtToPaid" placeholder="Amount" readonly="readonly" type="text" />
						                 </div>
						              </div>
						              <!-- TOTAL AMOUNT to be PAID END -->
						           </div>
						           <div class="errorbox alert alert-danger" id="paymentErrorDecsDiv" style="display:none">
						            <span class="erormsg" id="paymentErrorDecs"> </span>
						       </div>
						        </div>
						               <!-- BOX BODY END -->
						        <!--BOX FOOTER START-->
						        <div class="alert alert-warning" id="billerDescInBillInfoSection" style="margin: 0px 15px; font-size: 0.7em; font-weight: bold; display: none !important;">
						           <i aria-hidden="true" class="fa fa-info-circle" style="font-size: 1.5em;"></i>
						           <span id="billerDescInBillInfo"></span> 
						        </div>
						        <div class="box-footer row no-margin">
						           <button class="btn btn-primary waves-effect waves-light pull-right" id="btn_bill_pay" onclick="qPayPopUp(this, false);">Pay Now</button>
						           <button class="btn btn-primary waves-effect waves-light pull-right" id="btn_bill_pay_cancel" onclick="cancelNow();">Cancel</button>
						        </div>
						        <!--BOX FOOTER END-->
						     <!-- </form> -->
						     <!-- FORM (Bill Information) END -->
							</div>
							<!-- BOX END -->
							<!-- SECTION FOR BILL INFOEMATION - END -->
							
							<!-- SECTION FOR BILL PAYMENT RESPONSE - START -->
							<!-- BOX START -->
							<div class="box box-primary" id="billPrintArea" style="width:100%;display:none;">
							<input id="printRefid" name="printRefid" type="hidden" />
							<input id="billerPrintResponseTags" name="billerPrintResponseTags" type="hidden" />
							<!-- BOX TITLE or SECTION TITLE START -->
							<div class="box-header with-border" style="height: 70px;">
								<table id="printPaymentHeader" width="100%">
									<tr>
										<td width="33%" style="text-align:left;"><img id="merchantLogo" style="width:20%; display:none;"></td>
							   			<td width="33%" style="text-align:center;"><h3 class="box-title no-margin">Bill Receipt</h3></td>
							   			<td width="33%" style="text-align:right;"><img id="bbpsLogo" src="./image/BBPSLOGO.png" style="width:10%; display:none;"></td>
							   		</tr>
							   	</table>
							</div>
							<!-- BOX TITLE or SECTION TITLE END -->
							<!-- BOX BODY START -->
							<div class="box-body">
							
							   <div class="row123">
							   <table id="printPayment" width="100%" style="border: 1px solid black;border-collapse: collapse;" >
							   
							   	<tr id="printAgentTR" style="display:none;">
									<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Shop Name</td>
										<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printShopName"></span></td>
								    <td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Agent Mobile</td>   
										<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printAgentMobile"></span></td>
								</tr>
							   	
							   	<tr>
									<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Customer Mobile</td>
										<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printCustomerNumber"></span></td>
								    <td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Customer Name</td>   
										<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printCustomerName"></span></td>
								</tr>
							   
							   <tr>
									<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Biller Name</td>    
										<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printBillerName"></span></td>
								    <td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Biller ID</td>   
										<td width="30%" style="text-align:left;border: 1px solid black;border: 1px solid black;"><span id="printBillerID"></span></td>
								</tr>
							   
							   <tr>
									<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Bill Date</td>    
										<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printBillDate"></span></td>
								    <td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Due Date</td>   
										<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printDueDate"></span></td>
								</tr>
							   
							   <tr>
									<td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Bill Number</td>    
										<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printBillNumber"></span></td>
								    <td width="20%" style="text-align:left; font-weight: bold;border: 1px solid black;">Consumer Number</td>   
										<td width="30%" style="text-align:left;border: 1px solid black;"><span id="printConsumerNumber"></span></td>
								</tr>
								</table><br>
								
								<table id="printPayment2" width="100%" style="border: 1px solid black;border-collapse: collapse;" >
								
									<tr style="font-weight: bold;">
										<td width="15%" style="text-align:left;border: 1px solid black;">Order ID</td>
										<td width="20%" style="text-align:left;border: 1px solid black;">Transaction ID</td>
										<td width="18%" style="text-align:left;border: 1px solid black;">Bill Amount</td>
										<td width="20%" style="text-align:left;border: 1px solid black;">Date & Time</td>
										<td width="30%" style="text-align:left;border: 1px solid black;">Transaction Status</td>
									</tr>
									<tr>
										<td width="15%" style="text-align:left;border: 1px solid black;"><span id="printOrderID"></span></td>
										<td width="20%" style="text-align:left;border: 1px solid black;"><span id="printTransactionID"></span></td>
										<td width="18%" style="text-align:left;border: 1px solid black;"><span id="printBillAmount"></span></td>
										<td width="20%" style="text-align:left;border: 1px solid black;"><span id="printTxnDateTime"></span></td>
										<td width="30%" style="text-align:left;border: 1px solid black;color:green;"><span id="printTxnStatus"></span></td>
									</tr>
								</table>
								
								<table>
									<tr>
										<td width="30%">
								            <img src="./image/BharatAssured.png" style="width:35%;">
										</td>    
										<td width="70%" style="text-align:right; font-style: italic;"><!-- Powered by Bhartipay Services Pvt. Ltd. --></td>
										
									</tr>
								</table>
								<table id="removePrint" class="scrollD cell-border no-footer" width="100%" style="border-top: 1px solid #fafafa;" >
									<tr>
										<td width="25%"></td>
										<td width="25%"><button class="btn btn-primary waves-effect waves-light pull-right" id="btn_bill_print" onclick="printNow();">Print</button></td>
										<td width="25%"><button class="btn btn-primary waves-effect waves-light pull-left" id="btn_cancel_print" onclick="cancelNow();">Cancel</button></td>
								   		<td width="25%"></td>
								   	</tr>
							   	</table>
							   	</div>
								</div>
						  		<!-- BOX BODY END -->
							</div>
							<!-- BOX END -->
							<!-- SECTION FOR BILL PAYMENT RESPONSE - END -->
							
					      </div>
				        </div>
			        </div>
                </div>
            </div>
        </div>
	</div> 
</div><!--/.fluid-container-->


<script type="text/javascript">
		$(document).ready(function() {
			getTotalBalance();
			getBillerCategory();
			//showNotify(true,'Hi Bpay');
		});
	</script>


<jsp:include page="../footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<script type="text/javascript" src="./js/bbps/paybill.js"></script>
<script type="text/javascript" src="./js/bbps/bootstrap-dialog.min.js"></script>
<script type="text/javascript" src="./js/bbps/jquery-3.3.1.min.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>