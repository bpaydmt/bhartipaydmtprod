<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../theams.jsp"></jsp:include>
<jsp:include page="../reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
<link rel="stylesheet" type="text/css" href="./css/bbps/style.css" />
<link rel="stylesheet" type="text/css" href="./css/bbps/bootstrap-dialog.min.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
<script type="text/javascript" src="./js/bbps/jquery-3.3.1.min.js"></script>




<style>

  .col-7-width a { 
    border: none;
    color: #5d5757; 
    text-align: center;
    text-decoration: none;
    display: inline-block;  
    cursor: pointer;
  }



  .borderfortabbutton{
    border: 2px solid #ccc;
        padding: 10px 10px 25px 10px;
  }
  .errorMsg {
    right: 12%;
    top: 57px;
  }

  .chosen-container {
    width: 90% !important;
  }

  .chosen-container-single .chosen-single {
    padding: 9px 0 8px 8px !important;
  }

  .chosen-container-active.chosen-with-drop .chosen-single div b {
    background-position: -18px 4px;
  }

  .col-7-width {
    border-top: none !important;
    border-left: none !important;
    border-right: none !important;
    border-bottom: 2px solid #ccc;
    background-color: transparent !important;
    color: #666 !important;
    border-radius: 25px !important;
    display: block !important;
      float: left !important;
      width: 100% !important;
      /*padding-top: 10px !important;*/
       /* padding-bottom: 10px !important;*/
       padding: 7px;
       text-align: center;
  }

  .tab {
    display: block;
  }

  .tab:hover {
    color: #666 !important;
  }
  .dd .ddcommon .borderRadius {
    /*z-index: 1000 !important;*/
  }
  .bank-txt{
    margin-bottom: 10px; 
    margin-top: 10px;
  }
  .dd { 
        line-height: 10px;
        width: 100%!important;
  }
  .newRechargeUi .payplutus_tab_active a:hover, 
  .newRechargeUi .payplutus_tab_active a, 
  .newRechargeUi .payplutus_tab_active a:focus{
        position: relative;
        top: 11px;
  }
  .newRechargeUi .payplutus_tab a{
        position: relative;
        top: 11px;    
  }



  #OnlyForTAbAnchor .nav>li>a:focus, .smart-style-3 .nav>li>a{
  color: #111 !important;
  background-color: transparent !important;
  }

  #OnlyForTAbAnchor .nav-tabs {
      border-bottom: 0px solid #ddd !important;
  }

  #OnlyForTAbAnchor .nav-tabs>li>a:hover {
      border-color: transparent !important;
      border-top: 1px solid transparent !important;  /* // Removehover  */
  }

  #OnlyForTAbAnchor .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
  color: #111 !important;
  font-weight:bold;
   box-shadow: 0 0px 0 #eb843b !important;
   background-color: transparent !important;
   border: 0px solid #ddd !important;
   padding-top: 0 !important;
  }
  

</style>



</head>

<body>

    <!-- topbar starts -->
<jsp:include page="../header.jsp"></jsp:include>
    <!-- topbar ends -->
        
        <!-- left menu starts -->
<jsp:include page="../mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div class="bbpsnotify" id="bbpsnoti"></div>
<div id="main" role="main"> 
	
	<!-- <input id="agentId" type="hidden" value="CC01CC01513515340681" /> -->
	<div id="content">       
	    <div class="row"> 
            <div class="col-md-12">
	            <!-- <div style="margin-top: 15px;margin-bottom: 10px; ">
	                <img src="./image/BBPSLOGO.png" style="width:4%; float: right;">
	            </div> -->
	            <div class="newRechargeUi no-chosen-drop" style="margin-top:20px">
	            <div class="bbpsloader" id="loaderDiv"></div>
	            
	            	<div  id="OnlyForTAbAnchor"  class="box-content col-lg-12 col-md-12 col-sm-12 col-xs-12 borderfortabbutton">
	            	
	            		<!-- <div class="box-header-inside with-border">
	            			
            			</div> -->
            			<div class="content-header">
            			<h1 class="box-title-inside no-margin no-padding">Search Transaction</h1>
							<ol class="breadcrumb">
								<i><img src="./image/BBPSLOGO.png" style="width:39%; float: right;"></i>
							</ol>
						</div>
            			
            			<div id="txnFormDiv" style="margin-top: 100px;">
            				<!-- <form action="#" onsubmit="return Validate();"> -->
	            				<div>
	            					<span>Select</span>
			                        <input id="rdbtn1" type="radio" name="rdbtn1" onclick="selectTxnStatus(this)" value="TRANS_REF_ID" checked="checked"/>
			                        <strong>Transaction Reference ID</strong>
			                        <%-- <input id="rdbtn2" type="radio" name="rdbtn1" onclick="selectTxnStatus(this)" value="TRANS_MOBILE" >
			                        <strong>Mobile Number</strong> --%>
	            				</div>
	            				<div class="clear"></div>
			                    <div id="div_RefiD" class="formDetails" ><!-- style="display:none"> -->
			                        <span>Transaction Reference ID<strong>&nbsp;*</strong></span>
			                        <input name="txttxnrefid" type="text" id="txttxnrefid" style="width: 30%; margin-top:15px"/><br>
			                    <!-- </div>
			                    <div> -->
				                    <input type="button" name="btnSubmit1" value="Search"  onclick="txtStatus();" id="btnSubmit1" style="margin-top:30px"/>
				                </div>
				                
				                <div class="row transaction-type" id="div_MobileD" style="display: none">
											<!--FORM FIELD START-->
											<div class="col-sm-3">
												<div class="form-group">
													<label>Mobile No.</label><span class="mandatory-font">*</span>
													<!--   <input type='text' id='txnmobile' name='mobileNum'
			                                    maxlength="10" onchange="validateMobileNumber('txnmobile')">-->
													<input id="txnmobile" maxlength="10" name="mobileNum" placeholder="Enter Mobile Number" type="text" />
												</div>
											</div>
											<!--FORM FIELD END-->
											<!--FORM FIELD START-->
											<div class="col-sm-3">
												<div class="form-group">
													<label>From Date</label><span class="mandatory-font">*</span> 
													<input id="fromdt" name="fromdt" onchange="setMinDate(this);" placeholder="DD-MM-YYYY" type="text" />
												</div>
											</div>
											<!--FORM FIELD END-->
											<!--FORM FIELD START-->
											<div class="col-sm-3">
												<div class="form-group">
													<label>To Date</label><span class="mandatory-font">*</span> <input id="todt" name="todt" placeholder="DD-MM-YYYY" type="text" />
												</div>
											</div>
											<!--FORM FIELD END-->
											<!--OTP SECTION BOX START -->
											<div class="col-sm-12" id="otpBox_txn_mob">
											<div class="row">
											<div class="col-sm-3">
													<div class="form-group">
														<label id="tran_otp_label">Enter OTP</label> <input id="otp_txn_mob" max="999999" maxlength="6" min="000000" minlength="6" onchange="validateOTPLength(this);" oninput="convertToNumberAndUpdateMinMaxVal(this)" placeholder="OTP" type="password" />
													</div>
												</div>
												<!-- FORM FIELD START -->
											<div class="col-sm-9">
													<div class="form-group">
			
														<button class="btn btn-primary" data-keyboard="false" id="tran_gen_otp_txn_mob" onclick="generateOTP('tran_txn_mob',this);" type="button">Generate OTP</button>
														<button class="btn btn-primary" disabled="disabled" id="tran_resend_otp_txn_mob" onclick="resendOTPReq('tran_txn_mob');" type="button">Resend OTP</button>
													</div>
												</div>
													<!-- FORM FIELD END -->
											</div>
											<div class="alert alert-danger errorMsg" id="tranOtp_errormsg_txn_mob" style="display: none;">
													<span id="tranOtp_errormsgcont_txn_mob"> </span>
										    </div>	
										    <input type="button" name="btnSearch" value="Search"  onclick="mobStatus();" id="btnSearch"/>
										    </div>
											</div>
				                
			                <!-- </form> -->
            			</div>
            			<div class="error">
		                    <span id="lblerror" class="errorDetails" style="color: #FF0000;"></span>
		                </div>
		                
		                <div id="txnSuccessStatusRespDiv"></div>
		                
	            	</div>
	            </div>
            	
            </div>
        </div>
	</div> 
</div>

<jsp:include page="../footer.jsp"></jsp:include>






<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<script type="text/javascript" src="./js/bbps/paybill.js"></script>
<script type="text/javascript" src="./js/bbps/bootstrap-dialog-min.js"></script>
<script type="text/javascript" src="./js/bbps/jquery-3.3.1.min.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>