<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../theams.jsp"></jsp:include>
<jsp:include page="../reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
<link rel="stylesheet" type="text/css" href="./css/bbps/style.css" />
<link rel="stylesheet" type="text/css" href="./css/bbps/bootstrap-dialog.min.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
<script type="text/javascript" src="./js/bbps/jquery-3.3.1.min.js"></script>




<style>

  .col-7-width a { 
    border: none;
    color: #5d5757; 
    text-align: center;
    text-decoration: none;
    display: inline-block;  
    cursor: pointer;
  }



  .borderfortabbutton{
    border: 2px solid #ccc;
        padding: 10px 10px 25px 10px;
  }
  .errorMsg {
    right: 12%;
    top: 57px;
  }

  .chosen-container {
    width: 90% !important;
  }

  .chosen-container-single .chosen-single {
    padding: 9px 0 8px 8px !important;
  }

  .chosen-container-active.chosen-with-drop .chosen-single div b {
    background-position: -18px 4px;
  }

  .col-7-width {
    border-top: none !important;
    border-left: none !important;
    border-right: none !important;
    border-bottom: 2px solid #ccc;
    background-color: transparent !important;
    color: #666 !important;
    border-radius: 25px !important;
    display: block !important;
      float: left !important;
      width: 100% !important;
      /*padding-top: 10px !important;*/
       /* padding-bottom: 10px !important;*/
       padding: 7px;
       text-align: center;
  }

  .tab {
    display: block;
  }

  .tab:hover {
    color: #666 !important;
  }
  .dd .ddcommon .borderRadius {
    /*z-index: 1000 !important;*/
  }
  .bank-txt{
    margin-bottom: 10px; 
    margin-top: 10px;
  }
  .dd { 
        line-height: 10px;
        width: 100%!important;
  }
  .newRechargeUi .payplutus_tab_active a:hover, 
  .newRechargeUi .payplutus_tab_active a, 
  .newRechargeUi .payplutus_tab_active a:focus{
        position: relative;
        top: 11px;
  }
  .newRechargeUi .payplutus_tab a{
        position: relative;
        top: 11px;    
  }



  #OnlyForTAbAnchor .nav>li>a:focus, .smart-style-3 .nav>li>a{
  color: #111 !important;
  background-color: transparent !important;
  }

  #OnlyForTAbAnchor .nav-tabs {
      border-bottom: 0px solid #ddd !important;
  }

  #OnlyForTAbAnchor .nav-tabs>li>a:hover {
      border-color: transparent !important;
      border-top: 1px solid transparent !important;  /* // Removehover  */
  }

  #OnlyForTAbAnchor .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
  color: #111 !important;
  font-weight:bold;
   box-shadow: 0 0px 0 #eb843b !important;
   background-color: transparent !important;
   border: 0px solid #ddd !important;
   padding-top: 0 !important;
  }
  

</style>



</head>

<body>

    <!-- topbar starts -->
<jsp:include page="../header.jsp"></jsp:include>
    <!-- topbar ends -->
        
        <!-- left menu starts -->
<jsp:include page="../mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div class="bbpsnotify" id="bbpsnoti"></div>
<div id="main" role="main"> 
	<input id="agentId" type="hidden" value="CC01CC01513515340681" />
	<div id="content">       
	    <div class="row"> 
            <div class="col-md-12">
	            <!-- <div style="margin-top: 15px;margin-bottom: 10px; ">
	                <img src="./image/BBPSLOGO.png" style="width:4%; float: right;">
	            </div> -->
	            <div class="newRechargeUi no-chosen-drop" style="margin-top:20px">
	            	<div  id="OnlyForTAbAnchor"  class="box-content col-lg-12 col-md-12 col-sm-12 col-xs-12 borderfortabbutton">
	            	
	            		<div class="bbpsloader" id="loaderDiv"></div>
						
	            		<div class="content-header">
							<h1>Check Complaint Status</h1>
							<ol class="breadcrumb">
								<li><i class="fa fa-star mandatory-font"></i> Mandatory Input Field</li>
								<i><img src="./image/BBPSLOGO.png" style="width:20%; float: right;"></i>
							</ol>
						</div>
						
						<!-- BOX START -->
						<div class="box box-primary" style="margin-top:110px">
						<!-- BOX BODY START -->
						<div class="box-body">
						<!-- ROW START -->
						<div class="row">
							<!-- FORM FIELD START -->
							<div class="col-sm-3">
								<div class="form-group">
									<div class="required">
										<label>Type of Complaint</label>
										<span class="mandatory-font">*</span>
										<div class="select-style">
											<div class="select-style"><select class="form-control" id="complainttype_status" onchange="resetComplaint()" required="required">
												<option value="">Select Complaint Type</option>
												<option value="Transaction">Transaction Type</option>
												<option value="Service">Service Type</option>
											</select></div>
										</div>
									</div>
								</div>
							</div>
							<!--FORM FIELD END-->
							<!--FORM FIELD START-->
							<div class="col-sm-3">
								<div class="form-group">
									<div class="required">
										<label class="active">Complaint ID</label>
										<span class="mandatory-font">*</span> <input id="complaintid_status" maxlength="15" onkeyup="validateAlphaNumeric(this)" placeholder="Enter Complaint ID" required="required" type="text">
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<button class="btn btn-primary waves-effect waves-light pull-left alignDown" onclick="statusCheckRequest()" type="button">Check Status</button>
							</div>
							<!--FORM FIELD END-->
						</div>
						<!--ROW END-->
						
						<div class="error" id="complaintErrorDiv">
		                    <span id="complaintError" class="errorDetails" style="color: #FF0000;"></span>
		                </div>
						
						<div id="complaintSuccessRespDiv"></div>
						
						</div>
						</div>
						
	            	</div>
	            </div>
            </div>
        </div>
	</div> 
</div><!--/.fluid-container-->



<script type="text/javascript">
        
        function Validate() {
            //debugger;
            
            var checkedrdbtn1 = document.getElementById('rdbtn1').checked;
            if ((checkedrdbtn1 == true) && (document.getElementById('txttxnrefid').value == "")) {
                $('#complaintError').text("Please enter complaint id.");
                return false;
            }
            /*else if ((document.getElementById('txttxnrefid').value != "") && (document.getElementById('txttxnrefid').value.length != 12)) {
                $('#lblerror').text("Transaction reference id is not valid.");
                checkedrdbtn1 = false;
                return false;
            }
             else if ((checkedrdbtn1 == false) && (document.getElementById('txt_MobNumber').value == "")) {
                $('#lblerror').text("Please enter mobile Number.");
                checkedrdbtn2 = false;
                return false;
            }
            else if ((document.getElementById('txt_MobNumber').value != "") && (document.getElementById('txt_MobNumber').value.length != 10)) {
                $('#lblerror').text("mobile Number is not valid.");
                return false;
            }
            else if (((document.getElementById('txt_MobNumber').value != "") && (document.getElementById('txttxnrefid').value == "")) && ((document.getElementById('txt_FrmDt').value == "") || (document.getElementById('txt_ToDt').value == ""))) {
                $('#lblerror').text("Please Enter Date.");
                return false;
            } */
            else {
                $('#lblerror').text("");
                return true;
            }
        }
</script>

<jsp:include page="../footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<script type="text/javascript" src="./js/bbps/paybill.js"></script>
<script type="text/javascript" src="./js/bbps/bootstrap-dialog-min.js"></script>
<script type="text/javascript" src="./js/bbps/jquery-3.3.1.min.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>