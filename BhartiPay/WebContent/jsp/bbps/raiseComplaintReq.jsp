<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../theams.jsp"></jsp:include>
<jsp:include page="../reqFiles.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
<link rel="stylesheet" type="text/css" href="./css/bbps/style.css" />
<link rel="stylesheet" type="text/css" href="./css/bbps/bootstrap-dialog.min.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
<script type="text/javascript" src="./js/bbps/jquery-3.3.1.min.js"></script>




<style>

  .col-7-width a { 
    border: none;
    color: #5d5757; 
    text-align: center;
    text-decoration: none;
    display: inline-block;  
    cursor: pointer;
  }



  .borderfortabbutton{
    border: 2px solid #ccc;
        padding: 10px 10px 25px 10px;
  }
  .errorMsg {
    right: 12%;
    top: 57px;
  }

  .chosen-container {
    width: 90% !important;
  }

  .chosen-container-single .chosen-single {
    padding: 9px 0 8px 8px !important;
  }

  .chosen-container-active.chosen-with-drop .chosen-single div b {
    background-position: -18px 4px;
  }

  .col-7-width {
    border-top: none !important;
    border-left: none !important;
    border-right: none !important;
    border-bottom: 2px solid #ccc;
    background-color: transparent !important;
    color: #666 !important;
    border-radius: 25px !important;
    display: block !important;
      float: left !important;
      width: 100% !important;
      /*padding-top: 10px !important;*/
       /* padding-bottom: 10px !important;*/
       padding: 7px;
       text-align: center;
  }

  .tab {
    display: block;
  }

  .tab:hover {
    color: #666 !important;
  }
  .dd .ddcommon .borderRadius {
    /*z-index: 1000 !important;*/
  }
  .bank-txt{
    margin-bottom: 10px; 
    margin-top: 10px;
  }
  .dd { 
        line-height: 10px;
        width: 100%!important;
  }
  .newRechargeUi .payplutus_tab_active a:hover, 
  .newRechargeUi .payplutus_tab_active a, 
  .newRechargeUi .payplutus_tab_active a:focus{
        position: relative;
        top: 11px;
  }
  .newRechargeUi .payplutus_tab a{
        position: relative;
        top: 11px;    
  }



  #OnlyForTAbAnchor .nav>li>a:focus, .smart-style-3 .nav>li>a{
  color: #111 !important;
  background-color: transparent !important;
  }

  #OnlyForTAbAnchor .nav-tabs {
      border-bottom: 0px solid #ddd !important;
  }

  #OnlyForTAbAnchor .nav-tabs>li>a:hover {
      border-color: transparent !important;
      border-top: 1px solid transparent !important;  /* // Removehover  */
  }

  #OnlyForTAbAnchor .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
  color: #111 !important;
  font-weight:bold;
   box-shadow: 0 0px 0 #eb843b !important;
   background-color: transparent !important;
   border: 0px solid #ddd !important;
   padding-top: 0 !important;
  }
  

</style>



</head>

<body>

    <!-- topbar starts -->
<jsp:include page="../header.jsp"></jsp:include>
    <!-- topbar ends -->
        
        <!-- left menu starts -->
<jsp:include page="../mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div class="bbpsnotify" id="bbpsnoti"></div>
<div id="main" role="main"> 

	<div id="content">       
	    <div class="row"> 
            <div class="col-md-12">
	            
	            <div class="newRechargeUi no-chosen-drop" style="margin-top:20px">
	            	<div  id="OnlyForTAbAnchor"  class="box-content col-lg-12 col-md-12 col-sm-12 col-xs-12 borderfortabbutton">
	            	
	            		<div class="bbpsloader" id="loaderDiv"></div>
	            		
	            		<div class="content-header">
							<h1>Raise Transaction Related Issue</h1>
							<ol class="breadcrumb">
								<li><i class="fa fa-star mandatory-font"></i> Mandatory Input Field</li>
								<i><img src="./image/BBPSLOGO.png" style="width:20%; float: right;"></i>
							</ol>
						</div>
						
						<div id="txnRaiseCompaintRespDiv" style="margin-top:100px"></div>
						
						<!-- BOX START -->
						<div class="box box-primary">
						<!-- BOX BODY START -->
						<div class="box-body" id="boxBodyDiv">
						
							<div class="error" id="complaintErrorDiv">
			                    <span id="complaintError" class="errorDetails" style="color: #FF0000;"></span>
			                </div>
			                
			                <div >
			                
							<!-- BOX BODY START -->
							<div >	<!-- class="box-body" -->
								<div class="row">
									<!-- COMPLAINT TYPE - FIELD - START -->
									<div class="col-sm-4">
										<div class="form-group">
											<label>Type of Complaint</label> <div class="select-style"><select class="form-control" onchange="selectComplaintType(this)">
												<option value="">Select Complaint Type</option>
												<option value="transaction">Transaction Type</option>
												<!-- <option value="service">Service Type</option> -->
											<!-- Remove service type complaint from dropdown -->
											</select></div>
										</div>
									</div>
									<!-- COMPLAINT TYPE - FIELD - END -->
									<!-- SERVICE TYPE COMPLAINT - FIELD - START -->
									<div class="col-sm-4 " id="serviceType" style="display :none;" >
										<div class="form-group">
											<label>Service Type Complaint</label> <div class="select-style"><select class="form-control" id="service_com_type" onchange="selectServiceCompType(this)">
												<option value="">Select Participation Type</option>
												<option value="agent">Agent</option>
												<option value="biller">Biller</option>
												<!-- <option value="system">System</option> -->
											</select></div>
										</div>
									</div>
									<!-- SERVICE TYPE COMPLAINT - FIELD - END -->
								</div>
							</div>
							<!-- BOX BODY END -->
							</div>
			                
			                <!-- SECTION FOR - TRANSACTION TYPE COMPLAINT - START -->
			                
		                	<div>
								<!-- BOX START -->
								<div class="box box-primary" id="transactionType" style="display: none;">
									<!--BOX or SECTION HEADER / TITLE START -->
									<div class="box-header with-border">
										<h3 class="box-title no-margin">Transaction Type Complaint</h3>
									</div>
									<!--BOX or SECTION HEADER / TITLE END -->
									<!-- BOX BODY START -->
									<div class="box-body" id="srch_txn">
									<!-- RADIO BUTTON START -->
									      <div style="margin-bottom:20px;">
									      <!-- <input checked="" id="radioMobile" name="_transaction" onclick="selectTxnSearch(this)" type="radio" value="_transMobile" />
										  <label style="padding-right:20px;">Mobile No.</label> -->
									         
									      <input checked id="radioRefId" name="_transaction" onclick="selectTxnSearch(this)" type="radio" value="_transRefID" />
									      <label>Transaction Reference Id</label>
									      </div>
									  <!-- RADIO BUTTON END -->
									  <div class="row transaction-type" id="_transMobile" style="display: none">
											<!--FORM FIELD START-->
											<div class="col-sm-3">
												<div class="form-group">
													<label>Mobile No.</label><span class="mandatory-font">*</span>
													<!--   <input type='text' id='txnmobile' name='mobileNum'
			                                    maxlength="10" onchange="validateMobileNumber('txnmobile')">-->
													<input id="txnmobile" maxlength="10" name="mobileNum" placeholder="Enter Mobile Number" type="text" />
												</div>
											</div>
											<!--FORM FIELD END-->
											<!--FORM FIELD START-->
											<div class="col-sm-3">
												<div class="form-group">
													<label>From Date</label><span class="mandatory-font">*</span> 
													<input id="fromdt" name="fromdt" onchange="setMinDate(this);" placeholder="DD-MM-YYYY" type="text" />
												</div>
											</div>
											<!--FORM FIELD END-->
											<!--FORM FIELD START-->
											<div class="col-sm-3">
												<div class="form-group">
													<label>To Date</label><span class="mandatory-font">*</span> <input id="todt" name="todt" placeholder="DD-MM-YYYY" type="text" />
												</div>
											</div>
											<!--FORM FIELD END-->
											<!--OTP SECTION BOX START -->
											<div class="col-sm-12" id="otpBox_txn_mob">
											<div class="row">
											<div class="col-sm-3">
													<div class="form-group">
														<label id="tran_otp_label">Enter OTP</label> <input id="otp_txn_mob" max="999999" maxlength="6" min="000000" minlength="6" onchange="validateOTPLength(this);" oninput="convertToNumberAndUpdateMinMaxVal(this)" placeholder="OTP" type="password" />
													</div>
												</div>
												<!-- FORM FIELD START -->
											<div class="col-sm-9">
													<div class="form-group">
			
														<button class="btn btn-primary" data-keyboard="false" id="tran_gen_otp_txn_mob" onclick="generateOTP('tran_txn_mob',this);" type="button">Generate OTP</button>
														<button class="btn btn-primary" disabled="disabled" id="tran_resend_otp_txn_mob" onclick="resendOTPReq('tran_txn_mob');" type="button">Resend OTP</button>
													</div>
												</div>
													<!-- FORM FIELD END -->
											</div>
											<div class="alert alert-danger errorMsg" id="tranOtp_errormsg_txn_mob" style="display: none;">
													<span id="tranOtp_errormsgcont_txn_mob"> </span>
										    </div>	
										    </div>
											</div>
											<div class="row transaction-type" id="_transRefID">
												<!--FORM FIELD START-->
												<div class="col-sm-4">
													<div class="form-group">
														<label>Transaction Reference Id</label> <span class="mandatory-font">*</span>
														<input id="txnrefid" maxlength="20" name="txnrefid" placeholder="Enter Transaction Reference Id" type="text" />
				
													</div>
												</div>
												<!--FORM FIELD END-->
												
												<!-- FORM FIELD START -->
												<div class="col-sm-3">
													<div class="form-group">
														<label class="control-label">Transaction Complaint Reason</label>
														<span class="mandatory-font">*</span>
														 <select class="form-control" id="transactionReason" required="required">
															<option value="">Select Complaint Reason</option>
															<option value="1">Transaction Successful, account not updated</option>
															<option value="2">Amount deducted, biller account credited but transaction ID not received</option>
															<option value="3">Amount deducted, biller account not credited &amp; transaction ID not received</option>
															<option value="4">Amount deducted multiple times</option>
															<option value="5">Double payment updated</option>
															<option value="6">Erroneously paid in wrong account</option>
															<option value="7">Others, provide details in description</option>
														 </select>
													</div>
												</div>
												<!-- FORM FIELD END -->
												<!-- FORM FIELD START -->
												<div class="col-sm-6">
													<div class="form-group">
														<label>Complaint Description</label>
														<span class="mandatory-font">*</span> 
														<input id="transactionComplaintServDesc" maxlength="200" onblur="validateDescAlphNumeric(this);" placeholder="Enter Complaint Description" type="text" />
													</div>
												</div>
												<!-- FORM FIELD END -->
												
												<!-- OTP SECTION BOX START -->
												<div class="col-sm-12" id="otpBox" style="display:none;">
													<div class="row">
														<!-- FORM FIELD START -->
														<div class="col-sm-3">
															<div class="form-group">
																<label id="tran_otp_label">Enter OTP</label> <input id="otp_tran_type_comp" max="999999" maxlength="6" min="000000" minlength="6" onchange="validateOTPLength(this);" oninput="convertToNumberAndUpdateMinMaxVal(this)" placeholder="OTP" type="password" />
															</div>
														</div>
														<!-- FORM FIELD END -->
														<!-- FORM FIELD START -->
														<div class="col-sm-9">
															<div class="form-group">
				
																<button class="btn btn-primary" data-keyboard="false" id="tran_gen_otp_txn_type_comp" onclick="generateOTP('tran_type_comp',this);" type="button">Generate OTP</button>
																<button class="btn btn-primary" disabled="disabled" id="tran_resend_otp_txn_type_comp" onclick="resendOTPReq('tran_type_comp');" type="button">Resend OTP</button>
															</div>
															<div class="loader" id="loader" style="display: none"></div>
														</div>
														<!-- FORM FIELD END -->
													</div>
												</div>
												<!-- OTP SECTION BOX END -->
												
											</div>
											
											<!-- ALERT MESSAGE BOX START -->
											<!--ERROR ALERT MESSAGE BOX START-->
											<div class="alert alert-danger" id="errormsg" style="display: none">
												<div id="errormsgcont"></div>
											</div>
											<!--ERROR ALERT MESSAGE BOX END-->
											<!-- <div class="box-body" id="timeoutmsg" style="display:none">
				                                             </div> -->
											<!--TIME-OUT ALERT MESSAGE BOX START-->
											<div class="alert alert-info" id="timeoutmsgdiv" style="display: none">
												<div id="timeoutmsg"></div>
											</div>
											<div class="alert alert-danger errorMsg" id="tranOtp_errormsg_txn_mob_txn_type_comp" style="display: none;">
												<span id="tranOtp_errormsgcont_txn_mob_txn_type_comp"> </span>
											</div>
											<!--TIME-OUT ALERT MESSAGE BOX END-->
											<!-- ALERT MESSAGE BOX END -->
										</div>
										<!-- BOX BODY END -->
										
										<!-- BOX FOOTER START-->
										<div class="box-footer row no-margin">
											<button class="btn btn-primary waves-effect waves-light pull-right" id="btn_srch" onclick="submitTransactionComplaint()" type="button">Submit</button>
											<button class="btn btn-primary waves-effect waves-light pull-right" id="btn_reset" onclick="removeSearchRecord()" type="button">Reset</button>	
										</div>
										<!-- BOX FOOTER END-->
										
									  </div>
								  </div>
							  </div>
						  	</div>
						  	
						  	<!-- SECTION FOR - RESULT / LISTING - TRANSACTION TYPE COMPLAINT - START -->
							<!-- BOX START -->
							<div class="box box-primary" id="txnlist" style="display: none;" >
								<!-- BOX BODY START -->
								<div class="box-body">
									<div>
										<table id="Transaction-Seaerch" style="width: 100%;">
											<thead>
												<tr>
													<th style="width:16%">Transaction Reference Id</th>
													<th style="width:22%">Biller Name</th>
													<!-- <th style="padding-left:6px;width:12%;">Biller Name</th> -->
													<th style="width:11%">Payment Amount(INR)</th>
													<th style="width:10%">Status</th>
													<th style="width:16%">Date</th>
													<th style="width:24%">Action</th>
												</tr>
											</thead>
		
											<tbody class="searchResults" id="searchresult">
											</tbody>
		
										</table>
									</div>
									<!-- ALERT MESSAGE BOX START -->
									<!-- SUCCESS ALERT MESSAGE BOX START -->
									<div class="alert alert-success" id="msg" style="display: none">
										<div class="row" id="successmsg"></div>
									</div>
									<!-- SUCCESS ALERT MESSAGE BOX END -->
									<!-- ALERT MESSAGE BOX END -->
								</div>
								<!-- BOX BODY END -->
							</div>
							<!-- BOX END -->
							<!-- SECTION FOR - RESULT / LISTING - TRANSACTION TYPE COMPLAINT - END -->
			                
			                <!-- SECTION FOR REGISTERING Transaction Type Complaint - START -->
							<!-- BOX START -->
							<div class="box box-primary" id="registertxncomplaint" style="display: none" >
								<!--BOX or SECTION HEADER / TITLE START -->
								<div class="box-header with-border">
									<h3 class="box-title no-margin">Register Transaction Complaint</h3>
								</div>
								<!--BOX or SECTION HEADER / TITLE END -->
								<!-- BOX BODY START -->
								<div class="box-body">
									<div class="row">
										<!-- FORM FIELD START -->
										<div class="col-sm-3">
											<div class="form-group">
												<label>Type of Complaint</label> <input id="complainttype" readonly type="text" />
											</div>
										</div>
										<!-- FORM FIELD END -->
										<!-- FORM FIELD START -->
										<div class="col-sm-3" id="mobbox">
											<div class="form-group ">
												<label>Mobile No.</label>
												<span class="mandatory-font">*</span>
												<input id="complaintmobile" maxlength="10" onchange="validateMobileNumber('complaintmobile')" type="text" /> <input id="srch_by" type="hidden" />
											</div>
										</div>
										<!-- FORM FIELD END -->
										<!-- FORM FIELD START -->
										<div class="col-sm-3 ">
											<div class="form-group ">
												<label>Transaction Reference ID</label> <input id="complainttxnid" readonly="" type="text" />
											</div>
										</div>
										<!-- FORM FIELD END -->
										<!-- FORM FIELD START -->
										<div class="col-sm-3">
											<div class="form-group ">
												<label>Disposition</label>
												<span class="mandatory-font">*</span> 
												<select class="form-control" id="complaintdisp" required="required">
												</select>
											</div>
										</div>
										<!-- FORM FIELD END -->
										<!-- FORM FIELD START -->
										<div class="col-sm-6">
											<div class="form-group">
												<label>Complaint Description</label>
												<span class="mandatory-font">*</span>
												  <textarea id="complaintdesc" maxlength="200" onblur="validateDescAlphNumeric(this);" placeholder="Enter Description" style="resize:none" type="text"></textarea>
											</div>
										</div>
										<!-- FORM FIELD END -->
									</div>
									<div class="alert alert-danger errorMsg" id="tranOtp_errormsg" style="display: none;" >
										<span id="tranOtp_errormsgcont"> </span>
									</div>
								</div>
								<!-- BOX BODY END -->
								<!--BOX FOOTER START -->
								<div class="box-footer row no-margin">
									<button class="btn btn-primary waves-effect waves-light pull-right" disabled="disabled" id="tran_comp_sub" onclick="submitTransactionComplaint()" type="button">SUBMIT</button>
								</div>
								<!-- BOX FOOTER END -->
							</div>
							<!-- BOX END -->
							<!-- SECTION FOR REGISTERING Transaction Type Complaint - END -->
			                <!-- SECTION FOR - TRANSACTION TYPE COMPLAINT - END -->
			                
			                <!-- SECTION FOR - SERVICE TYPE COMPLAINT - START -->
			                <div>
							<!-- BOX START -->
							<div class="box box-primary" id="registerservicecomplaint" style="display: none" >
								<!--BOX or SECTION HEADER / TITLE START -->
								<div class="box-header with-border">
									<h3 class="box-title no-margin">Register Service Complaint</h3>
								</div>
								<!--BOX or SECTION HEADER / TITLE END -->
								
								<!-- BOX BODY START -->
								<div class="box-body">
									<div class="row">
										<!-- FORM FIELD START -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">Type of Complaint</label> <input class=" " id="complaintServtype" readonly="" type="text" />
											</div>
										</div>
										<!-- FORM FIELD END -->
										<!-- FORM FIELD START -->
										<div class="col-sm-3">
											<div class="form-group">
												<label>Participation Type</label> <input id="participatetype" readonly="" type="text" />
											</div>
										</div>
										<!-- FORM FIELD END -->
										<!-- FORM FIELD START -->
										<div class="col-sm-3" id="divagentid" style="display: none" >
											<div class="form-group">
												<label>Agent ID</label>
												<span class="mandatory-font">*</span>
												<input id="complaintagentid" maxlength="20" onblur="validateAgentId(this);" placeholder="Enter Agent ID" type="text" />
											</div>
										</div>
										<!-- FORM FIELD END -->
										<!-- FORM FIELD START -->
										<div class="col-sm-3" id="divbillerid" style="display: none" >
											<div class="form-group">
												<label>Biller Name</label>
												<span class="mandatory-font">*</span>
												<input id="complaintbillerid" maxlength="20" placeholder="Biller Name" type="text" />
											</div>
										</div>
										<!-- FORM FIELD END -->
										<!-- FORM FIELD START -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">Service Reason</label>
												<span class="mandatory-font">*</span>
												 <select class="form-control" id="servicereason" required="required">
												</select>
											</div>
										</div>
										<!-- FORM FIELD END -->
										<!-- FORM FIELD START -->
										<div class="col-sm-6">
											<div class="form-group">
												<label>Complaint Description</label>
												<span class="mandatory-font">*</span> 
												<input id="complaintServdesc" maxlength="200" onblur="validateDescAlphNumeric(this);" placeholder="Enter Complaint Description" type="text" />
											</div>
										</div>
										<!-- FORM FIELD END -->
										<!-- FORM FIELD START -->
										<%-- <div class="col-sm-3">
											<div class="form-group">
												<label>Mobile No.</label> 
												 <span class="mandatory-font">*</span><input id="otp_svc_mobile" maxlength="10" onblur="enableSvcGenOTPBtn();" placeholder="Enter Mobile Number" type="text" />
											</div>
										</div> --%>
										<!-- FORM FIELD END -->
										
										<!-- OTP SECTION START -->
										<div class="col-sm-9">
											<div class="row" id="otpBoxService" style="display:none;">
												<!-- FORM FIELD START -->
												<div class="col-sm-3">
													<div class="form-group">
														<label id="tran_otp_label">Enter OTP</label>
														<span class="mandatory-font">*</span> <input id="svc_otp" max="999999" maxlength="6" min="000000" minlength="6" onchange="validateOTPLength(this);" oninput="convertToNumberAndUpdateMinMaxVal(this)" placeholder="OTP" type="password" />
													</div>
												</div>
												<!-- FORM FIELD END -->
												<div class="col-sm-9" style="margin-top: 17px;">
													<div class="form-group">
														<button class="btn btn-primary waves-effect waves-light" data-keyboard="false" disabled="disabled" id="svc_gen_otp" onclick="generateOTP('svc',this);" type="button">Generate OTP</button>
														<button class="btn btn btn-primary waves-effect waves-light" disabled="disabled" id="svc_resend_otp" onclick="resendOTPReq('svc');" type="button">Resend OTP</button>
													</div>
												</div>
												
											</div>
										</div>
										<!-- OTP SECTION END -->
										</div>
											<div class="alert alert-danger" id="svcOtp_errormsg" style="display: none;">
																	<span id="svcOtp_errormsgcont"> </span>
											</div>
											<div class="alert alert-danger" id="errormsg" style="display: none">
												<div id="errormsgcont"></div>
											</div>
										</div>
										<!-- BOX BODY END -->
										<!--BOX FOOTER START-->
										<div class="box-footer row no-margin">
											<button class="btn btn-primary waves-effect waves-light pull-right" id="servicecomplaintbtn" onclick="submitServiceComplaint()" type="button">SUBMIT</button>
				
										</div>
										<!--BOX FOOTER END-->
									</div>
									<!-- BOX END -->
								</div>
										
			                
			                <!-- SECTION FOR - SERVICE TYPE COMPLAINT - END -->
			                
			                
							<div id="complaintSuccessRespDiv"></div>
						
						</div>
						</div>
						
	            	</div>
	            </div>
            </div>
        </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="../footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<script type="text/javascript" src="./js/bbps/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="./js/bbps/bootstrap-dialog-min.js"></script>
<script type="text/javascript" src="./js/bbps/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="./js/bbps/paybill.js"></script>

<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>