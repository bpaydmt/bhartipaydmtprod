<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PaymentGatewayTxnBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.ReconciliationReport"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.ReconciliationDetailsBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.ReconciliationSummaryBean"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
 $(document).ready(function() {
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Payment Gateway Report - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Payment Gateway Report - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Payment Gateway Report - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Payment Gateway Report - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Payment Gateway Report - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    $('#dp').datepicker({
      	 language: 'en',
      	maxDate: new Date(),
      	
      });
     $("#dp").blur(function(){
     	//alert()
     	  $('#dp1').datepicker({
     	     	 language: 'en',
     	     	 minDate: new Date($('#dp').val()),
     	     	maxDate: new Date(),
     	     });	
     })
    
} );
 </script>
 

</head>

<% 
DecimalFormat d=new DecimalFormat("0.00");
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));

ReconciliationReport map=(ReconciliationReport)request.getAttribute("mapObject");

ReconciliationSummaryBean summary=null; 
List<PaymentGatewayTxnBean> details=null;

if(map!=null){
 summary=map.getReconciliationSummaryBean(); 
 details=map.getPaymentGatewayTxnList();
}
%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->

        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  
        
        
      
            

 
 
 
<div class="box2">
	<div class="box-inner">
	
	
	
		<div class="box-header">
			<h2>Payment Gateway Report</h2>
		
		</div>
		
		<div class="box-content">
		
		<form action="PGReconciliationReport.action" method="post">
			 <div class="row"> 

								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
																		
										<input type="text"
										value="<s:property value='%{bean.stDate}'/>"
										name="bean.stDate" id="dp"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required >
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									 <input
										type="text" value="<s:property value='%{bean.endDate}'/>"
										name="bean.endDate" id="dp1"
										class="form-control datepicker-here1" placeholder="End Date"
										data-language="en" required >
								</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left">
									
									<div align="center" id="wwctrl_submit">
										<input class="btn btn-sm btn-block btn-success" id="submit"
											type="submit" value="Submit">
									</div>
								</div>
							</div>
							</form>
		</div>
		   <%if(summary!=null){ %>
		 <div class="content-block" style="padding:0 0 10px 0;">       
                <h3 class="title">Reconciliation Summary </h3>
             
		<div class="row" style="position: relative;top: -9px;">
		<div class="col-md-4"><h4>Total transaction: <%=summary.getTotalTxn() %></h4></div>
				<div class="col-md-4"><h4>Total Balance: <%if(summary.getTotalAmount()>0){out.print(d.format(summary.getTotalAmount()));}else{out.print("0.00");}%> </h4></div>
		
		
<h4 > </h4>
</div>

                </div>
                <%} %>
	</div>
	</div>	
		
		

							
		<div id="xyz">
			<table id="example" class="display">
				<thead>
					<tr>
						<th><u>User Id</u></th>
						<th><u>Txn Id</u></th>
						<th><u>PG Txn Id</u></th>
						<th><u>Amount</u></th>
						<th><u>Status</u></th>
						<th><u>Mobile</u></th>
						<th><u>Eamil</u></th>
						<th><u>Date</u></th>
						
					</tr>
				</thead>
				<tbody>
				<%
				if(details!=null){
				for(int i=0; i<details.size(); i++) {
					PaymentGatewayTxnBean tdo=details.get(i);%>
		          	<tr>
			         <td><%=tdo.getUserId()%></td> 
			         <td><%=tdo.getTrxId()%></td>
			         <td><%=tdo.getPgId()%></td>
		          <td><%=d.format(tdo.getTrxAmount())%></td>
		          <td><%=tdo.getTrxResult()%></td>
		          <td><%=tdo.getMobileNo()%></td>
		          <td><%=tdo.getEmail()%></td>
		          <td><%=tdo.getPgtxnDate()%></td>
           		  </tr>
			      <%} }%>	
			        </tbody>		</table>
		</div>
 

 


  		</div>
    </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



