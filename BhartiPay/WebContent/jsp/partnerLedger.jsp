<%@page import="com.bhartipay.wallet.report.bean.PartnerLedgerBean"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>


<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="gridJs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
 <script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>
 
  

  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
    $(document).ready(function() {

	  $("#partnerid").multipleSelect({
	         filter: true,
	         placeholder:"Select Application(s)"
	     }); 
	
	    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Partner Ledger - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Partner Ledger - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Partner Ledger - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Partner Ledger - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Partner Ledger - ' + '<%= currentDate %>',
              
            },{
                extend: 'colvis',
                columnText: function ( dt, idx, title ) 
                {
                    return (idx+1)+': '+title;
                }
            }
        ]
    } );
   
  

	    $('#dpStart').datepicker({
	     language: 'en',
	     autoClose:true,
	     maxDate: new Date(),
	    
	    });
	    if($('#dpStart').val().length != 0){
	    
	     $('#dpEnd').datepicker({
	           language: 'en',
	           autoClose:true,
	           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	           maxDate: new Date(),
	           
	          }); 
	     
	    }
	    $("#dpStart").blur(function(){
	  
	    $('#dpEnd').val("")
	     $('#dpEnd').datepicker({
	          language: 'en',
	         autoClose:true,
	         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	         maxDate: new Date(),
	         
	        }); 
	    })
	   
	    
	} );


	 function converDateToJsFormat(date) {
	    
	  var sDay = date.slice(0,2);
	  var sMonth = date.slice(3,6);
	  var yYear = date.slice(7,date.length)
	 
	  return sDay + " " +sMonth+ " " + yYear;
	 }
   
 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
//System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));


DecimalFormat d=new DecimalFormat("0.00");

%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

	<div id="main" role="main"> 
	    <div id="content">       
	        <div class="row"> 

 
				<div class="col-md-12"  id="hidethis2"> 

					<div class="box2">
						<div class="box-inner">
						
							<div class="box-header">
								<h2>Partner Ledger</h2>
							</div>
							
							<div class="box-content">
							
								<form action="GetPartnerLedger" method="post">
								    <div class="row">

										<div class="form-group  col-md-3 col-sm-3 txtnew col-xs-4">
											<s:select list="%{partnerList}" headerKey="-1" id="partnerid" name="inputBean.partnerName" cssClass="form-username form-control" multiple="true" required="required" />
										</div>

                                        <div class="form-group  col-md-4 col-sm-3 txtnew col-xs-4">
											<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
											<input type="text" value="<s:property value='%{inputBean.stDate}'/>" name="inputBean.stDate" id="dpStart" class="form-control datepicker-here1" placeholder="Start Date" data-language="en" required/>
										</div>
										<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-4">
											<input type="text" value="<s:property value='%{inputBean.endDate}'/>" name="inputBean.endDate" id="dpEnd" class="form-control datepicker-here2" placeholder="End Date" data-language="en"  required>
										</div>
										<div class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17"> 
											<div  id="wwctrl_submit">
												<input class="btn btn-success" id="submit" type="submit" value="Submit">
												<input class="btn btn-info" id="reset" type="reset" value="Reset">
											</div>
										</div>

                                    </div>
								</form>
							
							</div>
						</div>
					</div>	
						
										
					<div id="xyz">
						<table id="example" class="scrollD cell-border dataTable table table-bordered table-striped" width="100%">
							<thead>
							
							
								<tr>
									<th><u>Partner Name</u></th>
									<th><u>Transaction Date</u></th>
									<th><u>Funds Loaded</u></th>
									<th><u>Funds Consumed</u></th>
									<th><u>Closing Balance</u></th>
									
									
									
															
									
									
								</tr>
							</thead>
							<tbody>
							<%
										List<PartnerLedgerBean> pb=(List<PartnerLedgerBean>)request.getAttribute("listResult");

							if(pb!=null){
							
							
							for(int i=0; i<pb.size(); i++) {
								PartnerLedgerBean tdo=pb.get(i);%>
					          		  <tr>
					         		<td><%=tdo.getPartnername()%></td>
					          	   <td><%=tdo.getTxndate()%></td>
					    
					           <td><%if(tdo.getMoneyload()==0){out.print("0.00");}else{out.print(d.format(tdo.getMoneyload()));}%></td>
					             <td><%if(tdo.getMoneyconsume()==0){out.print("0.00");}else{out.print(d.format(tdo.getMoneyconsume()));}%></td>
					             <td><%=d.format(tdo.getBalances())%></td>
					            
					          
			                  		  </tr>
						      <%} }%>	
						        </tbody>		</table>
					</div> 

				</div>  

			</div> 
	    </div> 
	</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>



