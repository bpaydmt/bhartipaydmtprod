<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.lean.LeanAccount"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="gridJs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/> 


<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
  $(document).ready(function() {

    $('#example').DataTable( {
        dom: 'Bfrtip',
        order: [[ 1, "desc" ]],
        autoWidth: false,
        buttons: [
             
        ]
    } );
        
} );


function converDateToJsFormat(date) {

var sDay = date.slice(0,2);
var sMonth = date.slice(3,6);
var yYear = date.slice(7,date.length)

return sDay + " " +sMonth+ " " + yYear;
}


</script>


<script>

function getPlan(){
	debugger


	    var list=document.getElementById("selectAgent");
	    var aggr=list.options[list.selectedIndex].value;
	    document.getElementById("userId").value=aggr;
	    
	    $.ajax({
			method:'Post',
			cache:0,  		
			url:'AgentPlan',
			data:'userId='+aggr,
			
			success:function(response){
			    var json = JSON.parse(response);
			    var plan=json.planId;
			    document.getElementById("plans").textContent=plan;
			    document.getElementById("planId").value=plan;
			}
		})
	    
	 }


function deleteData()
{
    var myTable = document.getElementById("pricingTable");
	var rowCount = myTable.rows.length;
	for (var x=rowCount-1; x>0; x--) {
	   myTable.deleteRow(x);
	}
}

 

	function addPlan()
	{
	  document.getElementById("pricingTable").style.display = "block";
	  $("#pricingTable").show();

	  if(document.getElementById('pricingTable')){
		deleteData();
		}

       
      $.ajax({
			method:'Get',
			cache:0,  		
			url:'AddNewPlan',
			data:"",
			
			success:function(response){
			    var json = JSON.parse(response);
			     
			    var count=0;
		   		$.each(JSON.parse(response), function(idx, obj) {
		   		   count++;
		   		  $('#pricingTable').find('tbody')
		            .append('<tr><td width="5%">'+count+'</td> <td width="20%" >'+obj.rangeFrom+'</td> <td width="20%">'+obj.rangeTo+'</td><td width="20%"><input type="number" value='+obj.distributorperc+' id="first'+count+'" >   </td><td width="10%"><input type="number" value='+obj.superdistperc+' id="second'+count+'" >  </td> <td align="center">  <input type="button" value="Update" onclick="updateRecord(\'' +count+ '\')" id="updateButtons'+count+'" /></td>  <input type="hidden" value='+obj.commid+' id="third'+count+'" >  <tr>');
		   
		   		});
			}
		})
		
	}

   function updateRecord(id)
   {debugger
	   var dist=document.getElementById("first"+id).value; 
	   var supDist=document.getElementById("second"+id).value; 
	   var rowId=document.getElementById("third"+id).value; 
	   //alert(dist+"    "+supDist);

	   $.ajax({
			method:'Post',
			cache:0,  		
			url:'UpdateNewPlan',
			data:'distributorPer='+dist+'&superDistributorPer='+supDist+'&rowid='+rowId,
			
			success:function(response){
		     var json = JSON.parse(response);
		     var commId=json.commid;
             var message=json.agent;
             if(commId==1)
			    {
			    	
			        $('#msg').html(message); 
				    $("#msg").show();
				    setTimeout(function() { $("#msg").hide(); }, 3000);
			    }else
				  {
			    	 $('#msg').html(message); 
					 $("#msg").show();
					 setTimeout(function() { $("#msg").hide(); }, 3000);
				  }
			}
		})
       
   }
	
    function getPlanId()
    { 
    debugger 

	    var list=document.getElementById("selectAgent");
	    var agent=list.options[list.selectedIndex].value;
	    if(agent=='All')
        {
    	 alert("Please select Agent.");
    	 location.reload();
         return false;
        }else{
	       
    	 var list=document.getElementById("selectPlan");
		 var plan=list.options[list.selectedIndex].value;

	 	 document.getElementById("applyPlan").value=plan;
        }
	}

	
	function applyPlan()
	{debugger
		var userId=document.getElementById("userId").value;
		var plan=document.getElementById("applyPlan").value;

		var list=document.getElementById("selectPlan");
		var pp=list.options[list.selectedIndex].value;
         if(pp=='All')
            {
        	 alert("Please select Plan.");
             return false;
            }
		    
	    $.ajax({
				method:'Post',
				cache:0,  		
				url:'UpdateAgentPlan',
				data:'userId='+userId+'&planId='+plan,
				
				success:function(response){
				    var json = JSON.parse(response);
				    var plan=json.planId;
				    var message=json.agent;
				    var code=json.agentCode;
				 if(code=="1")
				 {
					 document.getElementById("plans").textContent=plan;
				     // document.getElementById("planId").value=plan;newPlan
				      $('#msg').html(plan+" "+message); 
			     	 $("#msg").show();
			      	 setTimeout(function() { $("#msg").hide(); }, 3000);
				 }else
				  {
					  $('#msg').html(message); 
				      $("#msg").show();
				      setTimeout(function() { $("#msg").hide(); }, 3000);
			  	  }    
				
				}
			})
		

	}

	function myfunction()
	{
		document.getElementById("pricingTable").style.display = "none";
		$("#pricingTable").hide();
		
		
	}
	
</script>
 

</head>

<body onload="myfunction()">

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
      
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12">  

<%
    User user = (User) session.getAttribute("User");

%>             
 
<div class="box2">
	<div class="box-inner">
	<div class="box-header">
		<h2>Set Pricing</h2>
	 </div>
	<div class="box-content">
	<div class="row">
	<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
	<div class="row">
	<div class="container">
	<div class="col-sm-4"> 
	<s:select list="%{agentList}" headerKey="All"
				headerValue="Select Agent" onchange="getPlan()" id="selectAgent"
				 cssClass="form-username" requiredLabel="true"/>
				 <input type="hidden"    id="userId"  name="userId" readonly="readonly" /> 
	
	  </div>
	  
	  <div class="col-md-3"> </div>
	   <label><B>Plan ID:-  </B></label> <span id="plans"></span>
      <div class="col-md-2" id="msg" style="color: green ; font-size: 15px;" ></div>
	  <input type="hidden"    id="planId"  name="planId" readonly="readonly" />
	</div><br> 

 
	
	<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
	<div class="row">
	<div class="container">
	<div class="col-md-8"> 
	<s:select list="%{planList}" headerKey="All"
				headerValue="Select Plan" onchange="getPlanId()" id="selectPlan"
				 cssClass="form-username" requiredLabel="true"/> 
				 <input type="hidden"    id="applyPlan"  name="applyPlan" readonly="readonly" />
	
	</div>
	  <div class="col-md-2">
		<input type="button" id="markedAslean" class="btn btn-success btn-fill" style="margin-top: 22px;"
					onclick="applyPlan()" value="Apply Plan"  />
		</div>
		
		 <div class="col-md-2">
		<input type="button" id="markedAslean" class="btn btn-success btn-fill" style="margin-top: 22px;"
					onclick="addPlan()" value="Add New Plan"  />
		</div>
	</div></div>
	
	</div>	</div>
																			
	  <!-- <div class="form-group col-md-3 txtnew col-sm-4 col-xs-6 text-left "> -->
		
		 
        </div>
		</div>
	</div>
	</div>	<br><br>
 
 <table align="center" border="5"    id="pricingTable" class="pricingTable" style="display: block;float: left; ">
	
 
  <thead>  
      <tr>
		<td align="center" style="width: 5%;"><u><b>Sr.No<b></u></td>
		<td align="center" style="width: 15%;"><u><b>From<b></u></td>
		<td align="center" style="width: 15%;"><u><b>To<b></u></td>
		<td align="center" style="width: 20%;"><u><b>Distributor %<b></u></td>
		<td align="center" style="width: 20%;"><u><b>SuperDistributor %<b></u></td>
		<td align="center" style="width: 20%;"><u><b>Action<b></u></td> 
		
	  </tr>
  </thead>				
 
  <tbody>
 
</tbody>
</table>
 
 
 
     </div>
	</div> 
</div><!--/.fluid-container-->

</div></div>
<%-- <jsp:include page="footer.jsp"></jsp:include> --%>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<script src="./js/Pricingdetail.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->



</body>
</html>


