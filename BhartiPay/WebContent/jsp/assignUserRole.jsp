<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
 
 <script type="text/javascript">
 
	function getMenuList(aggId)
	{  
		//alert(aggId);
		$.ajax({
	        url:"GetRoleMenuMapping",
	        cache:0,
	        data:"roleMapping.userId="+aggId,
	        success:function(result){
	     // alert(result);
	               document.getElementById('wconfig').innerHTML=result;
	               
	         }
	  });  
		return false;
	}
	
	
	
	function changeTheme(id){
		 var rd = $(id).find("input:radio"); 
		 rd.prop("checked", true)
		 $('html').attr("class",rd.val())

		}
 </script>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->
<div id="main">
        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
    <div class="box col-md-12" style="margin:0px">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon "></i>User Configuration</h2>

                
            </div>
            <div class="box-content row">
                <div class="col-lg-12 col-md-12"> 
               
		<form action="SaveUserRoleMapping" id="form_name" method="post">
				<div class="row">
				<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
				<input type="hidden" id="cmeLimitHidden" name="roleMapping.amount" />
									<font color="red"><s:actionerror/> </font>
									<font color="blue"><s:actionmessage/> </font>

												<div class="col-md-12">
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Select Sub-aggregator</label>
																	
													<s:select list="%{subAggList}" headerKey="-1"
												headerValue="Please select Sub-aggregator" id="aggregatorid"
												name="roleMapping.userId" cssClass="form-username"
												requiredLabel="true"
												onchange="getMenuList(document.getElementById('aggregatorid').value);"
												 />
														<div class="errorMsg" id="subAggeList">Please select at least one role </div>	
															
														</div>
													</div>
											

            
										<div id="wconfig">
					
													<div class="col-md-4">
														<div class="form-group">
															<label for="apptxt">Menu</label>
													<s:select list="%{aggRoleList}" size="10"
																id="appIdsSelect" name="roleMapping.roleType"
																
																multiple="true" placeholder="Allowed transaction type" />
																
																<div class="errorMsg" style="margin: 153px 0 0 0;" id="assigneeList">Please select at least one role </div>
													
													</div>
													</div>
									</div>
												
            							</div>		

									
									</div>
									
									</form>
									<div class="row"><div class="col-md-12">

			
											
													
													<div class="col-md-4">
										<div class="form-group">
										
										<button  onclick="setCmeLimit()" class="btn btn-info btn-fill"
											 style="margin-top: 20px;">Submit
										</button>

									
									</div>
									</div>


									</div></div>
					

                </div>
                

    
            </div>
        </div>
  
</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       </div>

</div>
</div><!--/.fluid-container-->

<div id="setCmeLimitPop" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:400px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">CME Limit</h4>
      </div>
      <div class="modal-body">
      <div class="row">
        <div class="form-group col-md-12">
      
        
        <input class="form-control onlyNum" id="cmeLimitField" maxlength="6" type="text" />
        </div>
        <div class="errorMsg" style="    position: relative;
    top: 0px;
 
    float: left;
    margin: 0px 0 11px 0;" id="cmeLimitError">CME limit should be more than INR 1 and should be less than 10,00,000 </div>
        <div class="col-md-12">
        	<button class="btn btn-default btn-block" onclick="setLimitConfirm()">Submit</button>
        </div>
        </div>
      </div>
      
    </div>

  </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<script>
function setCmeLimit(){
	var flag = 0;
	var mList = $("#appIdsSelect").val();
	console.log(mList);
	if($("#aggregatorid").val() != "-1" && mList != null){

		$.each(mList,function(index,val){
			
			if(val == "9004"){
				flag = 1

			}		 
		})
		if(flag == 1){
			$("#setCmeLimitPop").modal("show")
		}else{
			
			$("#form_name").submit();
		}

	}else{
		
		if($("#aggregatorid").val() == "-1"){
			$("#subAggeList").show();
		}else{
			$("#subAggeList").hide();
		}		
		if(mList == null){
			$("#assigneeList").show();
		}else{
			$("#assigneeList").hide();
		}
		
		
		
		
	}
}

function setLimitConfirm(){
 var cmeLimit =$("#cmeLimitField").val()
	if(parseInt(cmeLimit) > 999999 || parseInt(cmeLimit) < 0 || cmeLimit.length == 0 ){
		
		$("#cmeLimitError").show();
	}else{
		$("#cmeLimitHidden").val(cmeLimit)
	
		$("#cmeLimitError").hide();
		$("#form_name").submit();
	}
}
</script>

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->




</body>
</html>


