<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%-- <jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
    <script src='./js/highcharts.js'></script>   --%> 
<script type="text/javascript">

</script>

<style>
  .fixed-header #main {
    margin-top: 100px;
  }
</style>

</head>
<% 
DecimalFormat d=new DecimalFormat("0.00");
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
HashMap<String,String> commDetails=(HashMap<String,String>)session.getAttribute("commission");
HashMap<String,String> dashBoard=(HashMap<String,String>)session.getAttribute("dashBoard");
if(commDetails==null){
	commDetails=new HashMap();
}
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
%>
 
<body>
  <div id="main" role="main"> 
    <div class="content">      
     <!--  <div id="content" class="col-lg-9 col-sm-9" style="margin-top:8px;"> --> 

        <div class="">
          <%
            if(user.getUsertype()!=99&&user.getUsertype()!=1&&user.getUsertype()!=2){
            %>    
            <%
            }
            if(user.getUsertype()==2||user.getUsertype()==3||user.getUsertype()==7){
          %>

        	<div class="col-md-3 d-block">
          	<div class="t-block">
            	<h2>Transaction Amount - <strong>Current Day</strong></h2>
            	<p><%=d.format(Double.parseDouble(dashBoard.get("Day").toString())) %></p> 
          	</div>
          </div>
        	<div class="col-md-3 d-block">
        	  <div class="t-block">
            	<h2>Transaction Amount - <strong>This Month</strong></h2>
              <p><%=d.format(Double.parseDouble(dashBoard.get("Month").toString())) %></p>
          	</div>
          </div>

        	<div class="col-md-3 d-block">
          	<div class="t-block">
            	<h2>Transaction Amount - <strong>Last Month</strong></h2>
            	<p><%=d.format(Double.parseDouble(dashBoard.get("lastMonth").toString())) %></p>
          	</div>
          </div>

         	<div class="col-md-3 d-block">
          	<div class="t-block">
            	<h2>Transaction Credit - <strong>Current Day</strong></h2>
            	<p><%=d.format(Double.parseDouble(dashBoard.get("Credit").toString())) %></p> 
          	</div> 
          </div> 
          <!-- <div class="animation-div"> 
            <img alt="" src="./images/rechargeUi.png" id="rechargeUi">
            <img alt="" src="./images/rechargering.png" id="rechargering">
            <img alt="" src="./images/world.png" id="world">
          </div> --> 

        </div>	 
      	
        <%	
        }if(user.getUsertype()==100){}
        %> 

     <!--  </div> -->

    </div> 
  </div> 
</body>