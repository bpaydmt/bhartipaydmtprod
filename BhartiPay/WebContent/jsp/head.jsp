<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--<title>Bhartipay</title>-->
<meta name="description" content="Bhartipay" />
<meta name="keywords" content="Bhartipay" />
<meta name="author" content="Bhartipay" />

<link rel="icon" type="./image/png" sizes="32x32"
	href="./images/fav/favicon-32x32.png">

<link rel="icon" type="./image/png" sizes="16x16"
	href="./images/fav/favicon-16x16.png">
<!-- Bootstrap core CSS -->
<link href="./css/bootstrapLogin.min.css" rel="stylesheet" />
<link href="./css/plutus.css" type="text/css" rel="stylesheet">
<link href='./css/customStyle.css' rel='stylesheet'>
<link href="./css/font-awesome.min.css" rel="stylesheet">


<script src="js/validateWallet.js"></script>



