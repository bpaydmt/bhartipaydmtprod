<%@page import="com.bhartipay.wallet.report.bean.B2CMoneyTxnMast"%>
<%@page import="com.bhartipay.wallet.report.bean.WalletTxnDetailsRpt"%>


<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<jsp:include page="gridJs.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css" href="./css/newthemecss/css/newtheme.css" />
 
<script src="js/Newtheme-column-visibility.js" type="text/javascript"></script>


 
<%
 DecimalFormat d=new DecimalFormat("0.00");
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
 $(document).ready(function() {
    $('#dpStart').datepicker({
   	 language: 'en',
   	 autoClose:true,
   	 maxDate: new Date(),

   	});
   	if($('#dpStart').val().length != 0){

   	 $('#dpEnd').datepicker({
   	       language: 'en',
   	       autoClose:true,
   	       minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
   	       maxDate: new Date(),
   	       
   	      }); 
   	 
   	}
   	$("#dpStart").blur(function(){

	   	$('#dpEnd').val("")
	   	 $('#dpEnd').datepicker({
	   	      language: 'en',
	   	     autoClose:true,
	   	     minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
	   	     maxDate: new Date(),
	   	     
	   	    }); 
   	})
    
    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Wallet Transaction - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Wallet Transaction - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Wallet Transaction - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Wallet Transaction',
                message:"Generated on" + "<%= currentDate %>" + "",
             
              
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Wallet Transaction - ' + '<%= currentDate %>',
              
            }
        ]
    } );
    

    
} );









function converDateToJsFormat(date) {

var sDay = date.slice(0,2);
var sMonth = date.slice(3,6);
var yYear = date.slice(7,date.length)

return sDay + " " +sMonth+ " " + yYear;
}



 </script>
 

</head>

<% 
User user = (User) session.getAttribute("User");
List<B2CMoneyTxnMast>list=(List<B2CMoneyTxnMast>)request.getAttribute("resultList");


%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

<div id="main" role="main"> 
	<div id="content">       
	    <div class="row">    
  			<div class="col-lg-12 col-md-12 col-sm-12"> 
        
 
            

 
 
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header ">
			<h2>B2C DMT Report</h2>
		</div>
		
		<div class="box-content">
		
			<form action="B2CDMTReport" method="post">
				   <div class="row">


								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								
										 
										
										<input type="text"
										value="<s:property value='%{inputBean.stDate}'/>"
										name="inputBean.stDate" id="dpStart"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required/>
								</div>
								
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
										<input type="text"
										value="<s:property value='%{inputBean.endDate}'/>"
										name="inputBean.endDate" id="dpEnd"
										class="form-control datepicker-here1" placeholder="End Date"
										data-language="en" required/>
								</div>
								
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> </div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-success" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</div>
							</form>
		
		</div>
	</div>
	</div>	
							
		<div id="xyz">
			<table id="example" class="display" width="100%">
				<thead>
					<tr>
						<th><u>WtbtxnId</u></th>
						<th><u>User Id</u></th>
						<th><u>Acc Holder Name</u></th>
						<th><u>Account No</u></th>
						<th><u>IFSC Code </u></th>
						<th><u>Transfer Type</u></th>
						<th><u>Amount</u></th>
						<th><u>Status</u></th>
						<th><u>Txn Date</u></th>
						<th><u>Remark</u></th>
					</tr>
				</thead>
				<tbody>
				<%
				for(B2CMoneyTxnMast wtb:list){
				%>
				  	<tr>
		             <td><%=wtb.getWtbtxnId() %></td>
		             <td><%=wtb.getUserId()%></td>
		             <td><%=wtb.getAccHolderName()%></td>
		              <td><%=wtb.getAccountNo()%></td>
		              <td><%=wtb.getIfscCode() %></td>
		              <td><%=wtb.getTransferType()%></td>
		              <td><%=wtb.getAmount()%></td>
		              <td><%=wtb.getStatus()%></td>
		               <td><%=wtb.getTxndate()%></td>
		               <td><%=wtb.getRemarks()%></td>
                  		  </tr>
                  <%
                  }
				%>
			        </tbody>		</table>
		</div>
	 

 
  

  		</div>
    </div>
	</div> 
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<%--<script src='js/bootstrap.min.js'></script>--%>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


</body>
</html>


