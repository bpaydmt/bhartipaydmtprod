<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Map"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
<link rel="stylesheet" href="css/vendor/simple-line-icons.css">
<link rel="stylesheet" href="css/style.css">
<!-- favicon -->
<link rel="icon" href="favicon.ico">
<title>Bhartipay | Register Account</title>
</head>

<body>

 <% 
 String login=(String)request.getAttribute("login");
 String register=(String)request.getAttribute("register");
 String forgot=(String)request.getAttribute("forgot");
 %>

	<!-- HEADER -->
	<jsp:include page="mainHeader.jsp"></jsp:include>
	<!-- /HEADER -->

	<!-- MENU BAR -->
	<jsp:include page="mainMenuOption.jsp"></jsp:include>
	<!-- /MENU BAR -->

	<div class="section-headline-wrap">
		<div class="section-headline">
			<h2>Register Account</h2>
			<p>Home<span class="separator">/</span><span class="current-section">Register Account</span></p>
		</div>
	</div>

	<!-- SECTION -->
	<div class="section-wrap">
		<div class="section demo">

			<!-- FORM POPUP -->
			<div class="form-popup">
				<!-- CLOSE BTN -->
				<!-- <div class="close-btn">
					<svg class="svg-plus">
						<use xlink:href="#svg-plus"></use>
					</svg>
				</div>-->
				<!-- /CLOSE BTN -->

				<!-- FORM POPUP CONTENT -->
				
				<div class="form-popup-content">
					<h4 class="popup-title">Register Account</h4>
					<!-- LINE SEPARATOR -->
					<hr class="line-separator">
					<!-- /LINE SEPARATOR -->					
					
					<form method="post" name="register" id="register1"
						action="SaveSignUpUser1.action">

						<font color="red"> <s:actionerror /></font>
						<font color="blue"> <s:actionmessage />	</font> 
						
						<input type="hidden" name="user.usertype" value="1"> 
						<input
							class="form-control" name="user.aggreatorid" id="wwdw"
							type="hidden" value="-1" /> 
							<input class="form-control"
							name="user.distributerid" type="hidden" value="-1" /> 
							<input
							class="form-control" name="user.agentid" type="hidden" value="-1" />
							<input class="form-control" name="user.subAgentId" type="hidden"
							value="-1" />
						
						<%
							Map<String, String> mapResult = (Map<String, String>) request.getSession().getAttribute("mapResult");
						%>
						<%
							if (mapResult != null && mapResult.get("emailvalid") != null
									&& Double.parseDouble(String.valueOf(mapResult.get("emailvalid"))) == 0) {
						%>
						
							<label for="password" class="rl-label required">Password</label><input type="password" name="user.password" id="passw"
								autocomplete="off"
								class="password-vl mandatory"
								placeholder="Password">
								
							<label for="confirmPassword" class="rl-label required">Confirm Password</label><input type="password" name="user.confirmPassword" id="passw"
								autocomplete="off"
								class="password-vl mandatory"
								placeholder="Confirm Password">						
						<%
							}
						%>


							<label for="email_address2" class="rl-label" >Email
							Address</label> <input type="text" name="user.emailid" id="email"
							class="emailid mandatory"
							autocomplete="off"
							placeholder="Your Email"
							value="<s:property value='%{user.emailid}'/>">
							 <label
							for="username2" class="rl-label">User Name</label> <input type="text" name="user.name" id="name"
							autocomplete="off"
							class="userName mandatory"
							placeholder="Your Name"
							value="<s:property value='%{user.name}'/>"> 
							
							<label
							for="password2" class="rl-label">Mobile Number</label> <input
							type="text" name="user.mobileno" id="mobile"
							class="mobile mandatory"
							placeholder="Your Mobile Number">
																					
						<input type="button" onclick="submitVaForm('#register1')"
							id="signup" name="signup" value="Sign Up"
							class="button mid dark">

					</form>
				</div>
				
				<!-- /FORM POPUP CONTENT -->
			</div>
			<!-- /FORM POPUP -->


			<div class="clearfix"></div>
		</div>
	</div>
	<!-- /SECTION -->


		<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>