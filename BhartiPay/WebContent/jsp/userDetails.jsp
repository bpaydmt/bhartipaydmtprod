              
                <%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.TAG0"%>
<%@page import="java.util.List"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.Beneficiary"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.CardDetail"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.UserDetails"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
   
 <script type="text/javascript">
 function submitRemoveForm(id,event){

	  $(id).submit();
	  
	 
	}
 
 function submitTrans(id,ga,gt,sa,st){
	// event.preventDefault();	 
	
	 
	 var gaV = $(ga).val();
	 var gat = $(gt).val();
	 
	 $(sa).val(gaV);
	 $(st).val(gat);
	 submitRemoveForm(id); 
	 
 }
 </script>
</head>

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->


	<div class="ch-container">
		<div class="">

			<!-- left menu starts -->
			<jsp:include page="mainMenu.jsp"></jsp:include>
			<!-- left menu ends -->

			<!-- contents starts -->



			<div id="content" class="col-lg-9 col-sm-9">
				<!-- content hellostarts -->

				<!--            
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="DashBoard.action" class="wll">Home</a>
        </li>
        <li>
            <a href="Search.action" class="wll">Search</a>
        </li>
    </ul>
</div>
 -->

				<%
				User user=(User)session.getAttribute("User");
UserDetails userDetails=(UserDetails)request.getAttribute("userDetails");
CardDetail cardDetail=null;
Beneficiary beneficiary=null;
List<TAG0> tagList=null;
if(userDetails!=null){
 cardDetail=userDetails.getCardDetail();
 beneficiary= userDetails.getBeneficiary();
 if(beneficiary!=null)
 tagList=beneficiary.gettAG0();

%>
				<div class=" row">
					<div class="row" id="hidethis2">
						<div class="box col-md-12">
							<div class="box-inner">
								<div class="box-header well">
									<h2>
										<i class="glyphicon "></i>
										<%if(user.getUsertype()==1){ %>
										Send To Bank
										<%}else{ %>
										DMT Details
										<%} %>
									</h2>


								</div>
								<div class="box-content row">
<font color="red"><s:actionerror/> </font>
<font color="blue"><s:actionmessage/> </font>
									<div class="col-lg-12 col-md-12 box-inner">
										<%
								 if(cardDetail!=null&&user.getUsertype()==4){ 
								 %>
										<div class="row row-m-15">
											<div class="col-md-4">
												<strong> Mobile Number: </strong>
												<%=cardDetail.getMobileno() %>

											</div>
											<div class="col-md-4">
												<strong> Balance: </strong>
												<%=cardDetail.getBalance() %>

											</div>

											<div class="col-md-4">
												<strong> Remittance Limit Available: </strong>
												<%=cardDetail.getRemitLimitAvailable() %>
												INR
											</div>
										</div>
										<%} %>
										<div class="row">
											<div class="table-responsive b-details">
												<table class="table table-striped table-bordered">
													<thead>
														<tr>
															<th>Beneficiary Code</th>
															<th>Beneficiary Name</th>

															<th>Account Number</th>
															<th>Account Type</th>
															<th>IFCS Code</th>

															<th>Active</th>
															<th>Beneficiary Type</th>
															<th>Amount</th>
															<th>Transfer</th>
															<th>Remove</th>
														</tr>
													</thead>
													<tbody>

														<%if(tagList!=null){
                              Iterator<TAG0> tagIte=tagList.iterator();
                              while(tagIte.hasNext()){
                            	  TAG0 t=tagIte.next();
                            	  if(t!=null){
                              %>
														<tr>
															<td><%=t.getBeneficiaryCode() %></td>
															<td><%=t.getBeneficiaryName() %></td>

															<td><%=t.getAccountNumber() %></td>
															<td><%=t.getAccountType() %></td>
															<td><%=t.getIFSC() %></td>
															<td><%=t.getActive() %></td>

															<td><select class="b-account-type" id="benType<%=t.getBeneficiaryCode()%>">
																	<option value="NEFT">NEFT</option>
																	<option value="IMPS">IMPS</option>

															</select></td>

															<td><input type="text" class="b-amount-small amount-vl" name="amount"  id="amount<%=t.getBeneficiaryCode()%>"/></td>
															<td style="position:relative">
															<form action="FundTransfer" id="transfer<%=t.getBeneficiaryCode()%>" method="post">
																<input type="hidden" name="dmtInputBean.amount" id="frmAmount<%=t.getBeneficiaryCode()%>" value="">
																<input type="hidden" name="dmtInputBean.benType" id="frmBenType<%=t.getBeneficiaryCode()%>" value="">
																<input type="hidden" name="dmtInputBean.mobile" value="<%=cardDetail.getMobileno()%>">
																<input type="hidden" name="dmtInputBean.benCode" value="<%=t.getBeneficiaryCode()%>">
																<input type="hidden" name="dmtInputBean.benName" value="<%=t.getBeneficiaryName()%>">
																<input type="hidden" name="dmtInputBean.accountType" value="<%=t.getAccountType()%>">
																<input type="hidden" name="dmtInputBean.benAccount" value="<%=t.getAccountNumber()%>">
																<input type="hidden" name="dmtInputBean.benIFSC" value="<%=t.getIFSC()%>">
																<input type="hidden" name="dmtInputBean.surChargeAmount" id ="hiddenInput<%=t.getBeneficiaryCode()%>" >
																
															<a class="btn small-btn btn-info" onclick="openPopup('#confirmation<%=t.getBeneficiaryCode()%>', '#amount<%=t.getBeneficiaryCode()%>', '#ta<%=t.getBeneficiaryCode()%>','#surchargeDMT<%=t.getBeneficiaryCode()%>', '#cash-in-amount<%=t.getBeneficiaryCode()%>', '#total-cash-in<%=t.getBeneficiaryCode()%>', '#hiddenInput<%=t.getBeneficiaryCode()%>')" data-toggle="confirmation" 
															 href="#">  
																		Transfer</a>
															</form>
															<div class="popover confirmation left top in" id="confirmation<%=t.getBeneficiaryCode()%>" style=" left:-475px;  width: 470px!Important;">
              
               <h3 class="popover-title">Are you sure?</h3><div class="popover-content">
               <p class="confirmation-content" style="display: none;"></p><div class="confirmation-buttons text-center">
               <p style="color:#000;font-weight:bold">
               Want to transfer INR <span id="ta<%=t.getBeneficiaryCode()%>"> </span> in account of <%=t.getBeneficiaryName()%> (<%=t.getAccountNumber()%> )
               </p>
               <p >
              You will be charge Rs. <span id="surchargeDMT<%=t.getBeneficiaryCode()%>"></span> as surcharge fee on your Rs. <span id="cash-in-amount<%=t.getBeneficiaryCode()%>"></span> amount and total
              total amount is Rs. <span id="total-cash-in<%=t.getBeneficiaryCode()%>"></span></p>
              <br>
            
               <a href="#" class="btn btn-xs btn-primary" onclick="submitTrans('#transfer<%=t.getBeneficiaryCode()%>','#amount<%=t.getBeneficiaryCode()%>','#benType<%=t.getBeneficiaryCode()%>','#frmAmount<%=t.getBeneficiaryCode()%>','#frmBenType<%=t.getBeneficiaryCode()%>')"><i class="glyphicon glyphicon-ok"></i> Yes</a>
               <a href="#" class="btn btn-xs btn-default" onclick="closePopup('#confirmation<%=t.getBeneficiaryCode()%>')"><i class="glyphicon glyphicon-remove"></i> No</a></div></div></div></div>
								
															</td>
															<td>
																<form id="<%=t.getBeneficiaryCode()%>" action="DeleteBeneficiaryRequest" method="post">
																<input type="hidden" name="dmtInputBean.mobile" value="<%=cardDetail.getMobileno()%>">
																<input type="hidden" name="dmtInputBean.benCode" value="<%=t.getBeneficiaryCode()%>">
																<input type="hidden" name="dmtInputBean.benName" value="<%=t.getBeneficiaryName()%>">
																<input type="hidden" name="dmtInputBean.benType" value="<%=t.getBeneficiaryType()%>">
																<input type="hidden" name="dmtInputBean.benAccount" value="<%=t.getAccountNumber()%>">
																<input type="hidden" name="dmtInputBean.benIFSC" value="<%=t.getIFSC()%>">
																
																	<a onclick="submitRemoveForm('#<%=t.getBeneficiaryCode()%>')" href="#"> <span
																		class="glyphicon glyphicon-remove"></span></a>
																</form>
															</td>
														</tr>
														<%}
                            	  }
                              }
                              } %>
													</tbody>
												</table>
											</div>
											<div class="col-md-12">
												<div class="col-md-12">
													<div class="form-group">
														<!--  <button type="submit" id="submitBeneficiary" class="btn form-btn btn-info btn-fill">Add Beneficiary
                                        </button> -->

															<a href="AddBeneficiary" class="btn btn-info a-btn">Add Beneficiary</a>
															
															<a href="UserBalance" class="btn btn-info a-btn">Get sender balance</a>
															<a href="UserTransHistory" class="btn btn-info a-btn">Get trans history</a>
													</div>








												</div>

											</div>
										</div>

									</div>










								</div>



							</div>



						</div>
					</div>

				</div>

			</div>
		</div>

	</div>



	<!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

<script src='js/bootstrap.min.js'></script>

<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->
<div id="confirm" class="modal hide fade">
<script src="./dist/js/bootstrap-confirmation.min.js"></script>

<script>

function openPopup(id,amount, amountVal, surchargeDMT,cash,total,hiddenInput ){

	$(".confirmation").hide();	
	var am = $(amount).val();
	if(am.length == 0){
		alert("Please enter transfer amount");
	}else{

		$.ajax({
			  type: "POST",
			  url: 'CalculateDMTSurcharge',
			  data:{"suBean.amount":am},
			  success: function( data ) {
				  console.log(data)
				  if(data.status == 1000){
					 var surcharge =  data.samount;
					 var amount =  data.amount;					
					 $(id).fadeIn(); 
					 $(amountVal).text(am)
					
				   $(surchargeDMT).text(surcharge)
				   $(cash).text(amount)
				   $(total).text(surcharge + amount );
				   $(hiddenInput).val(surcharge)
				  }else{
					  alert("Oops!!! something went wrong. Please try after sometime.")
				  }
			  }, 
		});
		/* 
		$(amountVal).text(am)
		$(id).fadeIn(); */
	}
	//alert(am)
	
	
}
function closePopup(id){
$(id).fadeOut();

}
	



</script>


</body>
</html>


                