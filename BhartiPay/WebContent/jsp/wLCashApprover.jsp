<%@page import="com.bhartipay.wallet.transaction.persistence.vo.MoneyRequestBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.DmtDetailsMastBean"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.WalletMastBean"%>
<%@page import="java.text.DecimalFormat"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>



<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>


<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="theams.jsp"></jsp:include>
<jsp:include page="reqFiles.jsp"></jsp:include>
<script src="//code.jquery.com/jquery-1.12.3.js" type="text/javascript"></script> 
<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

  
 
 
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
 
<script type="text/javascript">
function acceptCashLoad(amount,id){
	$("#cashLoadAction").modal('show')
	$("#cashLoadAction p").text("Do yonu want to accept load amount Rs."+ amount); 

	
	 $("#startDateh").val($("#dpStart").val())
        $("#endtDateh").val($("#dpEnd").val())
         $("#aggrId").val(id);
         $("#aggregatoridM").val($("#aggregatorid").val());
         $("#amountLoad").val(amount);
         
         $("#walletLoadForm").attr("action","AcceptCashRequest")
         
	
}

function rejectCashLoad(amount,id){
	$("#cashLoadAction").modal('show')
	$("#cashLoadAction p").text("Do yonu want to reject load amount Rs."+ amount); 

	 $("#startDateh").val($("#dpStart").val())
       $("#endtDateh").val($("#dpEnd").val())
        $("#aggrId").val(id);
        $("#aggregatoridM").val($("#aggregatorid").val());
        $("#amountLoad").val(amount);
        $("#walletLoadForm").attr("action","RejectCashRequest")
}
$(document).ready(function() {

    $('#example').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 1, "asc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'DMT Details Report - ' + '<%= currentDate %>',
                message:'<%= currentDate %>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'DMT Details Report - ' + '<%= currentDate %>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'DMT Details Report - ' + '<%= currentDate %>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'DMT Details Report - ' + '<%= currentDate %>',
                message:" "+ "<%= currentDate %>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'DMT Details Report - ' + '<%= currentDate %>',
              
            }
        ]
    } );
  

    $('#dpStart').datepicker({
     language: 'en',
     autoClose:true,
     maxDate: new Date(),


    });
    if($('#dpStart').val().length != 0){
    	var selectedDate = new Date(converDateToJsFormat($('#dpStart').val()));
        var setDate = selectedDate.setDate(selectedDate.getDate() + 30);
    	$('#dpEnd').datepicker({
           language: 'en',
           autoClose:true,
           minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
           maxDate: new Date(),
           
          }); 
    	
    }
    $("#dpStart").blur(function(){
   
    $('#dpEnd').val("")
     $('#dpEnd').datepicker({
          language: 'en',
         autoClose:true,
         minDate: new Date(converDateToJsFormat($('#dpStart').val())),           
         maxDate: new Date(),
         
        }); 
    })
   
    
} );



	function converDateToJsFormat(date) {
	   
		var sDay = date.slice(0,2);
		var sMonth = date.slice(3,6);
		var yYear = date.slice(7,date.length)
	
		return sDay + " " +sMonth+ " " + yYear;
	}

	
	function acceptAgent(){

		var txt;
		var utr = $("#utr").val();
		$("#utr-hidden").val(utr)
		if(utr.length == 0){
		/* 	var u = confirm("You haven't provided UTR value do you still want to continue.");
			if(u){
				 return true;
			}else{
				return false;
			} */
			alert("You haven't provided UTR value");
			 return false;
		}else{

		var r = confirm("Do you want to accept agent.");
		if (r == true) {
		   return true;
		} else {
		    return false;
		    }
		}
		}
		function rejectAgent(){
			var dc = $("#decline-comment").val();
			if(dc.length == 0){		
				alert("Please provide the decline comment")
			    return false;

			}else{
				var txt;
				var r = confirm("Do you want to reject agent.");
				if (r == true) {
				   return true;
				} else {
					return false;
				}

				
			}
		}

		
		
 </script>
 <style>
#agentFooter, .paymentref{display:none}
</style>
</head>

<% 
User user = (User) session.getAttribute("User");
Date myDate = new Date();
System.out.println(myDate);
SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
String toDate=format.format(myDate);
Calendar cal = Calendar.getInstance();
cal.add(Calendar.DATE, -0);
Date from= cal.getTime();    
String fromDate = format.format(from);
Object[] commDetails=(Object[])session.getAttribute("commDetails");
Format f = NumberFormat.getCurrencyInstance(new Locale("en", "in"));


DecimalFormat d=new DecimalFormat("0.00");

%>
      
       

<body>

    <!-- topbar starts -->
<jsp:include page="header.jsp"></jsp:include>
    <!-- topbar ends -->
    
    
<div class="ch-container">
    <div class="">
        
        <!-- left menu starts -->
<jsp:include page="mainMenu.jsp"></jsp:include>
        <!-- left menu ends -->

        <!-- contents starts -->
        
        
        
                <div id="content" class="col-lg-9 col-sm-9">
            <!-- content hellostarts -->
            

<div class=" row">
<div class="row"  id="hidethis2">
<div id="container">
<div class="box2 col-md-12">
	<div class="box-inner">
	
		<div class="box-header well">
			<h2>DMT Details Report</h2>
		</div>
		
		<div class="box-content row">
		
			<form action="WLCashApprover" method="post" autocomplete="off">


								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<!-- 	<label for="dateFrom">Date From:</label>  --><br> 
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								<input
										type="hidden" name="walletBean.id" id="id"
										value="<%=user.getSubAggregatorId()%>"> 
										
										<input type="text"
										value="<s:property value='%{walletBean.stDate}'/>"
										name="walletBean.stDate" id="dpStart"
										class="form-control" value=""  placeholder="Start Date"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br> <input
													type="text"
													value="<s:property value='%{walletBean.endDate}'/>"
													name="walletBean.endDate" id="dpEnd"
													class="form-control " placeholder="End Date"
													data-language="en"  required>
											</div>
											
											<%
											if(user.getWhiteLabel()==0){
											%>
												<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									<br>
									<s:select list="%{aggrigatorList}" headerKey="All"
												headerValue="All" id="aggregatorid"
												name="walletBean.aggreatorid" cssClass="form-username"
												requiredLabel="true"/>
											</div>
										<%} %>		
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left margin-top17">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</form>
		
		</div>
	</div>
	</div>	
	
		

							
			<%
		List<MoneyRequestBean> moenyRequest=(List<MoneyRequestBean>)request.getAttribute("wlCashRequestList");
		
			%>				
							
			 <div id="xyz">
			<table id="example" class="display">
				<thead>
				
				
					<tr>
						<th><u>Aggregator Id</u></th>						
						<th><u>Amount</u></th>
						<th><u>Bank</u></th>
						<th><u>Sender Name</u></th>
						<th><u>Txn Type</u></th>
						<th><u>UTR No</u></th>
						<th><u>Request Date</u></th>
						<th><u>Remark</u></th>
						<th><u>Action</u></th>
					</tr>
				</thead>
				<tbody>
				<%
				if(moenyRequest!=null&&moenyRequest.size()>0){	
				for(MoneyRequestBean wmb:moenyRequest){
				%>
										<tr>
											<td><%=wmb.getAggreatorid()%></td>
											<td><%=wmb.getAmount()%></td>
											<td><%=wmb.getBank()%></td>
											<td><%=wmb.getSenderName()%></td>
											<td><%=wmb.getTxnType()%></td>
											<td><%=wmb.getUtrNo()%></td>
											<td><%=wmb.getRequestDate()%></td>
											<td><%=wmb.getRemark()!=null?wmb.getRemark():"-"%></td>
											<td>
											<%
											if(wmb.getRequestStatus()!=null&&wmb.getRequestStatus().equalsIgnoreCase("0")){
												if(user.getWhiteLabel()==0&&(user.getUsertype()==4||user.getUsertype()==6)){
											%>
										
											<input type="button" value="Accept"  onclick="acceptCashLoad('<%=wmb.getAmount()%>','<%=wmb.getId()%>')" class="btn btn-primary btn-info grid-small-btn grid-btn2">
											<form action="" id="cashApprove<%=wmb.getId()%>'">
											
											</form>
										
											<input type="button" value="Reject"  onclick="rejectCashLoad('<%=wmb.getAmount()%>','<%=wmb.getId()%>')" class="btn btn-primary btn-info grid-small-btn grid-btn2">
											<form action="" id="cashReject<%=wmb.getId()%>'">
											
											</form>
											<%
												}else{
													%>
													Pending
													<%
												}
											}else if(wmb.getRequestStatus()!=null&&wmb.getRequestStatus().equalsIgnoreCase("1")){
											%>
											Accepted
											<%	
											}else if(wmb.getRequestStatus()!=null&&wmb.getRequestStatus().equalsIgnoreCase("2")){
											%>
											Rejected
											<%	
											}
											%>
											</td>
											<%
                  }
				}                        
				%></tbody>		</table>
		</div> 
	</div>

</div>
</div>

</div>
        
        

        <!-- contents ends -->

       

</div>
</div><!--/.fluid-container-->

<jsp:include page="footer.jsp"></jsp:include>

<!-- external javascript -->

-<script src='js/bootstrap.min.js'></script>
	<script src="./js/datepicker.js"></script>
	<script src="./js/datepicker.en.js"></script>
<!-- library for cookie management -->
<script src="./js/jquery.cookie.js"></script>
<script src="./js/jquery.noty.js"></script>
<script src="./js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<!--<script src="./js/charisma.js"></script>-->


<div id="cashLoadAction" class="modal fade" role="dialog">
  <div class="modal-dialog model-lg" style="width: 400px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cash Load Request</h4>
      </div>
      <form action="" method="post" id="walletLoadForm">
      
      
    
      <div class="modal-body">
       <p></p>
       <div class="form-group">
       <input type="hidden" name="walletBean.stDate" id="startDateh" />
          <input type="hidden"  name="walletBean.endDate" id="endtDateh" />
             <input type="hidden"  name="walletBean.aggreatorid" id="aggregatoridM" />
               <input type="hidden"  name="walletBean.amount" id="amountLoad" />
                <input type="hidden"  name="walletBean.id" id="aggrId" />
                <label>Remark</label>
        <input type="text" name="walletBean.remark" class="form-control" placeholder="" />
       </div>
    <input type="submit" value="Submit" class="btn btn-block" />
      </div>
        </form>
    
    </div>

  </div>
</div>

<div id="newAgentBox" class="modal fade" role="dialog">
  <div class="modal-dialog model-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agent Details</h4>
      </div>
      <div class="modal-body">
        <p>Loading....</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


</body>
</html>