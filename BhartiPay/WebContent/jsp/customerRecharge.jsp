<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
 
 Date d1 = new Date();
 SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
 String currentDate = df.format(d1);
 %>
<% 
User user = (User) session.getAttribute("User");
List<RechargeTxnBean>list=(List<RechargeTxnBean>)request.getAttribute("resultList");
DecimalFormat d=new DecimalFormat("0.00");

%>
      
       


            


<div class="box2 col-md-12">
	<div class="box-inner"  style="float: left; width: 100%;">

		<div class="box-content">
		
			<form id="rechargeForm" >


								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
								<input	type="hidden" name="inputBean.userId" id="userid" value="<s:property value='%{inputBean.userId}'/>"/>	
								<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt">
								
										 
										
										<input type="text"
										value="<s:property value='%{inputBean.stDate}'/>"
										name="inputBean.stDate" id="rechargedpStart"
										class="form-control datepicker-here1" placeholder="Start Date"
										data-language="en" required/>
								</div>
								<div class="form-group  col-md-4 col-sm-3 txtnew col-xs-6">
									<!-- <label for="dateTo">Date To:</label>  -->
									 <input
													type="text"
													value="<s:property value='%{inputBean.endDate}'/>"
													name="inputBean.endDate" id="rechargedpEnd"
													class="form-control datepicker-here2" placeholder="End Date"
													data-language="en"  required>
											</div>
								<div
									class="form-group col-md-4 txtnew col-sm-3 col-xs-6 text-left ">
									
									<div  id="wwctrl_submit">
										<input class="btn btn-info" id="submit"
											type="submit" value="Submit">
												<input class="btn btn-info" id="reset"
											type="reset" value="Reset">
									</div>
								</div>
							</form>
		
		</div>
	</div>
	</div>	
							
		<div id="xyz">
			<table id="rechargeTable" class="display" width="100%">
				<thead>
				
				
					<tr>
						<th><u>Date</u></th>
						<th><u>Txn Id</u></th>
						<th><u>Type</u></th>
						<th><u>Operator</u></th>
						<th><u>Number</u></th>
						<th><u>Amount</u></th>
						<th><u>Status</u></th>
						
						
						
					</tr>
				</thead>
				<tbody>
				<%
				for(RechargeTxnBean wtb:list){
				%>
				  	<tr>
		             <td><%=wtb.getRechargeDate()  %></td>
		             <td><%=wtb.getRechargeId()%></td>
		             <td><%=wtb.getRechargeType() %></td>
		              <td><%=wtb.getRechargeOperator() %></td>
		              <td><%=wtb.getRechargeNumber() %></td>
		               <td><%=d.format(wtb.getRechargeAmt())%></td>
		                <td><%=wtb.getStatus()%></td>
		                
                  		  </tr>
                  <%
                  }
				%>
			        </tbody>		</table>
		</div>


<script>

$(function(){
	$("#rechargeForm").submit(function(e) {


	    $.ajax({
	           type: "POST",
	           url: "CustomerRechargeReport",
	           data: $("#rechargeForm").serialize(), 
	           success: function(data)
	           {
	        	   $("#recharge").html(data);
	   		    
	   		    $('#rechargeTable').DataTable( {
	   		        dom: 'Bfrtip',
	   		        autoWidth: false,
	   		        order: [[ 0, "desc" ]],
	   		        buttons: [
	   		             {
	   		             
	   		                extend: 'copy',
	   		                text: 'COPY',
	   		                title:'Recharge History - ' + '<%= currentDate %>',
	   		                message:'<%= currentDate %>',
	   		            },  {
	   		             
	   		                extend: 'csv',
	   		                text: 'CSV',
	   		                title:'Recharge History - ' + '<%= currentDate %>',
	   		              
	   		            },{
	   		             
	   		                extend: 'excel',
	   		                text: 'EXCEL',
	   		                title:'Recharge History - ' + '<%= currentDate %>',
	   		            
	   		            }, {
	   		             
	   		                extend: 'pdf',
	   		                text: 'PDF',
	   		                title:'Recharge History - ' + '<%= currentDate %>',
	   		                message:" "+ "<%= currentDate %>" + "",
	   		               
	   		            },  {
	   		             
	   		                extend: 'print',
	   		                text: 'PRINT',
	   		                title:'Recharge History - ' + '<%= currentDate %>',
	   		              
	   		            }
	   		        ]
	   		    } );
	   		    
	   		    
	   		    $('#rechargedpStart').datepicker({
	   		     language: 'en',
	   		     autoClose:true,
	   		     maxDate: new Date(),
	   		     

	   		    });
	   		 
	   		     

	   		     
	   		 
	   		    $("#rechargedpStart").blur(function(){
	   		       $('#rechargedpEnd').val("")
	   		     $('#rechargedpEnd').datepicker({
	   		          language: 'en',
	   		         autoClose:true,
	   		         minDate: new Date(converDateToJsFormat($('#rechargedpStart').val())),           
	   		         maxDate: new Date(),
	   		         
	   		        }); 
	   		    })
	         	}
	   		
	   		
	    })
	           
	    

	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});	
})



</script>






