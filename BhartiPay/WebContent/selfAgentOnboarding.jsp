<%@page import="java.util.Map"%>
<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>



<%
	User userSession = (User) session.getAttribute("User");
	if (userSession != null && userSession.getAggreatorid() != null
			&& !userSession.getAggreatorid().isEmpty()) {
		response.sendRedirect("UserHome");
	} else {
%>

<%
	    response.setHeader("Strict-Transport-Security", "max-age=7776000; includeSubdomains");
		response.addHeader("X-FRAME-OPTIONS", "DENY");
		response.setHeader("X-Frame-Options", "SAMEORIGIN");
		response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1 
		response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
		response.setDateHeader("Expires", -1); //prevents caching at the proxy server
		response.flushBuffer();
		// String pgToken=  net.pg.utility.Helper.getEncodedValue();
%>

<%@taglib uri="/struts-tags" prefix="s"%>

<%
		Map<String, String> mapResult = (Map<String, String>) session.getAttribute("mapResult");
		String theams = mapResult.get("themes");
		String favicon = "";
		if (mapResult != null)
			favicon = mapResult.get("favicon");
%>

<%

	String merchantName = mapResult.get("appname");
		String domainName = mapResult.get("domainName");
		String aggId = mapResult.get("aggreatorid");
%>
<!DOCTYPE html>
<html class="<%=theams%>">
<head>
<title><%=mapResult.get("caption")%></title>
<link href="./css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="./css/new_signup.css" rel='stylesheet' type='text/css' /> 
<link href="./css/normalize.min.css" rel='stylesheet' type='text/css' /> 
<link rel="shortcut icon" href="./images/<%=favicon%>">

<script type="text/javascript">

function validatePancardApi(elm){
	var inp = $(pan).val();
	if(inp.match(/^[A-Z]{5}[0-9]{4}[A-Z]{1}$/)){
		$.ajax({
			method:'Post',
			url:'ValidatePan',
			data:"pan="+inp+"&userType=2",
			success:function(result){
				console.log(result.status)
				
				if(result.status == "true"){
					$(pan).addClass("registerPanCard")
					$("#error").text("This pan card number is already register with another agent");
				}else{
					$("#error").text("");
				}
			}
		})
	}else{
		
		
	}
}	

function emailIsValid (email) {
	 
	
	  var email = document.getElementById('emailId').value;
	  let checkEmail =  /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
	  
	  if(checkEmail){
		  document.getElementById('emailId').style.borderColor = "#dfdfdf";
		 
	  }
	  else{
		  document.getElementById('emailId').style.borderColor = "#f05050";
		 
	  }
	  
	}
	
	
</script>

<style>
.selectinput{
  width: 140px;
    border: none;
    border-bottom: 1px solid #dfdfdf;
    padding: 0px;
    font-size: 14px;
    position: relative;
    top: 5px;
    border-top: none;
    outline: none;
    height: 32px;
    margin-right: 5px;
    cursor: pointer;
}
</style>


<%
	String redirect = request.getParameter("redirect");

		if (redirect == null) {
			redirect = "false";
		}
%>




</head>






<body onload="backButtonOverride();">
    <div class="container-fluid"> 
			<%
				String logo = (String) mapResult.get("logopic");
				String banner = (String) mapResult.get("bannerpic");
			%> 
        <div class="row no-gutters min-h-fullscreen bg-white">				
			
			<div class="col-md-7 col-sm-7 col-xs-12 bg-img" data-overlay="5" style="background-color: #bee4c7;">
			
				<div class="row h-100 pl-50">
					<div class="col-md-10 col-lg-8 align-self-end" style="padding-left: 0px;">
					    <img src="<%=logo%>"  style="width: 25%;" class="login-logo"/> 
					    <!-- <img src="./image/happy%20diwali%20(BHARTIPAY).gif"  style="" class="diwali-image"/> -->
					    <img src="./images/giphy.gif"  style="" class="diwali-image"/>
					</div>
					<!-- <div class="col-md-12 col-lg-12 align-self-end">
					     
					</div> -->
				</div>
			</div>				
				
			<div class="col-md-5 col-sm-5 col-xs-12 align-self-center">
				<div class="login-box" id="login">
					<h4>Sign Up</h4>

					<form action="SaveSelfAgentOnBoard" id="form_name" method="post" enctype="multipart/form-data" name="PG" >
	 
		
						<div>
							<font color="red" id="error"><s:actionerror escape="false" /></font> 
							<font color="blue"><s:actionmessage /></font>
						</div>
						
						<input	 name="walletBean.createdby"  type="hidden" value="<%=aggId %>" />
						<input	class="form-control" name="walletBean.agentType" id="agentType" type="hidden" value="SELF"/>
						<input	 name="walletBean.agentid"  type="hidden" value="-1" />
						<input	 name="walletBean.subAgentId"  type="hidden" value="-1" />
						<input	 name="walletBean.addressProofType"  type="hidden" value="Aadhar" />
						<input	 name="walletBean.aggreatorid"  id= "aggid" type="hidden" value="<%=aggId %>" />

						<table  width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
							<tr>
								<td>  
									<input name="walletBean.name" value="<s:property value='%{walletBean.name}'/>" type="text" id="fullName" style="text-transform: uppercase;width: 95%;padding: 12px 0px;margin-bottom: 0px;"  placeholder="Full Name" autocomplete="off" required="required"  />  								
								</td>
								<td>  
									<input name="walletBean.pan"  value="<s:property value='%{walletBean.pan}'/>" type="text" id="pan" style="text-transform: uppercase;width: 95%;padding: 12px 0px;margin-bottom: 0px;"  placeholder="Pan No" autocomplete="off" required="required"  onblur="validatePancardApi(this)"  class="mandatory pancard-vl" />  								
								</td>
							</tr>
							<tr>
								<td> 
									<input name="walletBean.mobileno" readonly="readonly" value="<s:property value='%{walletBean.mobileno}'/>" type="text" id="mobileNo" placeholder="Mobile No" autocomplete="off" style="width: 95%;padding: 12px 0px;margin-bottom: 0px;" required="required" title="Mobile number must have 10 digits and should start with 7,8 or 9."  maxlength="10"  pattern="[6789][0-9]{9}" requiredmessage="invalid mobile number" /> 
								</td>
								<td> 
									<input name="walletBean.emailid" value="<s:property value='%{walletBean.emailid}'/>" onfocusout="emailIsValid()" type="text" id="emailId" placeholder="Email Id" autocomplete="off" style="width: 95%;padding: 12px 0px;margin-bottom: 0px;" required="required"  maxlength="32" readonly="readonly"/> 
								</td>
							</tr>
							</table>
<br>
	<table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
							<tr>
								<td> 
									<input name="walletBean.adhar" value="<s:property value='%{walletBean.adhar}'/>"  type="text" id="aadhar" placeholder="Aadhar No" autocomplete="off" style="width: 95%;padding: 12px 0px;margin-bottom: 0px;" required="required" /> 
								</td>
								<td> 
									<input name="walletBean.address1" value="<s:property value='%{walletBean.address1}'/>"  type="text" id="address" placeholder="Address" autocomplete="off" style="text-transform: uppercase;width: 95%;padding: 12px 0px;margin-bottom: 0px;" required="required" /> 
								</td>
							</tr>
							</table>

<table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
	<tr>
	   <td>
		 <select name="walletBean.gender" class=" selectinput" style="text-transform: uppercase;"   id="gen">
		     <option value="Male">Male</option>
			 <option value="Female">Female</option>
		  </select>	
	    </td>
	    
	    <td>
	    <input type="date" name="walletBean.dob"  placeholder="DD/MM/YYYY" id="dob"  value="<s:property value='%{walletBean.dob}'/>" onchange="getDob()" required="required" autocomplete="off" style="text-transform: uppercase;width: 95%;padding: 12px 0px;margin-bottom: 0px;" >
	    </td>
	
	</tr>
</table>
	<table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
							<tr>
								<td> 
									<input name="walletBean.city" value="<s:property value='%{walletBean.city}'/>"  type="text" id="city" placeholder="City" autocomplete="off" style="text-transform: uppercase;width: 95%;padding: 12px 0px;margin-bottom: 0px;" required="required" /> 
								</td>
								
								<td> 
								<select name="walletBean.state" class=" selectinput"  id="state" style="text-transform: uppercase;">
																<option value="-1">Select State</option>
																<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
																<option value="Andhra Pradesh">Andhra Pradesh</option>
																<option value="Arunachal Pradesh">Arunachal Pradesh</option>
																<option value="Assam">Assam</option>
																<option value="Bihar">Bihar</option>
																<option value="Chandigarh">Chandigarh</option>
																<option value="Chhattisgarh">Chhattisgarh</option>
																<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
																<option value="Daman and Diu">Daman and Diu</option>
																<option value="Delhi">Delhi</option>
																<option value="Goa">Goa</option>
																<option value="Gujarat">Gujarat</option>
																<option value="Haryana">Haryana</option>
																<option value="Himachal Pradesh">Himachal Pradesh</option>
																<option value="Jammu and Kashmir">Jammu and Kashmir</option>
																<option value="Jharkhand">Jharkhand</option>
																<option value="Karnataka">Karnataka</option>
																<option value="Kerala">Kerala</option>
																<option value="Lakshadweep">Lakshadweep</option>
																<option value="Madhya Pradesh">Madhya Pradesh</option>
																<option value="Maharashtra">Maharashtra</option>
																<option value="Manipur">Manipur</option>
																<option value="Meghalaya">Meghalaya</option>
																<option value="Mizoram">Mizoram</option>
																<option value="Nagaland">Nagaland</option>
																<option value="Orissa">Orissa</option>
																<option value="Pondicherry">Pondicherry</option>
																<option value="Punjab">Punjab</option>
																<option value="Rajasthan">Rajasthan</option>
																<option value="Sikkim">Sikkim</option>
																<option value="Tamil Nadu">Tamil Nadu</option>
																<option value="Tripura">Tripura</option>
																<option value="Uttaranchal">Uttaranchal</option>
																<option value="Telangana">Telangana</option>
																<option value="Uttar Pradesh">Uttar Pradesh</option>
																<option value="West Bengal">West Bengal</option>
																</select>
											
														</td>
														
								<td><input name="walletBean.pin"
									value="<s:property value='%{walletBean.pin}'/>" type="text"
									id="pin"
									style="text-transform: uppercase; width: 95%; padding: 12px 0px; margin-bottom: 0px;"
									placeholder="Pin" autocomplete="off" required="required" /></td>

								<td></td>

							</tr>
</table>
<br>
<table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
							
							
							<tr>
								<td> 
									<input name="walletBean.shopName" value="<s:property value='%{walletBean.shopName}'/>"  type="text" id="shopName" placeholder="Shop Name" autocomplete="off" style="text-transform: uppercase;width: 95%;padding: 12px 0px;margin-bottom: 0px;" required="required" /> 
								</td>
								<td> 
									<input name="walletBean.shopAddress1" value="<s:property value='%{walletBean.shopAddress1}'/>"  type="text" id="shopaddress" placeholder="Shop Address" autocomplete="off" style="text-transform: uppercase;width: 95%;padding: 12px 0px;margin-bottom: 0px;" required="required" /> 
								</td>
							</tr>
	</table>
	<table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
							
							<tr>
								<td> 
									<input name="walletBean.shopCity" value="<s:property value='%{walletBean.shopCity}'/>"  type="text" id="shopcity" placeholder="Shop City" autocomplete="off" style="text-transform: uppercase;width: 95%;padding: 12px 0px;margin-bottom: 0px;" required="required" /> 
								</td>
								
								<td> 
								<select name="walletBean.shopState" class="selectinput"  id="shopState" style="text-transform: uppercase;">
																<option value="-1">Select Shop State</option>
																<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
																<option value="Andhra Pradesh">Andhra Pradesh</option>
																<option value="Arunachal Pradesh">Arunachal Pradesh</option>
																<option value="Assam">Assam</option>
																<option value="Bihar">Bihar</option>
																<option value="Chandigarh">Chandigarh</option>
																<option value="Chhattisgarh">Chhattisgarh</option>
																<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
																<option value="Daman and Diu">Daman and Diu</option>
																<option value="Delhi">Delhi</option>
																<option value="Goa">Goa</option>
																<option value="Gujarat">Gujarat</option>
																<option value="Haryana">Haryana</option>
																<option value="Himachal Pradesh">Himachal Pradesh</option>
																<option value="Jammu and Kashmir">Jammu and Kashmir</option>
																<option value="Jharkhand">Jharkhand</option>
																<option value="Karnataka">Karnataka</option>
																<option value="Kerala">Kerala</option>
																<option value="Lakshadweep">Lakshadweep</option>
																<option value="Madhya Pradesh">Madhya Pradesh</option>
																<option value="Maharashtra">Maharashtra</option>
																<option value="Manipur">Manipur</option>
																<option value="Meghalaya">Meghalaya</option>
																<option value="Mizoram">Mizoram</option>
																<option value="Nagaland">Nagaland</option>
																<option value="Orissa">Orissa</option>
																<option value="Pondicherry">Pondicherry</option>
																<option value="Punjab">Punjab</option>
																<option value="Rajasthan">Rajasthan</option>
																<option value="Sikkim">Sikkim</option>
																<option value="Tamil Nadu">Tamil Nadu</option>
																<option value="Tripura">Tripura</option>
																<option value="Uttaranchal">Uttaranchal</option>
																<option value="Telangana">Telangana</option>
																<option value="Uttar Pradesh">Uttar Pradesh</option>
																<option value="West Bengal">West Bengal</option>
																</select>
											
														</td>
														
								<td><input name="walletBean.shopPin"
									value="<s:property value='%{walletBean.shopPin}'/>" type="text"
									id="shoppin"
									style="text-transform: uppercase;width: 95%; padding: 12px 0px; margin-bottom: 0px;"
									placeholder="Shop Pin" autocomplete="off" required="required" /></td>

								<td></td>

							</tr>

						</table>
						<br>

						<table>
							<tr>
								<td><input name="walletBean.bankName"
									value="<s:property value='%{walletBean.bankName}'/>"
									type="text" id="shopcity" placeholder="Bank Name"
									autocomplete="off"
									style="text-transform: uppercase;width: 95%; padding: 12px 0px; margin-bottom: 0px;"
									required="required" /></td>
								<td><input name="walletBean.accountNumber"
									value="<s:property value='%{walletBean.accountNumber}'/>"
									type="text" id="shopcity" placeholder="Account Number"
									autocomplete="off"
									style="text-transform: uppercase;width: 95%; padding: 12px 0px; margin-bottom: 0px;"
									required="required" /></td>
								<td><input name="walletBean.ifscCode"
									value="<s:property value='%{walletBean.ifscCode}'/>"
									type="text" id="shopcity" placeholder="Ifsc Code"
									autocomplete="off"
									style="text-transform: uppercase;width: 95%; padding: 12px 0px; margin-bottom: 0px;"
									required="required" /></td>
							</tr>
						</table>

						
						<table>
							<tr>
								<td><label for="apptxt">Pan Card</label></td>
								<td><input type="file" name="walletBean.file1"
									required="required"style="width: 95%; padding: 12px 0px; margin-bottom: 0px;"></td>
							</tr>
							<tr>
								<td><label for="apptxt">Aadhar Card</label></td>
								<td><input type="file"
									name="walletBean.file2" required="required" style="width: 95%; padding: 12px 0px; margin-bottom: 0px;"></td>
							</tr>
							<tr>
								<td><label for="apptxt">Bank Details</label></td>
								<td><input type="file" name="walletBean.file3"
									required="required" style="width: 95%; padding: 12px 0px; margin-bottom: 0px;"></td>
							</tr>
						</table>
						
						<input type="hidden" value="${csrfPreventionSalt}" name="csrfPreventionSalt"> 

					<table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
						<tr>
							<td align="left" height="60" valign="middle" style="width:70%;">
								<input type="submit" class="btn btn-bold btn-block btn-primary" key="submit" value="Sign Up"/>
							</td>
							<td align="left" height="60" valign="middle" style="width:25%; margin-left: 10px;">
								<input type="reset" class="btn btn-bold btn-block btn-secondary" key="Reset" value="Reset"/>
							</td>
						</tr>
					</table>
						<table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
							<tr> 
								<td align="center" valign="middle">
									<s:a class="text-primary fw-500" action="UserHome">Already User? Login</s:a>
								</td>
							</tr>
						</table>

					</form>
				</div>
			</div>

		
		</div> 
    </div>
	<!-- Javascript -->
	<script src="./js/jquery.min.js"></script>
	<script src="./js/bootstrap.min.js"></script>
	<script src="./js/jquery.backstretch.min.js"></script>
	<script src="./js/scripts.js"></script>
	

<jsp:include page="policy.jsp"></jsp:include>


</body>



</html>
<%} %>
