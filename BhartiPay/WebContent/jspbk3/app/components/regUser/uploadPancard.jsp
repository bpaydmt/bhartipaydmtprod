<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="s" uri="/struts-tags"%>	


 
<div  data-ng-controller="uploadPanCtrl as pc"  >
<div class="container-fluid" id="panContainer">



<form  id="panFrom60Form" ng-submit="pc.submitPanForm()">

									

<div class="row">
<div class="col-md-12">
<h2 style="margin-top:0px" id="heading">
		Upload Pan Card	</h2>
		<span id="panErrMsg" style="color:red"></span>
</div>
<input type="hidden" value="{{$root.LoginSender.id}}" name="mudraSenderPanDetails.senderId" id="desc1" >
<input type="hidden" value="Pan" name="mudraSenderPanDetails.proofType" id="proofType" >

</div>
	<div class="row" id="pancard" >
		
		<div class="col-md-12 form-group" id="panField">
		
		<label >Enter Your PAN Card Number:</label>
				<input type="text"   value="<s:property value='%{mudraSenderPanDetails.panNumber}'/>" name="mudraSenderPanDetails.panNumber" id="panCardInp" 
																		class="form-control pancard-vl mandatory"
																		placeholder="Enter PAN" ng-model="pc.pan" capitalize>
				
		</div>
	
		<div class="col-md-12 form-group">
	
		<label id="fileLabelHeading">Upload PAN Card:</label>
				<input type="file" value="<s:property value='%{mudraSenderPanDetails.myFile1}'/>" name="mudraSenderPanDetails.myFile1" id="panfile"  class="mandatory" placeholder="PAN CARD" required readonly="readonly" required> 
				
		</div>
		
	<div class="col-md-12" id="panMsg" style="margin-top:15px"><strong>Note:</strong> If you don't have PAN card. You can submit FORM 60 instead of PAN Card. To submit FORM 60 <a href="javascript:void(0)" onclick="switchPanForm('form60')" id="form60LInk" >Click here</a></div>
		
	</div>
	<div class="row" id="from60" style="display:none" >
	
	
		<div class="col-md-12 form-group">
		<label><a href="./jspbk3/assets/downloads/Form60.pdf"  download>Download "Form 60" </a></label>
		<label class="pull-right"><a href="javascript:void(0)"  onclick="switchPanForm('Pan')" id="uploadPanCard"  >Upload PAN Card</a></label>
			
		</div>
	
		
	</div>

	<div class="row">
	<div class="col-md-12">
	<input class="btn btn-primary btn-block" type="submit" value="Submit" style="    padding: 11px;    margin: 20px 0 10px;">
</div>

</form>


	</div>
</div>
 <div class="modal-footer " style="text-align:left!important">
     <p><strong>
     
     "As Mandated by the IT Dept., Ministry of Finance (Govt. of India), sender will only be
     allowed to transfer upto Rs 50,000 per year without submission of PAN card. It is 
     strongly advised that you upload sender's PAN Card document along with KYC or
     fill form 60, to allow sender more than Rs 50,000 in a year.
     "
     </strong></p>
      </div>



<style>
.modal-dialog{width:400px!important}
</style> 