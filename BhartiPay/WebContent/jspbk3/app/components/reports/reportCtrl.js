
//transaction Ledger Report Controller
dmt.controller('transLedgerCtrl',function(Excel,$http,$rootScope,$timeout,$filter){
	var trans = this;
	trans.gridData = false;
	trans.tDate = {};
	trans.exl = false;
	trans.btnText = "Submit";
	trans.loading = false;
	trans.tableParam;
	
	    
/*	var currentDate = new Date();
	var year = currentDate.getFullYear() - 1;
	var month = currentDate.getMonth();
	var month1 = currentDate.getMonth() +1;
	var day = currentDate.getDate();*/
	
	trans.initDatePicker = function(){
		 $('#dpStart').datepicker({
	    	 language: 'en',	    	
	    	 maxDate: new Date(),
	    	// minDate: new Date(year+"/"+month+"/"+day),
	    	
	    	
	    });
	   $("#dpStart").blur(function(){
	 //  var selectedDate = new Date(converDateToJsFormat($('#dpStart').val()));
	  // var setDate = selectedDate.setDate(selectedDate.getDate() + 30);
	 //  console.log(setDate)
	   $('#dpEnd').val("")
	   	  $('#dpEnd').datepicker({
	   	     	 language: 'en',
	   	      minDate: new Date(converDateToJsFormat($('#dpStart').val())),	   	     	
	   	     	//maxDate: new Date(setDate),
	   	     	
	   	     });	
	   })
	};
	
	  trans.exportToExcel=function () {
		  trans.mystyle = {
			        sheetid: 'Transaction Ledger',
			        headers: true,
			        caption: {
			          title:'Transaction Ledger',
			          style:'font-size: 50px; color:#ccc;' // Sorry, styles do not works
			        },
			      
			        columns: [
			          {columnid:'narrartion',title:'Mode'},
			          {columnid:'senderid', title: 'Sender ID'},
			          {columnid:'txnid',title: 'Txn Id'},
			          {columnid:'bankname',title: 'Bank Name'},
			          {columnid:'ptytransdt',title: 'Payment Transaction Date'},
			          {columnid:'agentid',title: 'Agent ID'},
			          {columnid:'dramount',title: 'debit Amount'},
			          {columnid:'bankrrn',title: 'Bank RRN'},
			          {columnid:'lastname',title: 'Last Name'},
			          {columnid:'ifsccode',title: 'IFSC Code'},
			          {columnid:'name',title: 'Name'},
			          {columnid:'status',title: 'Status'},
			          {columnid:'mobileno',title: 'Mobile No'},
			          {columnid:'accountno',title: 'Account No'},
			         
			        ],
			        
			    };

		   
	        alasql('SELECT * INTO XLSX("TransactionLedger.xlsx",?) FROM ?',[trans.mystyle ,trans.tReport]);
	    };
	trans.genreateData = function(){
		var pdfTable = [[{text:"Trans Date",style:"header"},
		                   {text:"Trans ID",style:"header"},
		                   {text:"Amount",style:"header"},
		                   {text:"Mode",style:"header"},
		                   {text:"Sender Mobile No.",style:"header"},
		                   {text:"Bene Account No.\n(Bank Name)\nIFSC Code",style:"header"},
		                 
		                   {text:"Bank BRR",style:"header"},
		                   {text:"Status",style:"header"},
		                  
		                  
		                   ]];
		 
		var tData = trans.tReport
		  angular.forEach(tData, function(item,index) {
			  var newArr = new Array();
			
			  item.bankrrn = item.bankrrn == undefined ? "-"  : item.bankrrn
			  //	newArr.push(tableHeader[index]);
				newArr.push({text:item.ptytransdt,style:'td'});
			  	newArr.push({text:item.txnid,style:'td'});
			  	newArr.push({text:"INR " +item.dramount,style:'td'});
			  	newArr.push({text:item.narrartion,style:'td'})
				newArr.push({text:item.mobileno + "\n" ,style:'td'})
				newArr.push({text:item.accountno + "\n" + item.bankname+ "\n"+item.ifsccode,style:'td'})
			
				newArr.push({text:item.bankrrn,style:'td'});
				newArr.push({text:item.status,style:'td'});
				
				console.log(newArr)
				pdfTable.push(newArr)
	        });
		
		
		return pdfTable;
	}
	trans.pdfDownload = function(){
		
		genratePDFnPrint("PDF","TransactionLedger","Transaction Ledger",trans.genreateData)
	}
trans.printFile = function(){
		
		genratePDFnPrint("Print","","Transaction Ledger",trans.genreateData)
	}


	trans.getTransReport =  function(){
		trans.btnText = "Loading...";
    	trans.loading = true;
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		trans.tDate.agentId = $rootScope.loginUserData.id;
		trans.tDate.stDate =  $('#dpStart').val()
		trans.tDate.edDate =  $('#dpEnd').val();
		
		
		$http({			
	     method: 'POST',
	     url: 'GetTransacationLedgerDtlBK3',
	     data    :'data='+ JSON.stringify(trans.tDate),//forms user object
	           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
	   })
	    .then(function(res){ 	   
	    	console.log(res)
	    	trans.gridData = true;
	    	trans.tReport = res.data.transList;
	    	trans.ledgerCount = res.data.transList.length;
	    	trans.btnText = "Submit";
	    	trans.loading = false;
	    	
	   
	    
	  },function(res){
	   console.log(res.statusText)
	  });
	    	   } else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	       },function(res){
	  		   console.log(res.statusText)
	  		  });


}
	
	
	trans.getTransactionDetails =  function(id){
		// remove below two lines 
	  	      
		
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		
		
		
		$http({			
	     method: 'POST',
	     url: 'TransactionDetailsByTxnIdBK3',
	     data    :'txnId='+ id,//forms user object
	           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
	   })
	    .then(function(res){ 	   
	    	console.log(res)
	    	
	    	//uncommment below threelines ambuj
	    	$rootScope.modelBoxContent = "jspas/app/components/regUser/transactionSummary.jsp" + $rootScope.version;
	    	$('#myModal').modal('show');
	    	$rootScope.transSummary = angular.fromJson(res.data.details);
			//$rootScope.loginUserData.finalBalance = $rootScope.transSummary.agentWalletAmount;
			//$rootScope.LoginSender.transferLimit  =  $rootScope.transSummary.senderLimit;
			  			  	      
	    
	  },function(res){
	   console.log(res.statusText)
	  });
	    	   } else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	       },function(res){
	  		   console.log(res.statusText)
	  		  });


	}
	
});

// Agent Ledger Report Controller
dmt.controller('agentLedgerCtrl',function($http,$rootScope){
	var agent = this;
	agent.atDate = {};
	agent.agentgridData =false;
	agent.exl = false;
	agent.btnText = "Submit";
	agent.loading = false;
	/*var currentDate = new Date();
	var year = currentDate.getFullYear() - 1;
	var month = currentDate.getMonth();
	var month1 = currentDate.getMonth() +1;
	var day = currentDate.getDate();*/
	//agent.agentSearchForm.astDate = '';
	//agent.agentSearchForm.aedDate = '';
	
	
	agent.formValidation = true;
	agent.formFieldFilled = function(){
		if($('#adpStart').val().length != 0  && $('#adpStart').val().length != 0){
			formValidation = false;
		}else{
			formValidation = true;
		}
		alert($('#adpStart').val().length )
	}
	agent.initDatePicker = function(){
		
	    $('#adpStart').datepicker({
	    	 language: 'en',
	    	 maxDate: new Date(),
	    	// minDate: new Date(year+"-"+month+"-"+day),
	    	
	    });
	   $("#adpStart").blur(function(){
		 /*  var selectedDate = new Date(converDateToJsFormat($('#adpStart').val()));
		   var setDate = selectedDate.setDate(selectedDate.getDate() + 30);*/
		   $('#adpEnd').val("");					
	
	   	  $('#adpEnd').datepicker({
	   	     language: 'en',	   	    
	   	     minDate: new Date(converDateToJsFormat($('#adpStart').val())),	   	     	
	   	    // maxDate: new Date(setDate),
	   	     });	
	   });

		
	}

	
	agent.getAgentReport =  function(){
		agent.btnText = "Loading...";
		agent.loading = true;
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		agent.atDate.agentId = $rootScope.loginUserData.id;
		agent.atDate.stDate = $('#adpStart').val()
		agent.atDate.edDate = $('#adpEnd').val()
		
		
		$http({			
	     method: 'POST',
	     url: 'GetAgentLedgerDtlBK3',
	     data    :'data='+ JSON.stringify(agent.atDate),//forms user object
	           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
	   })
	    .then(function(res){ 	   
	    	console.log(res)
	    	agent.agentgridData = true;
	    	agent.atReport = res.data.agentLedger;
	    	agent.ledgerCount = res.data.agentLedger.length;
	 
	    	agent.btnText = "Submit";
	    	agent.loading = false;
	   	
			  			  	      
				   
			  			  	      
	    
	  },function(res){
	   console.log(res.statusText)
	  });
	} else{
		   alert("Your session has expired. Please login again.")
		   window.close();
	   }
},function(res){
	   console.log(res.statusText)
	  });


	}

	agent.exportToExcel=function(){ 
		agent.mystyle = {
			        sheetid: 'Agent Ledger',
			        headers: true,
			        caption: {
			          title:'Agent Ledger',
			          style:'font-size: 50px; color:#ccc;' // Sorry, styles do not works
			        },
			            
			        
			        columns: [
			          {columnid:'txnDate',title:'Transaction Date'},
			          {columnid:'userid', title: 'User ID'},
			          {columnid:'resptxnid',title: 'Response Txn Id'},
			          {columnid:'txntype',title: 'Transaction Type'},
			          {columnid:'mobileno',title: 'Mobile No'},
			          {columnid:'agentid',title: 'Agent ID'},
			          {columnid:'accountno',title: 'Account No'},
			          {columnid:'name',title: 'Name'},
			          {columnid:'lastname',title: 'Last Name'},
			          {columnid:'ifsccode',title: 'IFSC Code'},
			          {columnid:'dramount',title: 'Debit Amount'},
			          {columnid:'txncredit',title: 'Transaction Credit'},
			          {columnid:'closingbal',title: 'Closing balance'},
			          
			         
			        ],
			        
			    };

		 alasql('SELECT * INTO XLSX("AgentLedger.xlsx",?) FROM ?',[agent.mystyle,agent.atReport]);
    }
	agent.genreateData = function(){
		var pdfTable = [[{text:"Date",style:"header"},
		                   {text:"Agent ID",style:"header"},
		                   {text:"Txn ID",style:"header"},
		                   {text:"Txn Type",style:"header"},
		                   {text:"Sender Mobile ",style:"header"},
		                   {text:"Account Number",style:"header"},
		                   {text:"IFSC Code",style:"header"},
		                   {text:"Debit",style:"header"},
		                   {text:"Credit",style:"header"},
		                   {text:"Running Balance",style:"header"}
		                  
		                   ]];
		 
		var tData = agent.atReport
		  angular.forEach(tData, function(item,index) {
			  var newArr = new Array();

			  	newArr.push({text:item.txnDate,style:'td'});
			  	newArr.push({text:item.userid,style:'td'});
			  	newArr.push({text:item.resptxnid,style:'td'})
				newArr.push({text:item.txntype,style:'td'})
				newArr.push({text:item.mobileno,style:'td'})
				newArr.push({text:item.accountno +" "+ item.name,style:'td'})
				newArr.push({text:item.ifsccode,style:'td'})
				newArr.push({text:"INR " + item.dramount,style:'td'});
				newArr.push({text:"INR " + item.txncredit,style:'td'});
				newArr.push({text:"INR " + item.closingbal,style:'td'});
				pdfTable.push(newArr)
	        });
		
		
		return pdfTable;
	}
	agent.pdfDownload = function(){
		
		genratePDFnPrint("PDF","Agent_Ledger","Agent Ledger",agent.genreateData)
	}
		agent.printFile = function(){
		
		genratePDFnPrint("Print","","Agent Ledger",agent.genreateData)
	}	
})
// Sender Ledger Report Controller
dmt.controller('senderLedgerCtrl',function($http,$rootScope){
	var sender = this;

	sender.sendergridData =false;
	sender.exl = false;
	sender.btnText = "Submit";
	sender.loading = false;
	sender.refundTransaction = function(id,senderid){

		var senderid = senderid == undefined ? "" : senderid;
		 $rootScope.modelBoxContent = "jspbk3/app/components/reports/refundPopup.html"+  $rootScope.version;
		 $('#myModal').modal('show');
			$rootScope.refunddone = false;
			
		 setTimeout(function(){
			 $("#txtId").val(id);
			 $("#refundResponse").text('')
			  $("#refundMpin").val('')
			$("#sendersenderidSearch").val(senderid);
				
				
		 },200)
		 	
		
	}
	
	sender.emptyfield= function(){
		
		sender.senderForm.mobileNo = ''
		sender.senderForm.accountno = ''
		sender.senderForm.transactionId = ''
	}
	
	sender.getsenderReport =  function(){
		sender.btnText = "Loading...";
		sender.loading = true;
		$http.get('CheckSession')
	       .then(function(res){ 
	    	  // alert(2)
	    	   console.log(res)
	    	   if(res.data.status == "TRUE"){
		var senderObj = {"mobileNo":sender.senderForm.mobileNo,"accountno":sender.senderForm.accountno,"transactionId":sender.senderForm.transactionId,"agentId":$rootScope.loginUserData.id}
		

		$http({			
	     method: 'POST',
	     url: 'GetSenerLedgerDtlBK3',
	     data    :'data='+ JSON.stringify(senderObj),//forms user object
	           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
	   })
	    .then(function(res){ 	   
	    	console.log(res)
	    	
			  
	    
	    	$rootScope.senderLedgerList = res.data.senderLedgerList;
	    	$rootScope.senderLedgerCount = res.data.senderLedgerList.length;
	    	angular.forEach($rootScope.senderLedgerList, function(item,index) {
	    		
				  item.bankrrn = item.bankrrn == "" ? "-"  : item.bankrrn
	    		
	    		$rootScope.senderLedgerList[index].bankrrn =  item.bankrrn
				  
				
						  
		    	})
	     	
		    	sender.ledgerList = $rootScope.senderLedgerList
	     	
	     	
	    	sender.sendergridData = true;
	    	sender.btnText = "Submit";
	    	sender.loading = false;
	    	
				   
			  			  	      
	    
	  },function(res){
	   console.log(res.statusText)
	  });
	    	   } else{
	    		   alert("Your session has expired. Please login again.")
	    		   window.close();
	    	   }
	       },function(res){
	  		   console.log(res.statusText)
	  		  });

	}

	sender.exportToExcel=function(tableId){ 
		sender.mystyle = {
		        sheetid: 'Sender Ledger',
		        headers: true,
		        caption: {
		          title:'Sender Ledger',
		          style:'font-size: 50px; color:#ccc;' // Sorry, styles do not works
		        },
		        	        
		        columns: [
		          {columnid:'id',title:'ID'},
		          {columnid:'dramount',title: 'Debit'},
		          {columnid:'cramount',title: 'Credit Amount'},
		          {columnid:'isbank',title: 'Bank'},
		          {columnid:'narrartion',title: 'Mode'},
		          {columnid:'accountno',title: 'Account No'},
		          {columnid:'bankrrn',title: 'Bank RRN'},
		          {columnid:'remark',title: 'Remark'},
		          {columnid:'ifsccode',title: 'IFSC Code'},
		          {columnid:'ptytransdt',title: 'Payment Transaction Date'},
		         
		          
		         
		        ],
		        
		    };

	 alasql('SELECT * INTO XLSX("SenderLedger.xlsx",?) FROM ?',[sender.mystyle,sender.ledgerList]);
    }
	sender.genreateData = function(){
		
		var pdfTable = [[{text:"Txn ID",style:"header"},
		                   {text:"Account Number \nIFSC Code",style:"header"},
		                   {text:"Debit",style:"header"},
		                   {text:"Credit",style:"header"},
		                  
		                   {text:"Txt Type",style:"header"},		                               
		                   {text:"Bank RRN",style:"header"},
		                   {text:"Status",style:"header"},
		                   {text:"Txt Date",style:"header"}
		                  
		                   ]];
		 
		var tData = sender.ledgerList
		  angular.forEach(tData, function(item,index) {
			  
			  var newArr = new Array();
			
			  item.bankrrn = item.bankrrn == "" ? "-"  : item.bankrrn
					  item.bankrrn = item.bankrrn == undefined ? "-"  : item.bankrrn
					  item.remark = item.remark == undefined ? "-"  : item.remark
					  console.log(item.txnid)
			  	newArr.push({text:item.id,style:'td'});
			  	newArr.push({text:item.accountno +"\n"+ item.ifsccode,style:'td'});			  
				newArr.push({text:"INR " + item.dramount,style:'td'})
				newArr.push({text:"INR " + item.cramount,style:'td'})
				;
				newArr.push({text:item.narrartion ,style:'td'})
				newArr.push({text:item.bankrrn ,style:'td'})
				newArr.push({text:item.remark,style:'td'})
				newArr.push({text:item.ptytransdt,style:'td'})				
				pdfTable.push(newArr)
				console.log(newArr)
	        });
		
		
		return pdfTable;
	}
	sender.pdfDownload = function(){
		console.log(sender.genreateData)
		genratePDFnPrint("PDF","Sender_Ledger","Sender Ledger",sender.genreateData )
	}
		sender.printFile = function(){
		
		genratePDFnPrint("Print","","Sender Ledger",sender.genreateData)
	}	
})
function genrateDate(){
	var d = new Date()
	var day = d.getDate()
	var month = d.getMonth() + 1 ;
	var year = d.getFullYear();
	
	return day + "-" + month + "-" + year;
	
	
	
}

function genratePDFnPrint(type,fileName,title,data){	

		var docDefinition = {
				  content: [
				        	{ 
				    			text: title, style:'pageTitle'				    			 
				    		},
				    		{ 
				    			text: Date(),style:'dateStyle'				    			 
				    		},
				    {
				    	
				      table: {
				       
				        headerRows: 1,
				        body:  data()
				      }
				    }
				  ],
				  styles: {
				        header: {
				        	color:"#fff",fillColor:"#f36d25",	fontSize: 10,bold:true
				        },
					  td: {
				        	color:"#999",fontSize: 10
				        },
				       pageTitle:{fontSize:20,bold:true,margin:[0,10,0,0],color:"#f36d25"},
				       dateStyle:{fontSize:12,color:"#999",margin:[0,3,0,7]}
				    },	   
				    
				    pageMargins:[20,20,20,20]
				};
		if(type == "PDF"){

			 pdfMake.createPdf(docDefinition).download(fileName + "_" + genrateDate() + ".pdf");
				
		}else if(type == "Print"){
			
			 pdfMake.createPdf(docDefinition).print()
		}
	}
dmt.controller('refundCtrl',function($http,$rootScope){
	var refund = this;
	
	refund.loading = false;
	$rootScope.refunddone = false
	refund.submitRefund = function(){
		refund.loading = true;
		//"mobileNo": refund.mobile,"accountNo": refund.account,"id":refund.id,"mpin":refund.mpin 
	var refundObj = {};
		
		
	refundObj.mobileNo = $("#senderMobileNo").val();
	refundObj.accountno =$("#senderAccountNo").val();
	refundObj.txnid =  $("#txtId").val();
	refundObj.mpin = refund.mpin;
	refundObj.senderid=$("#sendersenderidSearch").val();
	
	console.log(refundObj)
	//var senderObj = {"mobileNo":sender.senderForm.mobileNo,"accountno":sender.senderForm.accountno,"agentId":$rootScope.loginUserData.id}

		$http({
		     method: 'POST',
		     url: 'InitiateDMTRefundByAgent',
		     data    :'data='+ JSON.stringify(refundObj) ,// user object
		           headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		   }).then(function(res){
				refund.loading = false;
			
			   console.log(res)
			  if(res.data.statuscode == "1000"){
				  $rootScope.senderLedgerCount =  res.data.senderLedgerList.length;
					$rootScope.senderLedgerList = res.data.senderLedgerList;
					 $("#refundResponse").text( res.data.statusmsg).css('color','green')
					 $rootScope.refunddone = true;
			   }else{
				  $("#refundResponse").text( res.data.statusmsg).css('color','red')
			   }
		   })
	}
	
});

