<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeTxnBean"%>
<%@page import="com.bhartipay.wallet.recharge.bean.RechargeBean"%>
<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.WalletToBankTxnMast"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>

<%@page import="com.bhartipay.wallet.user.persistence.vo.User"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="java.util.Map"%>
<%
	Map<String, String> mapResult = (Map<String, String>) session.getAttribute("mapResult");
	String theams = mapResult.get("themes");
	String favicon = "";
	if (mapResult != null)
		favicon = mapResult.get("favicon");

	String version = "?i=1000tuesday20191";
	String sessionid = (String) session.getAttribute("sessionid");
%>
<%
	String merchantName = mapResult.get("appname");
	String domainName = mapResult.get("domainName");
%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.text.DecimalFormat"%>
<%@page
	import="com.bhartipay.wallet.transaction.persistence.vo.PassbookBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.Format"%>
<%@page import="com.bhartipay.wallet.report.bean.ResponseBean"%>
<%@page import="com.bhartipay.wallet.report.bean.RefundAgent"%>


<jsp:include page="/jsp/theams.jsp"></jsp:include>
<jsp:include page="/jsp/reqFiles.jsp"></jsp:include>
<jsp:include page="/jsp/gridJs.jsp"></jsp:include>

<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/theme-converter.css" />
<link rel="stylesheet" type="text/css"
	href="./css/newthemecss/css/newtheme.css" />

<script type="text/javascript"
	src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>

<script src="js/jquery1.dataTables.min.js" type="text/javascript"></script>
<script src="js/dataTables1.buttons.min.js" type="text/javascript"></script>
<script src="js/jszip.min.js" type="text/javascript"></script>
<script src="js/pdfmake.min.js" type="text/javascript"></script>
<script src="js/vfs_fonts.js" type="text/javascript"></script>
<script src="js/buttons.html5.min.js" type="text/javascript"></script>
<script src="js/buttons.print.min.js" type="text/javascript"></script>

<!-- -------------------------------------------------------------- -->

<link rel="stylesheet" type="text/css"
	href="jspbk3/assets/cssLibs/bootstrap-3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.css">
<link rel="stylesheet" type="text/css" href="jspbk3/mainStyle.css">

<%-- <link href="./css/datepicker.min.css" rel="stylesheet" type="text/css">
<link href="jspbk3/assets/cssLibs/chosen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="jspbk3/assets/jsLibs/angular.min.js"></script>
<script type="text/javascript" src="jspbk3/assets/jsLibs/angular-route.js"></script>
<script type="text/javascript" src="jspbk3/assets/jsLibs/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="jspbk3/assets/jsLibs/angu-fixed-header-table.js"></script>
<script  type="text/javascript"  src="jspbk3/assets/jsLibs/dirPagination.js"></script> --%>

<script type="text/javascript" src="jspbk3/assets/jsLibs/pdfmake.min.js"></script>
<script type="text/javascript" src="jspbk3/assets/jsLibs/vfs_fonts.js"></script>
<script type="text/javascript"
	src="jspbk3/assets/cssLibs/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="jspbk3/app/dmt.module.js<%=version%>"></script>
<script type="text/javascript"
	src="jspbk3/app/dmt.routes.js<%=version%>"></script>
<script type="text/javascript"
	src="jspbk3/app/services/appServices.js<%=version%>"></script>
<script type="text/javascript"
	src="jspbk3/app/components/newUser/senderFormCtrl.js<%=version%>"></script>
<script type="text/javascript"
	src="jspbk3/app/components/error/errorMsgCtrl.js<%=version%>"></script>
<script type="text/javascript"
	src="jspbk3/app/components/addBeneficiary/addBeneficiaryCtrl.js<%=version%>"></script>
<script type="text/javascript"
	src="jspbk3/app/components/regUser/regUserCtrl.js<%=version%>"></script>
<script type="text/javascript"
	src="jspbk3/app/components/importSender/importSenderCtrl.js<%=version%>"></script>
<script type="text/javascript"
	src="jspbk3/app/components/favGrid/favListCtrl.js<%=version%>"></script>
<script type="text/javascript"
	src="jspbk3/app/components/reports/reportCtrl.js<%=version%>"></script>
<script type="text/javascript" src="./js/datepicker.js"></script>
<script type="text/javascript" src="./js/datepicker.en.js"></script>

<script type="text/javascript" src="jspbk3/assets/jsLibs/Blob.min.js"></script>
<script type="text/javascript"
	src="jspbk3/assets/jsLibs/FileSaver.min.js"></script>
<script type="text/javascript"
	src="jspbk3/assets/jsLibs/tableexport.min.js"></script>
<script type="text/javascript" src="jspbk3/assets/jsLibs/alasql.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.12.7/xlsx.core.min.js"></script>
<script type="text/javascript"
	src="jspbk3/assets/jsLibs/chosen.jquery.js"></script>

<style>
.btn-group.pull-right {
	display: none
}

.box-inner {
	border: 0px solid #DEDEDE;
	margin: 0;
	background: #fff;
	padding: 0px 0px 0px 0px;
	-webkit-box-shadow: 0 0 10px rgba(189, 189, 189, 0.4);
	-moz-box-shadow: 0 0 10px rgba(189, 189, 189, 0.4);
	-ms-box-shadow: 0 0 10px rgba(189, 189, 189, 0.4);
	-o-box-shadow: 0 0 10px rgba(189, 189, 189, 0.4);
	box-shadow: 0 0 10px rgba(189, 189, 189, 0.4);
}

.box-header {
	background: #de7b1f;
	color: #fff;
	padding: 9px 8px;
	font-size: 17px;
	margin: 2px 0;
	min-height: 42px;
	line-height: 30px;
}

.box-content {
	padding: 10px;
	min-height: 59px;
	height: auto;
}
</style>

<%
	Date d1 = new Date();
	SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
	DecimalFormat dformat = new DecimalFormat("###.00");
	String currentDate = df.format(d1);
	List<RefundAgent> refundList = new ArrayList<RefundAgent>();
	ResponseBean refundAgent = null;
	if (request.getAttribute("refundListResp") != null) {
		refundAgent = (ResponseBean) request.getAttribute("refundListResp");
		refundList = refundAgent.getRefundList();
	}
%>

<script type="text/javascript">
   $(document).ready(function() {    
    $('#table').DataTable( {
        dom: 'Bfrtip',
        autoWidth: false,
        order: [[ 0, "desc" ]],
        buttons: [
             {
             
                extend: 'copy',
                text: 'COPY',
                title:'Wallet History - ' + '<%=currentDate%>',
                message:'<%=currentDate%>',
            },  {
             
                extend: 'csv',
                text: 'CSV',
                title:'Wallet History - ' + '<%=currentDate%>',
              
            },{
             
                extend: 'excel',
                text: 'EXCEL',
                title:'Wallet History - ' + '<%=currentDate%>',
            
            }, {
             
                extend: 'pdf',
                text: 'PDF',
                title:'Wallet History - ' + '<%=currentDate%>',
                message:" "+ "<%=currentDate%>" + "",
               
            },  {
             
                extend: 'print',
                text: 'PRINT',
                title:'Wallet History - ' + '<%=currentDate%>
	',

			} ]
		});

	});
</script>

</head>

<%
	User user = (User) session.getAttribute("User");
	List<RechargeTxnBean> list = (List<RechargeTxnBean>) request.getAttribute("resultList");
%>

<body>

	<!-- topbar starts -->
	<jsp:include page="/jsp/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<!-- left menu starts -->
	<jsp:include page="/jsp/mainMenu.jsp"></jsp:include>
	<!-- left menu ends -->

	<!-- contents starts -->


	<div id="main" role="main">
		<div id="content">
			<div class="row">

				<div class="col-md-12">
					<div class="box2">
						<div class="box-inner">
							<div class="box-header">
								<h2>Sender Mobile Number</h2>
							</div>
							<h5 style="padding-left: 5px;">(To add new sender or search
								existing sender)</h5>
							<div class="box-content">
								<form data-ng-submit="sender.postSenderData();">
									<font color="red"><s:actionerror /></font> <font color="blue"><s:actionmessage />
									</font>
									<div class="col-md-4 col-sm-3 txtnew col-xs-6">
										<input type="hidden" value="${csrfPreventionSalt}"
											name="csrfPreventionSalt"> <input autocomplete="off"
											data-mw-input-restrict="onlynumbers"
											data-ng-model="sender.mobile" id="senderNum"
											data-ng-keyup="sender.valid()" maxlength="10" type="text"
											style="margin-left: 0px;" class="form-control "
											placeholder=" Mobile Number" required />
									</div>
									<div class="col-md-4 txtnew col-sm-3 col-xs-6 text-left">
										<div id="wwctrl_submit">
											<input class="btn btn-infobtn btn-info btn-fill submit-form"
												data-ng-disabled="sender.mobileValidation" id="submit"
												type="submit" value="Submit"> <input
												class="btn btn-info" id="reset" type="reset" value="Reset">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12">
					<div id="xyz">



						<%-- <table id="table" class="scrollD cell-border dataTable">
							<thead> 
								<tr>
									<th><u>Id</u></th>
									<th><u>MobileNo</u></th>
									<th><u>Transaction Type</u></th>
									<th><u>Txn Amount</u></th>
									<th><u>Refund Amount</u></th>
									<th>
										<tdo!=null&&tdo.getOtp()!=null&&tdo.getOtp().equalsIgnoreCase("0") u>Txn Status</u>
									</th>
									<th><u>Refund</u></th> 	 	
								</tr>
							</thead>
							<tbody>
							    <% 
							    if(refundList !=null && refundList.size() > 0){ 
								RefundAgent tdo=refundList.get(0);%>
					          	<tr> 
					          	    <td><%=tdo.getId()%></td> 
					         	    <td><%=tdo.getMobileno()%></td> 
									<td><%=tdo.getNarrartion()%></td>
									<td><%=dformat.format(Double.parseDouble(tdo.getTxnamount()))%></td>
					                <td><%=dformat.format(Double.parseDouble(tdo.getDramount()))%></td>
					                <td><%=tdo.getStatus()%></td>
					                <td>
							       	    <%if(tdo.getStatus().equals("Confirmation Awaited")){ %> 
							       	    <button class="btn btn-danger" style="background:red!important;padding: 5px 18px"disabled="disabled">Refund</button>
							       	    <%}else if(tdo.getStatus().equals("FAILED")) {%>
							       		<% if( tdo!=null&&tdo.getOtp()!=null){%>
							       		<button class="btn btn-info" onclick="verifyByOtp('<%=tdo.getMobileno() %>','<%=tdo.getId() %>','<%=tdo.getOtp()%>')" style="background:green!important;padding: 5px 18px"  >Refund</button>  
							          	<%} %>  
							          	<%} %>
					                </td>
				              	</tr>
						        <%} %>	
						    </tbody>		
						</table> --%>

						<div class="grid-box col-md-12"
							data-ng-controller="favlistCtrl as fav"
							data-ng-init="fav.popGrid();">
							<div class="box-header">
								Priority Sender(s) <input type="text"
									class="grid-search pull-right" data-ng-model="fav.search" />
							</div>
							<div class="table-container col-md-12">




								<table class="table table-responsive table-striped" fixed-header>


									<thead>
										<tr>
											<th><a href="javascript:void(0)" class="whiteColor"
												data-ng-click="sortTypeFav = 'name'; favSortReverse = !favSortReverse">
													Name <span
													data-ng-show="sortTypeFav == 'name' && !favSortReverse"
													class="glyphicon glyphicon-chevron-down"></span> <span
													data-ng-show="sortTypeFav == 'name' && favSortReverse"
													class="glyphicon glyphicon-chevron-up"></span>
											</a></th>
											<th><a href="javascript:void(0)" class="whiteColor"
												data-ng-click="sortTypeFav = 'mobileNumber'; favSortReverse = !favSortReverse">
													Mobile Number <span
													data-ng-show="sortTypeFav == 'mobileNumber' && !favSortReverse"
													class="glyphicon glyphicon-chevron-down"></span> <span
													data-ng-show="sortTypeFav == 'mobileNumber' && favSortReverse"
													class="glyphicon glyphicon-chevron-up"></span>
											</a></th>
											<th><a href="javascript:void(0)" class="whiteColor"
												data-ng-click="sortTypeFav = 'mobileNumber'; favSortReverse = !favSortReverse">
													Wallet Balance <span
													data-ng-show="sortTypeFav == 'walletbalance' && !favSortReverse"
													class="glyphicon glyphicon-chevron-down"></span> <span
													data-ng-show="sortTypeFav == 'walletbalance' && favSortReverse"
													class="glyphicon glyphicon-chevron-up"></span>
											</a></th>
											<th><a href="javascript:void(0)" class="whiteColor"
												data-ng-click="sortTypeFav = 'mobileNumber'; favSortReverse = !favSortReverse">
													Transfer Limit <span
													data-ng-show="sortTypeFav == 'transferlimit' && !favSortReverse"
													class="glyphicon glyphicon-chevron-down"></span> <span
													data-ng-show="sortTypeFav == 'transferlimit' && favSortReverse"
													class="glyphicon glyphicon-chevron-up"></span>
											</a></th>
											<!--       <th>
			   <a href="javascript:void(0)"  class="whiteColor" data-ng-click="sortTypeFav = 'mobileNumber'; favSortReverse = !favSortReverse">
   		KYC Status
            <span data-ng-show="sortTypeFav == 'kycstatus' && !favSortReverse" class="glyphicon glyphicon-chevron-down"></span>
            <span data-ng-show="sortTypeFav == 'kycstatus' && favSortReverse" class="glyphicon glyphicon-chevron-up"></span>
          </a></th> -->

											<th>Action</th>
											<!-- <th>Update KYC</th> -->
											<th>Remove</th>
										</tr>
									</thead>

									<tr
										data-ng-repeat="gridItem in $root.favList |  orderBy:sortTypeFav: favSortReverse | filter: fav.search"
										id="row-{{gridItem.favouritid}}">
										<td>{{gridItem.name}}</td>
										<td>{{gridItem.mobileno}}</td>
										<td>{{gridItem.walletbalance | currency:
											$root.loginUserData.countrycurrency + " "}}</td>
										<td>{{gridItem.transferlimit | currency:
											$root.loginUserData.countrycurrency + " "}}</td>
										<!-- <td>
    {{gridItem.kycstatus}}
</td> -->
										<td><input type="button" value="Login Sender"
											data-ng-click="fav.loginSender(gridItem.mobileno)"
											data-ng-disabled="fav.isProccessing"
											class="btn-secondary btn grid-btn" /></td>
										<!-- <td>

	
	  <input type="button" value="Update KYC" data-ng-click="fav.updateSenderKYC(gridItem.mobileno)"  data-ng-disabled="fav.isProccessing" data-ng-hide="gridItem.kycstatus == 'FULL KYC'" class="btn-secondary btn grid-btn" />
	
<span style="color:green" data-ng-show="gridItem.kycstatus == 'FULL KYC'">KYC Done</span>
  
</td> -->
										<td class="delete-opt-col" style="overflow: visible;"><a
											href="javascript:void(0)" id="delete{{gridItem.favouritid}}"
											data-ng-click="fav.showDeleteFav( gridItem.favouritid)"><span
												class="glyphicon glyphicon-trash" aria-hidden="true"></span>

										</a>
											<div class="popover confirmation fade left in fav-grid-popup"
												style="top: -21px; left: 38px; display: none;">
												<div class="arrow"></div>
												<div class="popover-content">
													<p class="confirmation-content">Are you sure?</p>
													<div class="confirmation-buttons text-center">
														<div class="btn-group">
															<a href="javascript:void(0)" class="btn btn-xs "
																data-ng-click="fav.deleteFav(gridItem.senderid ,gridItem.favouritid)"
																data-apply="confirmation"> <i
																class="glyphicon glyphicon-ok"></i> Yes
															</a><a href="javascript:void(0)"
																data-ng-click="fav.hideDeletePopup(gridItem.favouritid)"
																class="btn btn-xs btn-default"
																data-dismiss="confirmation"><i
																class="glyphicon glyphicon-remove"></i> No</a>
														</div>
													</div>
												</div>
											</div></td>


									</tr>
								</table>



							</div>
						</div>



					</div>
				</div>

				<div id="otpPopup" class="modal fade" role="dialog">
					<div class="modal-dialog" style="width: 400px">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">
									Verify <span class="t-isOtp"></span>
								</h4>
							</div>

							<div class="modal-body">
								<div class="form-group">
									<form method="post" action="refundAgentRequest">
										<input type="hidden" name="user.userId" id="otpMobile" /> <input
											type="hidden" name="transactionId" id="refund-id" /> <input
											type="hidden" name="user.hceToken" id="isOtp" value="1">
										<label class="t-isOtp">OTP/MPIN</label> <input
											class="form-control" name="user.otp" type="password"
											id="r-otp" required> <input
											class="btn btn-info btn-block" style="margin: 20px 0 0 0"
											type="submit" value="Submit"/ >
									</form>
								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<%
		if (request.getAttribute("refundListResp") != null && refundAgent.getCode().equals("3000")) {
	%>

	<script>
		$(function() {
			$("#refundModal").modal()
		});
	</script>
	<div id="refundModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Refund Response</h4>
				</div>
				<div class="modal-body">
					<p><%=refundAgent.getMessage()%>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
	<%
		}
	%>

	<jsp:include page="/jsp/footer.jsp"></jsp:include>

	<!-- external javascript -->

	<%--<script src='js/bootstrap.min.js'></script>--%>

	<!-- library for cookie management -->
	<script src="./js/jquery.cookie.js"></script>
	<script src="./js/jquery.noty.js"></script>
	<script src="./js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<!--<script src="./js/charisma.js"></script>-->

	<script>
		function verifyByOtp(m, r, isOtp) {
			$("#r-otp").val('')

			$('#isOtp').val(isOtp);

			$('#otpMobile').val(m);
			$('#refund-id').val(r);
			if (isOtp == 0) {
				$('#isOtp').attr('maxlength', '4')
				$(".t-isOtp").text('MPIN')
			} else {
				$(".t-isOtp").text('OTP');
				$('#isOtp').attr('maxlength', '6')
				$.ajax({
					method : 'POST',
					url : 'RefundOtp',
					data : "mastBean.mobileNo=" + m + "&mastBean.id=" + r,
					success : function(res) {
						console.log(res)
					}

				});
			}
			$("#otpPopup").modal('show');
			$("#otpMobile").val(m)

		}

		function summitOtp(m) {
			var o = $("#r-otp").val()
			$.ajax({
				method : 'POST',
				url : 'VerifyRefundOTP" ',
				data : "user.userId=" + m + "&user.otp=" + o,
				success : function(res) {

					console.log(res)

					if (res.status == 'true') {

						//$("#otpPopup").modal('show');
						//$("#otpMobile").val(m)
					}

				}

			});

		}
	</script>

	<script type="text/javascript" type="text/javascript">
    $(function(){
	SESSIONID = '<%=sessionid %>'
	
	
		$(window).focus(function(){
	
		 $.get('CheckUserSession',function(data){
			
			if(data.sessionId != SESSIONID){

				window.close();
				window.location.replace("/UserHome");
			}
		 })
		 })
		 
   }) 
</script>

</body>
</html>